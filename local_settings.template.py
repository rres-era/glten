# This is a TEMPLATE for the local_settings.py file -- make a copy, then edit.

# Import standard settings.

from glten.settings import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '-^5m%te18i!q5px0(+6w=bo-ja-l%$-dyfm-lgt5)b-doji!eb'   # Update for production instances

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
ALLOWED_HOSTS = ['localhost', '127.0.0.1', '[::1]']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': '<FIXME>',
        'USER': '<FIXME>',
        'PASSWORD': '<FIXME>',
        'HOST': '127.0.0.1',
        'PORT': '5432'
    }
}

# E-mail configuration

EMAIL_HOST = '<FIXME>'
EMAIL_PORT = 465      # Typically 465 if using "implicit" HTTPS, 587 if using STARTTLS, or 25 for unencrypted SMTP (not recommended!)
EMAIL_USE_SSL = True  # May want EMAIL_USE_TLS instead 
EMAIL_HOST_USER = '<FIXME>' 
EMAIL_HOST_PASSWORD = '<FIXME>'

DEFAULT_FROM_EMAIL = '<FIXME>'

# Server-side validation
#
# This is implemented as a separate, JS, service (currently bundled alongside the server-side rendering support)
# in order to avoid duplicated logic and potential mismatches with the client-side validation.  The backend
# allows this to be disabled (by setting SERVER_VALIDATION_ENDPOINT to None), but this potentially allows
# invalid descriptions to be published, if using a custom (or outdated) client.

# SERVER_VALIDATION_ENDPOINT = 'http://localhost:4000/_internal/validate'  # Uncomment to validate against a local SSR server.
SERVER_VALIDATION_ENDPOINT = None  # No server-side validation.  Not recommended for production!
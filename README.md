# GLTEN Portal

## Prerequisites

Operating system: recent MacOS or Linux distribution.  Windows 10 (with Windows Subsystem
for Linux) should also work.

Python: developed with 3.7.  3.6 should also be fine.

NodeJS: recommend 10.16.x.  Later versions should also be fine.

You will also need a PostgreSQL database.  Testing is against version 11.x, but
exact version is probably not critical.

## Configuring the backend (database etc.)

If not previously done, install a recent PostgreSQL server, and create an empty
database:

      psql [...connection details]
      create database glten;

To configure the Django backend application, make a copy of `local_settings.template.py`,
named `local_settings.py`, and edit as appropriate.  At a minimum, insert your local database
connection details (username and password).  For production instances, there are other
parameters which should generally be changed, too.

Ideally, you should provide the name and credentials of an out-bound e-mail server, although
this isn't strictly needed for local testing.

## Running the system

We recommend running in a virtualenv.  You can create one using:

      python -m venv venv
      . ./venv/bin/activate

Install dependencies:

      pip install -r requirements.txt

Setup the database:

      python manage.py migrate
      python manage.py createsuperuser    # and create a user account for login
      python manage.py load_ontologies

Build the frontend:

      python manage.py build_frontend

Run the server:

      python manage.py runserver

By default, the system will run on http://localhost:8000

Currently, you can log in using the credentials you set up with `createsuperuser`,
view a list of experiments you own, and view/edit them using a (work-in-progress)
editing interface.  There is also a mechanism for requesting publication, and
users with appropriate permissions can view a queue of publication requests which
can be approved and denied.

## Running the frontend in development mode

For development purposes, you may prefer to run an Angular development
server alongside the Django application.  This is a bit faster than
repeatedly running `build_frontend`, and supports automatic reloading.

       cd glten-frontend
       npm install
       npm install -g @angular/cli    # NB may require sudo
       ng serve --open


## Updating

If you update to a new version of the code, we always recomment:

      pip install -r requirements.txt  # Update back-end requirements
      python manage.py migrate         # Update database schema
      python manage.py load_ontologies   # Update vocabulary data
      python manage.py build_frontend
      python manage.py collectstatic
      (cd glten-frontend && npm serve:ssr)  # If using server-side rendering,
                                            # and always for production deployments

## Accounts

User accounts can be make with `createsuperuser` from the command line, or via the
Django admin web interface (available at http://localhost:4200/admin).  Superusers
have publishing priveleges by default, while users created via the web interface
need an explicit "see and approve publication requests" grant (via the admin interface).

## Server-side rendering

The system optionally supports server-side rendering.  To try this:

    cd glten-frontend
    npm run build:ssr
    npm run serve:ssr

Server-side rendering is implemented using and "Angular Universal" system, and is mostly
non-invasive in the main code.  One detail to watch out for is that Leaflet requires
access to certain DOM APIs, and current Angular design makes loading the leaflet code
conditionally very difficult.  Therefore, we use domino to mock a minimal DOM on the
server side.

The behaviour of the server can be tweaked with environment variables

  * `PORT` network port to serve on (default 4000)
  * `LISTEN_ADDRESS` network address to listen on.  Default is '127.0.0.1', which
     means that connections will only be accepted over the `localhost` interface.
     If serving publically without a proxy, or proxying on another machine, you may
     need to change this, typically to '0.0.0.0' (listen on all interfaces).
  * `BACKEND_SERVER` base URL of the backend (default 'http://localhost:8000').

## Server-side description validation

For production installations, we recommend that the descriptions get re-validated
on the server side in order to prevent non-standard clients (or, possibly, version
skew between server and client -- if the user has kept a browser tab open for a long
time) approving a non-valid description.

To avoid potential mismatches between client-side and validation logic, we perform
the server side validation in Javascript, re-using client side validation code.  Currently,
the validation server is bundled with the server-side-rendering support service.  

To enable server-side validation, ensure that the 'serve:ssr' server is running, then
set SERVER_VALIDATION_ENDPOINT (in local_settings.py) appropriately.  If
SERVER_VALIDATION_ENDPOINT is left as None, server-side validation is skipped (which
is helpful for development, but probably not a good idea in production).

## Public Deployment

For public deployment, we recommend using the "server-side rendering" approach.  In
outline:

1. Ensure that the Django configuration (`local_settings.py`) is appropriate for
public use.  In particular, ensure `DEBUG = False`, `SECRET_KEY` is set to a value
which does not appear in any public repositories, and e-mail options are set 
appropriately.  We also recommend that server-side validation is enabled (which
should be straightforward, since we'll be runnng an SSR server).

2. Run `python manage.py collectstatic`

3. Run the backend application using an appropriate WSGI server.  We don't recommend
using `runserver` in production.  [Green Unicorn](https://gunicorn.org/) is a reasonable
choice.  Run with:

        gunicorn -w 4 glten.wsgi

The `-w` parameter specified the number of workers to use.  Because some backend operations
can block for non-trivial time on IO (in particular, sending transactional e-mails), this
should almost certainly be greater than 1 even when running on fairly low-specification
machines.

4. Run the frontend with:

        npm run build:ssr
        npm run serve:ssr

There should now be a complete installation running on http://localhost:4000/.

5. Generally, you'll want to run a traditional web-server as a proxy in front of the
application, for serving any additional static content and providing extra protocol
functionality (HTTPS, HTTP2, etc.).  If using Apache, a sensible starting point for
proxy configuration might be:

        ProxyPass / http://localhost:4000/
        ProxyPassReverse / http://localhost:4000/

You may need to explictly enable mod_proxy and mod_proxy_http to make this work.

## Deployment via supervisor

A *very* simple example `supervisor.conf` file is included.  You can use this to run
the backend and SSR servers together (with some simple process monitoring) using:

        venv/bin/supervisord -c supervisor.conf

This could be adapted for running as a daemon via init scripts, but is likely to
need some changes to match the local environment.  As an absolute minimum, set
`nodaemon=false` and check path names.

## Adding new fields

1. Add the desired field to `models.py`.  Note that you will almost always want to make
it nullable and/or blankable even if the field is not optional, because users expect
to be able to store incomplete drafts in the system.

2. Check serializers.py.  Some serializers explicitly list the fields to include, in which
case you will need to add your new field(s).

3. For important textual fields, consider whether you want to add them to the search index.
This is currently defined in `api.py` (TODO might want to move this?)

4. `python manage.py makemigrations -n description_of_fields`  Note that this creates a
new `.py` file in `glten/migrations`, which should be added to the repository in
the same commit as your chances to `models.py`, so potentially worth `git add` ing
the new file now to make sure you don't forget it.

5. `python manage.py migrate`

6. On the frontend side, add the field to `src/app/types.ts`

7. Edit `experiment-form-model.service.ts` to convert between wire format objects and
[Angular Reactive Forms](https://angular.io/guide/reactive-forms) model objects.  If
your new field is non-optional, or has other validation constraints, this is the place
to specify them.

8. Add a new section to the HTML template of the relevant "editor" component.  For simple
fields, this will look something like:

        <div class="form-group"> 
            <label for="new-field">My new field</label>
            <input class="form-control"
                   glt-is-invalid
                   id="new-field"
                   type="text"
                   formControlName="newField" />
            <div class="invalid-feedback">
                You must specify a name.
            </div>
            <glt-form-help>
                This is an important new field I've just added to the models.
            </glt-form-help>
        </div>

### Adding an "ontology term" field.

If also adding a new vocabulary, add it to the `ontologies` directory and update 
`glten/management/commands/load_ontologies.py` to load it.  The list of ontologies
to load is configured in the BOOTSTRAP_ONTOLOGIES array at the top of `load_ontologies.py`,
including instructions for configuring a new ontology.

Add a `LabelOrTerm` field to `models.py`.  Note that this actually corresponds to
two fields in the database, and the wire-protocol objects.

You'll need to update `serializers.py` with an ontology-specific field serializer, e.g.:

             new_field_term = VocabularyTermRelatedField(required=False, 
                                                         allow_null=True,
                                                         queryset=VocabularyTerm.objects.filter(vocabulary__name='My new ontology'))

The editor HTML should follow the pattern:

        <div class="form-group">
            <label for="new-field">New ontology field</label>
            <glt-vocab-term-input inputId="new-field"
                                  vocabulary="My new ontology"
                                  [model]="model.get('newField')">
                <glt-form-help>
                    If help is provided, it should be a child of 
                    glt-vocab-term-input.
                </glt-form-help>
            </glt-vocab-term-input>
        </div>







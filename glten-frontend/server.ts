/**
 * *** NOTE ON IMPORTING FROM ANGULAR AND NGUNIVERSAL IN THIS FILE ***
 *
 * If your application uses third-party dependencies, you'll need to
 * either use Webpack or the Angular CLI's `bundleDependencies` feature
 * in order to adequately package them for use on the server without a
 * node_modules directory.
 *
 * However, due to the nature of the CLI's `bundleDependencies`, importing
 * Angular in this file will create a different instance of Angular than
 * the version in the compiled application code. This leads to unavoidable
 * conflicts. Therefore, please do not explicitly import from @angular or
 * @nguniversal in this file. You can export any needed resources
 * from your application's main.server.ts file, as seen below with the
 * import for `ngExpressEngine`.
 */

import 'zone.js/dist/zone-node';

import * as express from 'express';
import proxy from 'express-http-proxy';
import {join, dirname} from 'path';

const domino = require('domino');  // "Require" rather than "import" means we can monkey-patch domino (see below)

// Mock some global DOM objects, which are required by LeafletJS.  
// Recent Angular makes it difficult to conditionally load components or directives based on
// server/client platform, so we have to include Leaflet in the server builds.  This prevents
// resulting errors.  Might be possible to remove this if the dependency on Leaflet is removed
// in the future.

const win = domino.createWindow();
win.devicePixelRatio = 1;  // Not present in default domino, but keeps Leaflet happy.
global['window'] = win;
global['document'] = win.document;
global['navigator'] = win.navigator;

// Express server
const app = express();

const PORT = process.env.PORT || 4000;
const LISTEN_ADDRESS = process.env.LISTEN_ADDRESS || '127.0.0.1';
const DIST_FOLDER = join(process.cwd(), 'dist/browser');
const DJANGO_STATIC_FOLDER = join(dirname(process.cwd()), 'static');

const BACKEND_SERVER = process.env.BACKEND_SERVER || 'http://localhost:8000';

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const {AppServerModuleNgFactory, LAZY_MODULE_MAP, ngExpressEngine, provideModuleMap, platformServer, INITIAL_CONFIG, ExperimentFormModelService } = require('./dist/server/main');

// Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));

app.set('view engine', 'html');
app.set('views', DIST_FOLDER);

{
    const validationRouter = express.Router();
    validationRouter.use(express.json());
    validationRouter.post('/validate', (req, resp, next) => {

        /*
         * Instantiate the app module in order to get a form model service object. 
         * Currently, we need to provide a dummy document in order that the module
         * initialises correctly.  It may be possible to avoid this if we move
         * ExperimentFormModelService into its own module.
         */

        const platform = platformServer([
            {
                provide: INITIAL_CONFIG,
                useValue: {
                    document: '<glt-root></glt-root>',
                    url: `http://localhost:4000/`
                }
            },
            provideModuleMap(LAZY_MODULE_MAP),

        ]);

        const mod = platform.bootstrapModuleFactory(AppServerModuleNgFactory);
        mod.then((mod) => {
            const modelService = mod.injector.get(ExperimentFormModelService);
            const model = modelService.descToFormModel(req.body);

            /*
             * Status should be VALID or INVALID.   PENDING is also possible, but appears to
             * only happen if asynchronous validators are used.
             */

            resp.send({'status': model.status});
        }).catch((err) => {
            next(err);
        });
    });
    app.use('/_internal', validationRouter);
}

// Proxy API requests.
app.use('/api/**', proxy(BACKEND_SERVER, {
    proxyReqPathResolver: (req) => {
        return req.originalUrl;
    }
}));

// Proxy Django accounts management
app.use('/accounts/**', proxy(BACKEND_SERVER, {
    proxyReqPathResolver: (req) => {
        return req.originalUrl;
    }
}))

// Proxy Django admin management
app.use('/admin/**', proxy(BACKEND_SERVER, {
    proxyReqPathResolver: (req) => {
        return req.originalUrl;
    }
}))

// Example Express Rest API endpoints
// app.get('/api/**', (req, res) => { });
// Serve static files from /browser
app.use('/static', express.static(DJANGO_STATIC_FOLDER));
app.get('*.*', express.static(DIST_FOLDER, {
  maxAge: '1y'
}));

// All regular routes use the Universal engine
app.get('*', (req, res) => {
  res.render('index', { req });
});

// Start up the Node server
app.listen(PORT, LISTEN_ADDRESS, () => {
  console.log(`Node Express server listening on http://${LISTEN_ADDRESS}:${PORT}`);
});

import { enableProdMode } from '@angular/core';

import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

// Patch xhr2 so that we can set cookies from server-side requests (see also,
// server-urls.interceptor)
import * as xhr2 from 'xhr2';
xhr2.prototype._restrictedHeaders = {};

export { AppServerModule } from './app/app.server.module';
export { ngExpressEngine } from "@nguniversal/express-engine";
export { provideModuleMap } from "@nguniversal/module-map-ngfactory-loader";
export { INITIAL_CONFIG, platformServer } from "@angular/platform-server";
export { ExperimentFormModelService } from "./app/experiment-form-model.service";


import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { tap, map, mergeMap, shareReplay, catchError } from 'rxjs/operators';

import lunr from 'lunr';

import { Vocabulary, VocabularyTerm } from './types';

@Injectable({
    providedIn: 'root'
})
export class VocabLookupService {
    private apiBase = '/api/v0';
    private _vocabs: Observable<Vocabulary[]>;
    private _vocabTerms: {string: Observable<VocabHolder>} = 
        <{string: Observable<VocabHolder>}>{};

    constructor(private http: HttpClient) { }

    get vocabs() {
        if (!this._vocabs) {
            const url = `${this.apiBase}/vocab`;
            this._vocabs = this.http.get<Vocabulary[]>(url).pipe(
                shareReplay(1)
            );
        }
        return this._vocabs;
    }

    getVocab(name, includeDescriptions=false) : Observable<VocabHolder> {
        const key = `${name}__${!!includeDescriptions}`;

        if (!this._vocabTerms[key]) {
            this._vocabTerms[key] = this.vocabs.pipe(
                mergeMap((vocabData) => {
                    const vocabMeta = vocabData.filter((v) => v.name === name)[0];
                    if (!vocabMeta) {
                        return throwError(`No vocabulary named ${name}`);
                    } else {
                        const url = `${this.apiBase}/vocab/${vocabMeta.id}/${includeDescriptions ? 'descriptions' : 'terms'}`;
                        return this.http.get<VocabularyTerm[]>(url);
                    }
                }),
                catchError((err) => {
                    // If loading fails, create a blank ontology-lookup.
                    // There's an argument for propagating the error further, but catching
                    // it downstream is problematic because shareReplay doesn't replay errors.

                    console.log(err);
                    return [];
                }),
                map(makeVocabHolder),
                shareReplay(1)
            );
        }

        return this._vocabTerms[key];
    }

    lookup(vocabName, includeDescriptions, minLength, term) : Observable<VocabTermMatch[]> {
        if (term.length < minLength) {
            return of(<VocabTermMatch[]>[]);
        } else {
            return this.getVocab(vocabName, includeDescriptions).pipe(
                map((terms) => terms.search(term))
            );
        }
    }

    allTerms(vocabName, includeDescriptions) : Observable<VocabTermMatch[]> {
        return this.getVocab(vocabName, includeDescriptions).pipe(
            map((vocab) => 
                vocab.terms.
                    filter((v) => !v.dead).
                    map((term) => new VocabTermMatch(term, null))
            )
        );
    }
}

class VocabHolder {
    terms:  VocabularyTerm[];
    termsByID: {string: VocabularyTerm[]};
    index: any;

    constructor(terms: VocabularyTerm[], index: any) {
        this.terms = terms;
        this.index = index;

        this.termsByID = <{string: VocabularyTerm[]}>{};
        for (const term of terms) {
            this.termsByID[term.identifier] = term;
        }
    }

    search(term) {
        const fullTextMatches = this.index.search(term)
            .map((searchResult) => {
                return (
                    new VocabTermMatch(
                        this.termsByID[searchResult.ref],
                        searchResult.matchData.metadata
                    )
                );
            });

        if (fullTextMatches.length > 0) {
            return fullTextMatches.splice(0, 10);
        } else {
            const lcTerm = term.toLowerCase();

            return this.terms
                .filter((v) => !v.dead && v.name && v.name.toLowerCase().indexOf(lcTerm) >= 0)
                .map((term) => new VocabTermMatch(term, null))
                .splice(0, 10);
        }

    }
}

function makeVocabHolder(terms: VocabularyTerm[]) : VocabHolder {
    const index = lunr(function() {
        // NB this has to be an old-style function rather than an arrow function
        // because Lunr's API depends on binding this.

        this.ref('identifier');
        this.field('identifier');
        this.field('name');
        this.field('description')
        this.metadataWhitelist = ['position'] // To support search-match snippets

        for (const term of terms) {
            if (!term.dead) {
                this.add(term);
            }
        }
    });

    return new VocabHolder(terms, index);
}

export class VocabTermMatch {
    constructor(public term: VocabularyTerm, public matchData: any) {}
}


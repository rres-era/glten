import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ExperimentDesc } from '../types';
import { PublishedExperimentsService } from '../published-experiments.service';

@Component({
    selector: 'glt-experiment-view',
    templateUrl: './experiment-view.component.html',
    styleUrls: ['./experiment-view.component.css']
})
export class ExperimentViewComponent implements OnInit {
    expt: ExperimentDesc;
    err = null;

    constructor(
        private experimentService: PublishedExperimentsService,
        private route: ActivatedRoute
    ) { }

    get experimentID() {
        return parseInt(this.route.snapshot.paramMap.get('id'));
    }

    ngOnInit() {
        this.load();
    }

    load() {
        this.experimentService.getExperiment(this.experimentID)
            .subscribe(
                (desc) => {
                    this.err = null;
                    this.expt = desc;
                },
                (err) => {
                   this.err = err;
                   this.expt = null;
                }
            );
    }

}

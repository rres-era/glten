import { Component, Input } from '@angular/core';

import { ExperimentPeriod } from '../types';

@Component({
    selector: 'glt-design-phase-view',
    templateUrl: './design-phase-view.component.html',
    styleUrls: ['./design-phase-view.component.css']
})
export class DesignPhaseViewComponent {
    @Input() period: ExperimentPeriod;

    constructor() { }

    getCropName(id: number, all_crops: boolean): string {
        if (all_crops) {
            return 'All crops';
        } else if (id === null) {
            return '-';
        } else {
            const crop = this.period.crops.find((crop) => crop.id === id);
            if (!crop) {
                return 'Unknown';
            } else {
                return crop.crop_label || crop.crop_term;
            }
        }
    }

    getMaterialName(material: string, cropId: number): string {
        if (material === 'AllCrops') {
            return 'All crops';
        } else if (material === 'Soil') {
            return 'Soil';
        } else if (material === 'SpecifiedCrop') {
            return this.getCropName(cropId, false);
        } else {
            return 'Not specified';
        }
    }

    getFactorName(id: number): string {
        if (id === null) {
            return '-';
        } else {
            const factor = this.period.factors.find((factor) => factor.id === id);
            if (!factor) {
                return 'Unknown';
            } else {
                return factor.factor_label || factor.factor_term;
            }
        }
    }

    getFactorLevelName(id: number): string {
        if (id === null) {
            return '-';
        } else {
            for (const factor of this.period.factors) {
                for (const level of factor.levels) {
                    if (level.id === id) {
                        return level.factor_variant_label || level.factor_variant_term;
                    }
                }
            }
        }
        return 'Unknown';
    }

    getRotationGroups(phases) {
        const years = [];
        let yearPhases = [];
        for (const phase of phases) {
            if (!phase.same_phase && yearPhases.length > 0) {
                years.push(yearPhases);
                yearPhases = [];
            }
            yearPhases.push(phase);
        }
        if (yearPhases.length > 0) {
            years.push(yearPhases);
        }
        return years;
    }

    isNumber(val) {
        // Required because typeof can't be used in Angular templates (!)
        return typeof(val) === 'number';
    }

}

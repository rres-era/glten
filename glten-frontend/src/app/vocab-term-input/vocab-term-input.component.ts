import { Component, Input, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, mergeMap, catchError, tap } from 'rxjs/operators';

import { VocabularyTerm } from '../types';
import { VocabLookupService, VocabTermMatch } from '../vocab-lookup.service';

const MAX_CONTEXT_CHARS = 25;
const MAX_CONTEXT_WORDS = 2;

/*
 * Currently implementing this as a component, which is the most straightforward
 * approach.  However, the way the "ontology term" indicators update doesn't really
 * fit the reactive-forms model, and could plausibly be a performance issue
 * if there are a large number of these on one page.  If need be, this could
 * (at the cost of some extra complexity) be reimplemented as a directive,
 * using ControlValueAccessor to interact with the form system.
 */

@Component({
    selector: 'glt-vocab-term-input',
    templateUrl: './vocab-term-input.component.html',
    styleUrls: ['./vocab-term-input.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class VocabTermInputComponent {
    @Input() inputId: string;
    @Input() vocabulary: string;
    @Input() model: FormControl;

    /*
     * As well as working as a type-ahead, act as a drop-down menu, with a button to show all
     * available terms.  Best used for vocabularies with few (< about 20) terms.
     */
    @Input() altMenu: boolean;

    /*
     * Full text search runs against descriptions as well as labels.
     */
    @Input() termDescriptions: boolean;

    @ViewChild('vocabInput', { static: true }) vocabInput: ElementRef;

    constructor(private vocabLookup: VocabLookupService) { 
        this.search = this.search.bind(this);
    }

    search(text$: Observable<string>) {
        return text$.pipe(
            debounceTime(200),
            distinctUntilChanged(),
            mergeMap((s) => {
                if (this.altMenu && !s || s.length < 3) {
                    return this.vocabLookup.allTerms(this.vocabulary, this.termDescriptions);
                } else {
                    return this.vocabLookup.lookup(this.vocabulary, this.termDescriptions, 3, s)
                }
            })
        )
    }

    get termID(): string {
        const v = this.model.value;
        if (v && v.identifier) {
            return v.identifier;
        }
    }

    formatInput(term) {
        if (typeof(term) === 'string') {
            return term;
        } else {
            return term.name;
        }
    }

    /*
     * Return a list of span records ({text, clazz}) which represent a highlighted match to the term description,
     * in the case that the match is the result of a full-text match.
     * 
     * NB. This implementation is fairly heavily tied to Lunr's match-metadata datastructure, and would need
     * re-writing if we used a different full-text search system.
     *
     * NB. This gets called more often than one might hope.  If it becomes a performance bottleneck, consider
     * caching (or possibly re-factoring as a Pipe?)
     */

    getMatch(result) {
        if (!result.matchData) return [];
        for (const termMatch of Object.values(result.matchData) as any[]) {
            if (termMatch.description && termMatch.description.position) {
                const [[offset, len]] = termMatch.description.position;

                const spans = [],
                      desc = result.term.description;
                if (offset > 0) {
                    let chars = 1, words = 0;
                    while (chars < MAX_CONTEXT_CHARS && words < MAX_CONTEXT_WORDS && offset - chars > 0) {
                        ++chars;
                        if (desc[offset - chars] === ' ') ++words;
                    }

                    spans.push({
                        text: desc.substring(
                            offset - chars,
                            offset
                        ),
                        clazz: 'match-normal'
                    });
                }
                spans.push(
                    {text: desc.substring(offset, offset+len), clazz: 'match-highlight'}
                )
                if (offset + len < desc.length) {

                    const start = offset + len;
                    let chars = 1, words = 0;
                    while (chars < MAX_CONTEXT_CHARS && words < MAX_CONTEXT_WORDS && start + chars < (desc.length - 1)) {
                        ++chars;
                        if (desc[start + chars] === ' ') ++words;
                    }

                    spans.push({
                        text: desc.substring(start, start + chars),
                        clazz: 'match-normal'
                    });
                }

                return spans;
            }
        }

        return [];
    }

    selectItem(ev) {
        ev.preventDefault();

        this.model.setValue(ev.item.term);
    }

    openAltMenu() {
        // ngb-typeahead doesn't (at least of 4.x) offer any way to programatically open the drop
        // down.  The technique below ("fake" an input event on the control) seems to be reasonably
        // robust in practice but isn't especially Angular.  The alternative would probably be to
        // build our own typeahead component...

        this.vocabInput.nativeElement.value = '';
        const event = document.createEvent('Event');
        event.initEvent('input', false, false);
        this.vocabInput.nativeElement.dispatchEvent(event);
    }
}

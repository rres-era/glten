import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';

import { RotationPhase, SelectableItem } from '../types';
import { ExperimentFormModelService } from '../experiment-form-model.service';

@Component({
  selector: 'glt-rotation-editor',
  templateUrl: './rotation-editor.component.html',
  styleUrls: ['./rotation-editor.component.css']
})
export class RotationEditorComponent implements OnInit {
    phasingValues = [
        "Complete",
        "Incomplete"
    ]

    @Input() rotationModel: FormGroup;
    @Input() cropModels: FormArray;
    @Output() onRemove: EventEmitter<any> = new EventEmitter();

    gpCache: FormGroup[][] = null;

    constructor(private modelService: ExperimentFormModelService) { }

    ngOnInit() {
        /* 
         * The crop list is itself editable, so we need to watch it for changes, and 
         * invalidate any references to crops which are deleted.
         */

        this.cropModels.valueChanges.subscribe(
            (vc) => {
                const selectableCrops = this.selectableCrops;
                for (const phaseModel of this.phases.controls) {
                    const pmCropControl = phaseModel.get('cropId');
                    if (!selectableCrops.some((sc) => sc.id === pmCropControl.value)) {
                        pmCropControl.setValue(null);
                        pmCropControl.markAsDirty();
                    }
                }    
            }
        );
    }

    get phasing() {
        return this.rotationModel.get('phasing') as FormControl;
    }

    get phasingLabel() {
        return this.phasing.value || 'Select one...';
    }

    setPhasing(value) {
        this.phasing.setValue(value);
        this.phasing.markAsDirty();
    }

    get phases() {
        return this.rotationModel.get('phases') as FormArray;
    }

    addPhase() {
        this.phases.push(
            this.modelService.rotationPhaseToFormModel(new RotationPhase())
        );
        this.phases.markAsDirty();
    }

    removePhase(idx: number) {
        this.phases.removeAt(idx);
        this.phases.markAsDirty();
    }

    get selectableCrops(): SelectableItem[] {
        return this.modelService.selectableCrops(this.cropModels);
    }

    cropLabelForPhase(phaseModel: FormGroup): string {
        const cropId = phaseModel.get('cropId').value;
        if (cropId === null) {
            return 'Select crop...'
        } else {
            const crop = this.selectableCrops.find((sc) => sc.id === cropId);
            if (crop) {
                return crop.name;
            } else {
                return '???';
            }
        }
    }

    setCropForPhase(phaseModel: FormGroup, cropID: number) {
        const cropControl = phaseModel.get('cropId');
        cropControl.setValue(cropID);
        cropControl.markAsDirty();
    }

    get groupedPhases(): FormGroup[][] {
        const groups = [];
        let currentGroup = [];
        for (const phaseModel of (this.phases.controls as FormGroup[])) {
            if (!this.isSamePhase(phaseModel)) {
                if (currentGroup.length > 0) {
                    groups.push(currentGroup);
                    currentGroup = [];
                }
            }
            currentGroup.push(phaseModel);
        }
        if (currentGroup.length > 0) {
            groups.push(currentGroup);
        }

        return groups;
    }

    isSamePhase(phaseModel: FormGroup) {
        return phaseModel.get('samePhaseAsPrevious').value;
    }

    toggleSamePhase(phaseModel: FormGroup) {
        const samePhaseControl = phaseModel.get('samePhaseAsPrevious');
        samePhaseControl.setValue(!samePhaseControl.value);
        samePhaseControl.markAsDirty();
    }

    trackByIndex(idx, item) {
        return idx;
    }
}
import { Component, Input } from '@angular/core';

import { ExperimentDesc } from '../types';
import { MapConfigService } from '../map-config.service';

@Component({
    selector: 'glt-site-view',
    templateUrl: './site-view.component.html',
    styleUrls: ['./site-view.component.css']
})
export class SiteViewComponent  {
    @Input() expt: ExperimentDesc;
    @Input() experimentId: number;

    baseLayers: any;

    constructor(private mapConfigService: MapConfigService) {
        this.baseLayers = this.mapConfigService.makeBaseLayersMap();
    }

    get mapOptions(): any {
        return this.mapConfigService.mapConfig(
            14,
            this.expt
        );
    }

    get mapLayers(): any {
        return [
            this.mapConfigService.experimentLayer([this.expt])
        ];
    }

    onMapReady(map) {
        setTimeout(() => {
            map.invalidateSize(true);
        }, 0);

        this.mapConfigService.addAttributionControl(map);
    }
}

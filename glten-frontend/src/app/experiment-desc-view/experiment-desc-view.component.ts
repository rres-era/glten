import { Component, Input } from '@angular/core';

import { ExperimentDesc } from '../types';
import { TermLabelsService } from '../term-labels.service';

@Component({
    selector: 'glt-experiment-desc-view',
    templateUrl: './experiment-desc-view.component.html',
    styleUrls: ['./experiment-desc-view.component.css']
})
export class ExperimentDescViewComponent  {
    @Input() expt: ExperimentDesc;
    @Input() experimentId: number;

    constructor(private termLabels: TermLabelsService) { }
}

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';

import deepEqual from 'deep-equal';

import { Crop, SelectableItem, Material } from '../types';
import { ExperimentFormModelService } from '../experiment-form-model.service';

@Component({
  selector: 'glt-measurement-editor',
  templateUrl: './measurement-editor.component.html',
  styleUrls: ['./measurement-editor.component.css']
})
export class MeasurementEditorComponent implements OnInit {

    @Input() measurementModel: FormGroup;
    @Input() cropModels: FormArray;
    @Output() onRemove: EventEmitter<any> = new EventEmitter();

    constructor(private modelService: ExperimentFormModelService) { }

    ngOnInit() {
        /* 
         * The crop list is itself editable, so we need to watch it for changes, and 
         * invalidate any references to crops which are deleted.
         */

        this.cropModels.valueChanges.subscribe(
            (newCrops) => {
                const mmControl = this.measurementModel.get('material');

                const selectableCrops = this.selectableMaterials;
                if (!selectableCrops.some((sc) => deepEqual(sc.id, mmControl.value))) {
                    mmControl.setValue(new Material(null, null));
                    mmControl.markAsDirty();
                }
            }
        );
    }

    setMaterial(material: Material) {
        const materialControl = this.measurementModel.get('material');
        materialControl.setValue(material);
        materialControl.markAsDirty();
    }

    get selectableMaterials(): SelectableItem[] {
        return [
            new SelectableItem(new Material(null, null), 'Not specified'),
            new SelectableItem(new Material('Soil', null), 'Soil'),
            new SelectableItem(new Material('AllCrops', null), 'All crops'),
            ...this.modelService.selectableCrops(this.cropModels).map((cropItem) => new SelectableItem(
                new Material('SpecifiedCrop', cropItem.id),
                cropItem.name
            ))
        ];
    }

    get materialLabel() {
        const material = this.measurementModel.get('material').value;
        // No special-case for null because it's a selectable option.
        const item = this.selectableMaterials.find((sc) => deepEqual(sc.id, material));
        if (item) {
            return item.name;
        } else {
            return '???';
        }
    }
}

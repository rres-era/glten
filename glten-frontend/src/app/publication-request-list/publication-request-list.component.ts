import { Component, OnInit } from '@angular/core';

import { ExperimentPublicationRequest } from "../types";
import { CurationService } from "../curation.service";

@Component({
    selector: 'glt-publication-request-list',
    templateUrl: './publication-request-list.component.html',
    styleUrls: ['./publication-request-list.component.css']
})
export class PublicationRequestListComponent implements OnInit {

    requests: ExperimentPublicationRequest[] = [];
    err: Error;

    constructor(private experimentService: CurationService) { }

    ngOnInit() {
        this.loadRequests()
    }

    loadRequests() {
        this.experimentService.getPendingPublicationRequests()
            .subscribe(
                (pubreqs) => {
                    this.err = null;
                    this.requests = pubreqs;
                },
                (err) => {
                    this.err = err;
                    this.requests = [];
                }
            );
    }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators, ValidationErrors } from '@angular/forms';

import { tap } from 'rxjs/operators';

import { ExperimentPublicationRequest, ExperimentDesc } from "../types";
import { CurationService } from "../curation.service";

@Component({
  selector: 'glt-publication-request-review',
  templateUrl: './publication-request-review.component.html',
  styleUrls: ['./publication-request-review.component.css']
})
export class PublicationRequestReviewComponent implements OnInit {

    request: ExperimentPublicationRequest;
    desc: ExperimentDesc;

    model: FormGroup;

    constructor(
        private experimentService: CurationService,
        private route: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder
    ) {
        this.approve = this.approve.bind(this);
        this.reject = this.reject.bind(this);
    }

    get currentExperiment() {
        return parseInt(this.route.snapshot.paramMap.get('id'));
    }

    ngOnInit() {
        this.loadRequest();

        this.model = this.fb.group({
            comment: ['', [Validators.required]]
        });
    }

    loadRequest() {
        this.experimentService.getPublicationRequest(this.currentExperiment)
            .subscribe(
                (pubreq) => {
                    this.request = pubreq;
                    this.desc = pubreq.experiment_desc;
                },
                (err) => {
                    console.log(err);
                    alert(err.message);
                }
            );
    }

    approve() {
        return this.experimentService.postPublicationRequestApprove(this.currentExperiment)
            .pipe(
                tap({
                    next: (_) => {
                        // Success
                        this.router.navigate(['/review-requests'])
                    },
                    error:(err) => {
                        console.log(err);
                        alert(err.message);
                    }
                })
            );
    }

    reject() {
        return this.experimentService.postPublicationRequestReject(
            this.currentExperiment,
            this.model.get('comment').value
        )
            .pipe(
                tap({
                    next: (_) => {
                        // Success
                        this.router.navigate(['/review-requests'])
                    },
                    error:  (err) => {
                        console.log(err);
                        alert(err.message);
                    }
                })
            );
    }

}

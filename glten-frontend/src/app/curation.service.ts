import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { CookieService } from 'ngx-cookie';

import { Experiment, ExperimentDesc, ExperimentDescBase, ExperimentPublicationRequest } from './types';
import { LoginRedirectService } from './login-redirect.service';

@Injectable({
    providedIn: 'root'
})
export class CurationService {
    private apiBase = '/api/v0';
    private httpHeaders: HttpHeaders;

    constructor(
        private http: HttpClient,
        private cookies: CookieService,
        private login: LoginRedirectService
    ) {
        let token = null;
        try {
            token = cookies.get('csrftoken');
        } catch (err) {
            // This will fail when running on server because no cookies.  Since we should
            // never need to POST from a server-side rendering op, just ignore.
        }

        this.httpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'X-CSRFToken': token
        });
    }

    private makeErrorHandler<T> (operation, onFailure?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(onFailure as T);
        };
    }

    getExperiments(): Observable<Experiment[]> {
        const url = `${this.apiBase}/experiment`
        return this.http.get<Experiment[]>(url)
            .pipe(
                tap(
                    null,
                    (err) => {
                        console.log(err);
                        if (err.status && err.status === 403) {
                            this.login.redirectViaLogin();
                        }
                    }
                )
            );
    }

    createExperiment() {
        const url = `${this.apiBase}/experiment`;
        return this.http.post<Experiment[]>(url, {}, {headers: this.httpHeaders})
            .pipe(
                tap(
                    null,
                    (err) => console.log(err)
                )
            );
    }

    getExperimentHistory(experimentID: number): Observable<ExperimentDescBase[]> {
        const url = `${this.apiBase}/experiment/${experimentID}`;
        return this.http.get<ExperimentDescBase[]>(url)
            .pipe(
                tap(
                    null,
                    (err) => {
                        console.log(err);
                        if (err.status && err.status === 403) {
                            this.login.redirectViaLogin();
                        }
                    }
                )
            );
    }

    getExperimentDesc(experimentID: number, experimentVersion: number = null) {
        let url;
        if (experimentVersion === null) {
            url = `${this.apiBase}/experiment/${experimentID}/desc`;
        } else {
            url = `${this.apiBase}/experiment/${experimentID}/desc/${experimentVersion}`;
        }

        return this.http.get<ExperimentDesc>(url)
            .pipe(
                tap(
                    null,
                    (err) => {
                        console.log(err);
                        if (err.status && err.status === 403) {
                            this.login.redirectViaLogin();
                        }
                    }
                )
            );
    }

    postExperimentDesc(experimentID: number, desc: ExperimentDesc): Observable<ExperimentDesc> {
        const url = `${this.apiBase}/experiment/${experimentID}/desc`;

        return this.http.post<ExperimentDesc>(url, desc, {headers: this.httpHeaders})
            .pipe(
                tap(
                    null,
                    (err) => console.log(err)
                )
            );
    }

    postPublicationRequest(experimentID: number): Observable<ExperimentPublicationRequest> {
    	const url = `${this.apiBase}/experiment/${experimentID}/publish`;

    	return this.http.post<ExperimentPublicationRequest>(url, {}, {headers: this.httpHeaders})
    		.pipe(
    			tap(
    				null,
    				(err) => console.log(err)
    			)
    		);
    }

    getPendingPublicationRequests(): Observable<ExperimentPublicationRequest[]> {
        const url = `${this.apiBase}/pubreq`;

        return this.http.get<ExperimentPublicationRequest[]>(url)
            .pipe(
                tap(
                    null,
                    (err) => {
                        console.log(err);
                        if (err.status && err.status === 403) {
                            this.login.redirectViaLogin();
                        }
                    }
                )
            );
    }

    getPublicationRequest(experimentID: number): Observable<ExperimentPublicationRequest> {
        const url = `${this.apiBase}/pubreq/${experimentID}`

        return this.http.get<ExperimentPublicationRequest>(url)
            .pipe(
                tap(
                    null,
                    (err) => {
                        console.log(err);
                        if (err.status && err.status === 403) {
                            this.login.redirectViaLogin();
                        }
                    }
                )
            );
    }

    postPublicationRequestApprove(experimentID: number): Observable<ExperimentPublicationRequest> {
        const url = `${this.apiBase}/pubreq/${experimentID}/approve`

        return this.http.post<ExperimentPublicationRequest>(url, {}, {headers: this.httpHeaders})
            .pipe(
                tap(
                    null,
                    (err) => console.log(err)
                )
            );
    }

    postPublicationRequestReject(experimentID: number, comment: string): Observable<ExperimentPublicationRequest> {
        const url = `${this.apiBase}/pubreq/${experimentID}/reject`

        return this.http.post<ExperimentPublicationRequest>(
            url,
            {comment: comment}, 
            {headers: this.httpHeaders}
        ).pipe(
            tap(
                null,
                (err) => console.log(err)
            )
        );
    }

    postUnpublish(experimentID: number): Observable<ExperimentPublicationRequest> {
        const url = `${this.apiBase}/experiment/${experimentID}/unpublish`;

        return this.http.post<ExperimentPublicationRequest>(
            url,
            {comment: 'Unpublishing...'}, 
            {headers: this.httpHeaders}
        ).pipe(
            tap(
                null,
                (err) => console.log(err)
            )
        );
    }


    postDelete(experimentID: number): Observable<Experiment> {
        const url = `${this.apiBase}/experiment/${experimentID}/delete`;

        return this.http.post<Experiment>(
            url,
            {}, 
            {headers: this.httpHeaders}
        ).pipe(
            tap(
                null,
                (err) => console.log(err)
            )
        );
    }
}

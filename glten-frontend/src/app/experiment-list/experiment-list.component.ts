import { Component, OnInit } from '@angular/core';
import { tap } from 'rxjs/operators';

import { Experiment } from "../types";
import { CurationService } from '../curation.service';

@Component({
    selector: 'glt-experiment-list',
    templateUrl: './experiment-list.component.html',
    styleUrls: ['./experiment-list.component.css']
})
export class ExperimentListComponent implements OnInit {
    experiments: Experiment[] = null;
    err: Error;

    constructor(private experimentService: CurationService) { }

    ngOnInit() {
        this.loadExperiments();
    }

    loadExperiments() {
        this.experiments = null;
        this.experimentService.getExperiments()
            .subscribe(
                (expts) => {
                    this.err = null;
                    this.experiments = expts;
                },
                (err) => {
                    this.err = err;
                    this.experiments = [];
                }
            );
    }

    createExperiment() {
        this.experimentService.createExperiment()
            .subscribe(
                (_) => {
                    // next
                    this.loadExperiments()
                },
                (err) => {
                    // error
                    alert(err.message);
                },
                () => {
                    // complete
                });
    }

    makeDeleteAction(experimentID) {
        return () => {
            if (window.confirm('Are you sure you want to delete this experiment?')) {
                return this.experimentService.postDelete(experimentID)
                    .pipe(
                        tap(
                            () => this.loadExperiments(),
                            (err) => window.alert(err.message)
                        )
                    );
            }
        }
    }
}

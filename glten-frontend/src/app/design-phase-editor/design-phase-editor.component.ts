import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, mergeMap } from 'rxjs/operators';

import { VocabularyTerm, Factor, Crop, Rotation, FactorCombination, ExperimentMeasurement } from '../types';
import { ExperimentFormModelService } from '../experiment-form-model.service';
import { VocabLookupService } from '../vocab-lookup.service';

@Component({
  selector: 'glt-design-phase-editor',
  templateUrl: './design-phase-editor.component.html',
  styleUrls: ['./design-phase-editor.component.css']
})
export class DesignPhaseEditorComponent {
    @Input() phaseModel: FormGroup;
    @Input() canRemove: boolean;
    @Output() onRemove: EventEmitter<any> = new EventEmitter();

    constructor(private vocabLookup: VocabLookupService, private modelService: ExperimentFormModelService) { }

    get factors() {
        return this.phaseModel.get('factors') as FormArray;
    }

    addFactor() {
        this.factors.push(this.modelService.factorToFormModel(new Factor()));
        this.factors.markAsDirty();
    }

    removeFactor(index: number) {
        this.factors.removeAt(index);
        this.factors.markAsDirty();
    }

    get crops() {
        return this.phaseModel.get('crops') as FormArray;
    }

    addCrop() {
        this.crops.push(this.modelService.cropToFormModel(new Crop()));
    }

    removeCrop(idx: number) {
        this.crops.removeAt(idx);
        this.crops.markAsDirty();
    }

    get rotations() {
        return this.phaseModel.get('rotations') as FormArray;
    }

    addRotation() {
        this.rotations.push(this.modelService.rotationToFormModel(new Rotation()));
        this.rotations.markAsDirty();
    }

    removeRotation(idx: number) {
        this.rotations.removeAt(idx);
        this.rotations.markAsDirty();
    }

    get factorCombinations() {
        return this.phaseModel.get('factorCombinations') as FormArray;
    }

    addFactorCombination() {
        this.factorCombinations.push(
            this.modelService.combinationToFormModel(new FactorCombination())
        );
        this.factorCombinations.markAsDirty();
    }

    removeFactorCombination(idx: number) {
        this.factorCombinations.removeAt(idx);
        this.factorCombinations.markAsDirty();
    }

    get measurements() {
        return this.phaseModel.get('measurements') as FormArray;
    }

    addMeasurement() {
        this.measurements.push(
            this.modelService.measurementToFormModel(new ExperimentMeasurement())
        );
        this.measurements.markAsDirty();
    }

    removeMeasurement(idx: number) {
        this.measurements.removeAt(idx);
        this.measurements.markAsDirty();
    }
}

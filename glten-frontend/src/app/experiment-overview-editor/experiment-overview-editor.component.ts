import { Component, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { TermLabelsService } from '../term-labels.service';

@Component({
    selector: 'glt-experiment-overview-editor',
    templateUrl: './experiment-overview-editor.component.html',
    styleUrls: ['./experiment-overview-editor.component.css']
})
export class ExperimentOverviewEditorComponent {
    @Input() overviewModel: FormGroup;

    constructor(public termLabels: TermLabelsService) { }

    get policyStatusControl(): FormControl {
        return this.overviewModel.get('dataPolicyStatus') as FormControl;
    }

    get dataPolicyLabel(): string {
        return this.termLabels.dataPolicyLabel(this.policyStatusControl.value);
    }

    setDataPolicyStatus(status) {
        this.policyStatusControl.setValue(status);
        this.policyStatusControl.markAsDirty();
    }

    get licenseStatusControl(): FormControl {
        return this.overviewModel.get('dataLicenseStatus') as FormControl;
    }

    get dataLicenseLabel(): string {
        return this.termLabels.dataLicenseLabel(this.licenseStatusControl.value);
    }

    setDataLicenseStatus(status) {
        this.licenseStatusControl.setValue(status);
        this.licenseStatusControl.markAsDirty();
    }

    get statementStatusControl(): FormControl {
        return this.overviewModel.get('dataStatementStatus') as FormControl;
    }

    get dataStatementLabel(): string {
        return this.termLabels.dataStatementLabel(this.statementStatusControl.value);
    }

    setDataStatementStatus(status) {
        this.statementStatusControl.setValue(status);
        this.statementStatusControl.markAsDirty();
    }
}

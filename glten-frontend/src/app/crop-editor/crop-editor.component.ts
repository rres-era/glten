import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'glt-crop-editor',
  templateUrl: './crop-editor.component.html',
  styleUrls: ['./crop-editor.component.css']
})
export class CropEditorComponent {

    @Input() cropModel : FormGroup;
    @Output() onRemove: EventEmitter<any> = new EventEmitter();

    constructor() { }

}

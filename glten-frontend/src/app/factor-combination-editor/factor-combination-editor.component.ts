import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';

import { Crop, FactorCombinationMember, SelectableItem } from '../types';
import { ExperimentFormModelService } from '../experiment-form-model.service';

@Component({
  selector: 'glt-factor-combination-editor',
  templateUrl: './factor-combination-editor.component.html',
  styleUrls: ['./factor-combination-editor.component.css']
})
export class FactorCombinationEditorComponent implements OnInit {
    @Input() combinationModel: FormGroup;
    @Input() cropModels: FormArray;
    @Input() factorModels: FormArray;
    @Output() onRemove: EventEmitter<any> = new EventEmitter();

    constructor(private modelService: ExperimentFormModelService) { }

    ngOnInit() {
        /*
         * Crop and factor lists are editable, so watch for changes and invalidate
         * any references to deleted crops
         */

        this.cropModels.valueChanges.subscribe(
            (newCrops) => {
                const selectableCrops = this.selectableCrops;
                for (const memberModel of this.members.controls) {
                    const mmCropControl = memberModel.get('cropId');
                    if (!selectableCrops.some((sc) => sc.id === mmCropControl.value)) {
                        mmCropControl.setValue(null);
                        mmCropControl.markAsDirty();
                    }
                }
            }
        );

        this.factorModels.valueChanges.subscribe(
            (_vc) => {
                const selectableFactors = this.selectableFactors;
                for (const memberModel of this.members.controls) {
                    const mmFactorControl = memberModel.get('factorId');
                    const mmFactorLevelControl = memberModel.get('factorLevelId');
                    if (!selectableFactors.some((sf) => sf.id === mmFactorControl.value)) {
                        mmFactorControl.setValue(null);
                        mmFactorControl.markAsDirty();
                        mmFactorLevelControl.setValue(null);
                        mmFactorLevelControl.markAsDirty();
                    }

                    const levelId = mmFactorLevelControl.value;
                    if (levelId !== null) {
                        const selectableLevels = this.selectableLevelsForMember(memberModel as FormGroup);
                        if (!selectableLevels.some((sl) => sl.id === levelId)) {
                            mmFactorLevelControl.setValue(null);
                            mmFactorLevelControl.markAsDirty();
                        }
                    }
                }
            }
        );
    }

    get members() {
        return this.combinationModel.get('members') as FormArray;
    }

    addMember() {
        this.members.push(
            this.modelService.combinationMemberToFormModel(new FactorCombinationMember())
        );
        this.members.markAsDirty();
    }

    removeMember(idx: number) {
        this.members.removeAt(idx);
        this.members.markAsDirty();
    }

    get selectableCrops(): SelectableItem[] {
        return [
            new SelectableItem(null, 'Not specified'),
            new SelectableItem(Crop.ALL_CROPS, 'All crops'),
            ...this.modelService.selectableCrops(this.cropModels)
        ];
    }

    get selectableFactors(): SelectableItem[] {
        return this.modelService.selectableFactors(this.factorModels);
    }

    setFactorForMember(memberModel: FormGroup, factorId: number) {
        const factorControl = memberModel.get('factorId');
        factorControl.setValue(factorId);
        factorControl.markAsDirty();
    }

    factorLabelForMember(memberModel: FormGroup) {
        const factorId = memberModel.get('factorId').value;
        if (factorId === null) {
            return 'Select factor...';
        } else {
            const factor = this.selectableFactors.find((sf) => sf.id === factorId);
            if (factor) {
                return factor.name;
            } else {
                return '???';
            }
        }
    }

    setCropForMember(memberModel: FormGroup, cropId: number) {
        const cropControl = memberModel.get('cropId');
        cropControl.setValue(cropId);
        cropControl.markAsDirty();
    }

    cropLabelForMember(memberModel: FormGroup) {
        const cropId = memberModel.get('cropId').value;
        // No special-case for null because it's a selectable option.
        const crop = this.selectableCrops.find((sc) => sc.id === cropId);
        if (crop) {
            return crop.name;
        } else {
            return '???';
        }
    }

    selectableLevelsForMember(memberModel: FormGroup): SelectableItem[] {
        const factorId = memberModel.get('factorId').value;
        if (factorId === null) {
            return [];
        } else {
            const factorModel = this.factorModels.controls.find((fm) => fm.get('id').value === factorId);
            return this.modelService.selectableFactorLevels(factorModel.get('levels') as FormArray);
        }
    }

    factorLevelLabelForMember(memberModel: FormGroup) {
        const factorId = memberModel.get('factorId').value,
              levelId = memberModel.get('factorLevelId').value;

        if (factorId === null) {
            return 'Select factor first';
        } else if (levelId === null) {
            return 'Select level...'
        } else {
            const factorModel = this.factorModels.controls.find((fm) => fm.get('id').value === factorId);
            const levels = this.modelService.selectableFactorLevels(factorModel.get('levels') as FormArray);
            const level = levels.find((sl) => sl.id === levelId);
            if (level) {
                return level.name;
            } else {
                return '???';
            }
        }
    }

    setLevelForMember(memberModel: FormGroup, levelId: number) {
        const levelControl = memberModel.get('factorLevelId');
        levelControl.setValue(levelId);
        levelControl.markAsDirty();
    }
}

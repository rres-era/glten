import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExperimentListComponent } from './experiment-list/experiment-list.component';
import { ExperimentEditorComponent } from './experiment-editor/experiment-editor.component';
import { PublicationRequestListComponent } from './publication-request-list/publication-request-list.component';
import { PublicationRequestReviewComponent } from './publication-request-review/publication-request-review.component';
import { PublicExperimentListComponent } from './public-experiment-list/public-experiment-list.component';
import { ExperimentViewComponent } from './experiment-view/experiment-view.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';

import { CanEditorDeactivateGuard } from './can-editor-deactivate.guard'

const routes: Routes = [
    { path: '', component: HomeComponent},
    { path: 'my-experiments', component: ExperimentListComponent },
    { path: 'my-experiments/:id/edit', component: ExperimentEditorComponent, canDeactivate: [CanEditorDeactivateGuard] },
    { path: 'review-requests', component: PublicationRequestListComponent },
    { path: 'review-requests/:id', component: PublicationRequestReviewComponent },
    { path: 'experiments', component: PublicExperimentListComponent },
    { path: 'experiments/:id', component: ExperimentViewComponent },
	{ path: 'about', component: AboutComponent},
    { path: '**', redirectTo: '/'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

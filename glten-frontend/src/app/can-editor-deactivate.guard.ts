import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';

import { ExperimentEditorComponent } from './experiment-editor/experiment-editor.component';

@Injectable({
    providedIn: 'root'
})
export class CanEditorDeactivateGuard implements CanDeactivate<ExperimentEditorComponent> {
    canDeactivate(editor: ExperimentEditorComponent) {
        if (editor.model && editor.model.dirty) {
            // TODO switch to an async confirmation mechanism (and return a Promise)
            return window.confirm('Unsaved changes, do you really want to leave?');
        } else {
            return true;
        }
    }
  
}

import { Component, OnInit } from '@angular/core';

import { ExperimentDescBase } from '../types';
import { PublishedExperimentsService, PublishedExperimentsResponse } from '../published-experiments.service';
import { MapConfigService } from '../map-config.service';

@Component({
  selector: 'glt-thumbnail-map',
  templateUrl: './thumbnail-map.component.html',
  styleUrls: ['./thumbnail-map.component.css']
})
export class ThumbnailMapComponent implements OnInit {

    experiments: ExperimentDescBase[];
    err;
    exptLayers = [];
    baseLayers: any;

    constructor(
        private experimentsService: PublishedExperimentsService,
        private mapConfigService: MapConfigService,
    ) {
        this.baseLayers = this.mapConfigService.makeBaseLayersMap();
    }

    ngOnInit() {
        this.loadExperiments();
    }

    loadExperiments() {
        this.experimentsService.getExperiments(null)
            .subscribe(
                (resp) => {
                    this.err = null;
                    this.experiments = resp.experiments;
                    this.exptLayers = [
                        this.mapConfigService.experimentLayer(this.experiments || [])
                    ]
                },
                (err) => {
                    this.err = err;
                    this.experiments = [];
                    this.exptLayers = [];
                }
            );
    }

    get mapOptions(): any {
        return this.mapConfigService.mapConfig();
    }

    onMapReady(map) {
        this.mapConfigService.addAttributionControl(map);
    }
}

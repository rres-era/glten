import { Directive, HostBinding } from '@angular/core';
import { NgControl } from "@angular/forms";

@Directive({
  selector: '[glt-is-invalid]'
})
export class IsInvalidDirective {

    constructor(private control : NgControl) { }

    @HostBinding('class.is-invalid')
    get isInvalid() {
        return this.control && this.control.control && this.control.control.enabled && !this.control.control.valid;
    }

}

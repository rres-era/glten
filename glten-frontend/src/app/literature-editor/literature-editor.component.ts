import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'glt-literature-editor',
    templateUrl: './literature-editor.component.html',
    styleUrls: ['./literature-editor.component.css']
})
export class LiteratureEditorComponent {
    @Input() literatureModel: FormGroup;
    @Output() onRemove: EventEmitter<any> = new EventEmitter();

    constructor() { }

}

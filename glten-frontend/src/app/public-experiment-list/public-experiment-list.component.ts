import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { tap } from 'rxjs/operators';

import { ExperimentDescBase } from '../types';
import { PublishedExperimentsService, PublishedExperimentsResponse } from '../published-experiments.service';
import { CurationService } from '../curation.service';
import { MapConfigService } from '../map-config.service';
import { UserService } from '../user.service';

@Component({
  selector: 'glt-public-experiment-list',
  templateUrl: './public-experiment-list.component.html',
  styleUrls: ['./public-experiment-list.component.css']
})
export class PublicExperimentListComponent implements OnInit {
    experiments: ExperimentDescBase[];
    err;
    wasQuery: boolean;
    total_experiments;
    total_matches;

    baseLayers: any;

    exptSearchControl = new FormControl('');
    exptLayers = [];

    userCanUnpublish = false;

    constructor(
        private experimentsService: PublishedExperimentsService,
        private curationService: CurationService,
        private mapConfigService: MapConfigService,
        private userService: UserService
    ) {
        this.baseLayers = this.mapConfigService.makeBaseLayersMap();
    }

    ngOnInit() {
        this.loadExperiments();
        this.userService.getCurrentUser().subscribe((user) => {
            if (user && user.is_publisher) {
                this.userCanUnpublish = true;
            }
        });
    }

    loadExperiments(query: string = null) {
        this.experimentsService.getExperiments(query)
            .subscribe(
                (resp) => {
                    this.err = null;
                    this.experiments = resp.experiments;
                    this.exptLayers = [
                        this.mapConfigService.experimentLayer(this.experiments || [])
                    ]
                    this.total_experiments = resp.total_experiments;
                    this.total_matches = resp.total_matches;
                    this.wasQuery = (query && query.length > 0);
                },
                (err) => {
                    this.err = err;
                    this.experiments = [];
                    this.exptLayers = [];
                }
            );
    }

    get mapOptions(): any {
        return this.mapConfigService.mapConfig();
    }

    exptSearchClicked(ev) {
        this.loadExperiments(this.exptSearchControl.value);
    }

    onMapReady(map) {
        this.mapConfigService.addAttributionControl(map);
    }

    makeUnpublishAction(experimentID) {
        return () => {
            if (window.confirm('Are you sure you want to unpublish this experiment?')) {
                return this.curationService.postUnpublish(experimentID)
                    .pipe(
                        tap(
                            () => this.loadExperiments(this.exptSearchControl.value),
                            (err) => window.alert(err.message)
                        )
                    );
            }
        }
    }
}

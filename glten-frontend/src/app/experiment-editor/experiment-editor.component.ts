import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormArray, FormGroup, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { defer, Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { NgbTabset } from '@ng-bootstrap/ng-bootstrap';

import { ExperimentDesc, ExperimentDescBase, ExperimentPeriod, Person, Organization, Contact,
    ExperimentLiterature } from '../types';
import { CurationService } from '../curation.service';
import { ExperimentFormModelService } from '../experiment-form-model.service';

@Component({
    selector: 'glt-experiment-editor',
    templateUrl: './experiment-editor.component.html',
    styleUrls: ['./experiment-editor.component.css'],
    encapsulation: ViewEncapsulation.None  // TODO required for current tab-set height-fixing CSS.
})
export class ExperimentEditorComponent implements OnInit {
    autosaveTime = 5;  // minutes
    baseDesc: ExperimentDesc;
    model: FormGroup;
    err: Error;
    publicationRequested: boolean = false;

    editHistory: ExperimentDescBase[];
    editHistoryErr;

    scrollPositionsByTab: {[s: string]: number} = {}

    @ViewChild(NgbTabset, { static: false }) tabs: NgbTabset;

    saveTimeout: any;

    constructor(
        private experimentsService: CurationService,
        private modelService: ExperimentFormModelService,
        private route: ActivatedRoute,
        private elementRef: ElementRef
    ) {
        this.onBeforeUnload = this.onBeforeUnload.bind(this);
        this.requestPublication = this.requestPublication.bind(this);
    }

    get currentExperiment() {
        return parseInt(this.route.snapshot.paramMap.get('id'));
    }

    ngOnInit() {
        this.loadDesc(this.currentExperiment, null);

        window.addEventListener('beforeunload', this.onBeforeUnload, false);
    }

    ngOnDestroy() {
        if (this.saveTimeout) {
            clearTimeout(this.saveTimeout);
            this.saveTimeout = null;
        }

         window.removeEventListener('beforeunload', this.onBeforeUnload, false);
    }

    onBeforeUnload(ev) {
        if (this.model && this.model.dirty) {
            if (!window.confirm('Unsaved changes, are you sure you want to leave')) {
                ev.preventDefault();
                ev.returnValue = '';  // Required by Chrome, per https://developer.mozilla.org/en-US/docs/Web/API/WindowEventHandlers/onbeforeunload
            }
        }
    }

    loadDesc(experiment, version) {
        this.experimentsService.getExperimentDesc(experiment, version)
            .subscribe(
                (desc) => {
                    this.err = null;
                    this.baseDesc = desc;
                    this.model = this.modelService.descToFormModel(desc)

                    this.model.statusChanges.subscribe(
                        (_) => {
                            if (this.publicationRequested) {
                                this.publicationRequested = false;
                            }

                            if (this.model.dirty && !this.saveTimeout) {
                                this.saveTimeout = setTimeout(() => {
                                    this.saveTimeout = null;
                                    if (this.model.dirty) {
                                        this.save();
                                    }
                                }, this.autosaveTime * 60 * 1000);
                            }
                        }
                    );
                },
                (err) => {
                   this.err = err;
                   this.baseDesc = null;
                   this.model = null;
                });
    }

    get desc(): ExperimentDesc {
        return this.modelService.modelToExperimentDesc(this.model);
    }

    get descJSON(): String {
        return JSON.stringify(this.desc, null, 2);
    }

    get contacts() {
        return this.model.get('contacts') as FormGroup;
    }

    get people() {
        return this.contacts.get('people') as FormArray;
    }

    addPerson() {
        this.people.push(this.modelService.personToFormModel(new Person()));
        this.people.markAsDirty();  // Adding a control doesn't auto-dirty
    }

    removePerson(idx: number) {
        this.people.removeAt(idx);
        this.people.markAsDirty();
    }

    get organizations() {
        return this.contacts.get('organizations') as FormArray;
    }

    addOrganization() {
        this.organizations.push(this.modelService.orgToFormModel(new Organization()));
        this.organizations.markAsDirty();
    }

    removeOrganization(idx: number) {
        this.organizations.removeAt(idx);
        this.organizations.markAsDirty();
    }

    get periods() {
        return this.model.get('periods') as FormArray;
    }

    removePeriod(idx: number) {
        this.periods.removeAt(idx);
        this.periods.markAsDirty();
    }

    get literature() {
        return this.model.get('literature') as FormArray;
    }

    addLiterature() {
        this.literature.push(this.modelService.literatureToFormModel(new ExperimentLiterature()));
        this.literature.markAsDirty();  // Adding a control doesn't auto-dirty
    }

    removeLiterature(idx: number) {
        this.literature.removeAt(idx);
        this.literature.markAsDirty();
    }

    get name() {
        return this.model.get('name') as FormControl;
    }

    save() {
        // Manual save before auto-save triggers.  Reset.
        if (this.saveTimeout) {
            clearTimeout(this.saveTimeout);
            this.saveTimeout = null;
        }

        defer(() => {
            // Disable controls during save, so that when we mark the form as pristine
            // afterwards, we know there haven't been intervening changes.
            this.model.disable();

            return this.experimentsService.postExperimentDesc(
                this.currentExperiment,
                this.desc
            )
        }).subscribe(
            (desc) => {
                // next

                // This looks a little strange, but seems to be the most reliable way to clear
                // the dirty flags on deeply-nested models.
                this.model.enable();
                this.model.reset(this.model.value);
                this.baseDesc = desc as ExperimentDesc;
            },
            (err) => {
                //error
                console.log(err);
                alert(err.message);
                this.model.enable();
            },
            () => {
                // this.model.enable();
            }
        );
    }

    tabChange(ev) {
        if (ev.nextId === '+') {
            ev.preventDefault();

            this.periods.push(
                this.modelService.periodToFormModel(new ExperimentPeriod())
            );
            this.periods.markAsDirty();

            const nextTab = `period-tab-${this.periods.length - 1}`;

            // There doesn't seem to be a straightforward way to hook when the tabset has been
            // updated.  A timeout appears to work reliably.  (NB. running a as a microtask, 
            // e.g. by hanging off a promise resolution, does not seem to be good enough).
            setTimeout(() => this.tabs.select(nextTab));
        } else {
            // Attempt to restore per-tab scroll positions when switching between tabs

            const tabContent = this.elementRef.nativeElement.querySelector('.tab-content');
            if (tabContent) {
                this.scrollPositionsByTab[ev.activeId] = tabContent.scrollTop;

                // Similarly, there is not way to hook *after* the tab transition has happened.
                // scrolling immediately breaks if the tab we are moving away from contains little
                // content (so we can't scroll down to the recorded position).  Once again, waiting
                // on a timeout gives the required delay without a visible glitch.
                setTimeout(() => {
                    tabContent.scroll({
                        top: this.scrollPositionsByTab[ev.nextId] || 0
                    });
                });
            }
        }
    }

    requestPublication(): Observable<any> {
        if (this.saveTimeout) {
            clearTimeout(this.saveTimeout);
            this.saveTimeout = null;
        }

        // Under certain circumstances, this.model.value seems to be clobbered by the
        // disable/enable cycle.  No obvious explanation -- may be an Angular bug.
        // To be sure, we cache this.model.value before we start...

        const prevValue = this.model.value;

    	return defer(() => {
    		this.model.disable();

            return this.experimentsService.postExperimentDesc(
                this.currentExperiment,
                this.desc
            )
        }).pipe(
        	switchMap((desc) => {
                this.model.reset(this.model.value);
                this.baseDesc = desc as ExperimentDesc;

                return this.experimentsService.postPublicationRequest(this.currentExperiment)
            }),
            tap({
                next: (pubreq) => {
                    // This looks a little strange, but seems to be the most reliable way to clear
                    // the dirty flags on deeply-nested models.

                    this.model.enable();
                    this.model.reset(prevValue);
                    this.publicationRequested = true;
                },
                error: (err) => {
                    console.log(err);
                    alert(err.message);
                    this.model.enable();
                    this.model.reset(prevValue);
                }
            })
    	);

        // Return an observable to interface with action-button.
    }

    historyOpenChange(newOpenState) {
        if (newOpenState) {
            this.editHistory = null;
            this.experimentsService.getExperimentHistory(this.currentExperiment)
                .subscribe(
                    (history) => {
                        this.editHistory = history;
                        this.editHistoryErr = null;
                    },
                    (err) => {
                        this.editHistory = null;
                        this.editHistoryErr = err;
                    }
                );
        }
    }

    historyItemClicked(item) {
        let message = 'Are you sure you want to load a saved version?';
        if (this.model.dirty) {
            message = message + ' Unsaved changes will be lost!'
        }
        if (window.confirm(message)) {
            this.loadDesc(item.experiment_id, item.id);
        }
    }
}
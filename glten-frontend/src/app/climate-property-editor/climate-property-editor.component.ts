import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'glt-climate-property-editor',
    templateUrl: './climate-property-editor.component.html',
    styleUrls: ['./climate-property-editor.component.css']
})
export class ClimatePropertyEditorComponent {
    @Input() model : FormGroup;
    @Output() onRemove: EventEmitter<any> = new EventEmitter();

    constructor() { }

}

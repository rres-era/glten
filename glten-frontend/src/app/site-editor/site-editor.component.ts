import { Component, Input } from '@angular/core';
import { FormGroup, FormArray, FormControl } from '@angular/forms';

import { ExperimentFormModelService } from '../experiment-form-model.service';
import { SiteSoilProperty, SiteClimateProperty } from '../types';

@Component({
    selector: 'glt-site-editor',
    templateUrl: './site-editor.component.html',
    styleUrls: ['./site-editor.component.css']
})
export class SiteEditorComponent {
    elevationUnitValues = ['Metres', 'Feet'];

    @Input() siteModel: FormGroup;

    constructor(private modelService: ExperimentFormModelService) { }

    get elevationUnitControl() : FormControl {
        return (this.siteModel.get('elevation') as FormGroup).get('unit') as FormControl;
    }

    get elevationUnitLabel() {
        return this.elevationUnitControl.value || 'Not specified';
    }

    setElevationUnit(unit: string) {
        this.elevationUnitControl.setValue(unit);
        this.elevationUnitControl.markAsDirty();
    }

    get soilProps() {
        return this.siteModel.get('soilProps') as FormArray;
    }

    addSoilProp() {
        this.soilProps.push(this.modelService.soilPropToFormModel(new SiteSoilProperty()));
        this.soilProps.markAsDirty();
    }

    removeSoilProp(idx: number) {
        this.soilProps.removeAt(idx);
        this.soilProps.markAsDirty();
    }

    get climateProps() {
        return this.siteModel.get('climateProps') as FormArray;
    }

    addClimateProp() {
        this.climateProps.push(this.modelService.climatePropToFormModel(new SiteClimateProperty()));
        this.climateProps.markAsDirty();
    }

    removeClimateProp(idx: number) {
        this.climateProps.removeAt(idx);
        this.climateProps.markAsDirty();
    }
}

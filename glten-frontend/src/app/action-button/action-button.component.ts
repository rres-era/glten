import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
    selector: 'glt-action-button',
    templateUrl: './action-button.component.html',
    styleUrls: ['./action-button.component.css']
})
export class ActionButtonComponent {
    busy: boolean = false;

    @Input() bsClass: string = "btn btn-outline-primary"
    @Input() disabled: boolean = false;
    @Input() action: () => Observable<any>;


    constructor() { }

    onClick(ev) {
        if (this.action && !this.busy) {
            this.busy = true;
            this.action().subscribe(
                (_) => {
                    // next
                    this.busy = false;
                },
                (_) => {
                    // err
                    this.busy = false;
                }
            );
        }
    }

}

export class Experiment {
    id: number;
    latest: ExperimentDescBase;
    published: ExperimentDescBase;
}

export class ExperimentDescBase {
    experiment_id: number;
    id: number;
    saved_time: string;

    name: string;
    site_name: string;

    site_centroid_latitude: number;
    site_centroid_longitude: number;

    people: Person[];
    organizations: Organization[];
}

export class ExperimentDesc extends ExperimentDescBase {
    local_identifier: string;
    url: string;

    experiment_type_term: string;
    experiment_type_label: string;

    start_year: number;
    end_year: number;
    ongoing: boolean;
    establishment_period_end: number;

    objective: string;
    description: string;

    periods: ExperimentPeriod[];

    // site

    site_name: string;
    site_local_code: string;
    site_geonames_id: string;
    site_doi: string;
    site_type_term: string;
    site_type_label: string;
    site_region: string;
    site_locality: string;
    site_country: string;
    site_elevation: number;
    site_elevation_unit: string;   // {'Metres', 'Feet'}
    site_slope: number;
    site_slope_aspect: string;
    site_visits_allowed: boolean;
    site_visiting_arrangements: string;
    site_history: string;
    site_management: string;

    site_soil_type_term: string;
    site_soil_type_label: string;
    site_soil_description: string;
    site_soil_properties: SiteSoilProperty[];

    site_climatic_type_term: string;
    site_climatic_type_label: string;
    site_climate_description: string;
    site_climate_properties: SiteClimateProperty[];

    // data

    farm_operations_data: boolean;
    sample_archive: boolean;
    samples_available: boolean;

    data_statement_term: string;
    data_statement_label: string;
    data_license_term: string;
    data_license_label: string;
    data_url: string;
    data_policy_status: string;
    data_policy_url: string;
    data_access_statement: string;
    data_contact: string;

    literature: ExperimentLiterature[];
}

export class ExperimentPeriod {
    start_year: number;
    end_year: number;
    ongoing: boolean;
    name: string;
    description: string;
    design_description: string;
    factorial: boolean;
    design_type_term: string;
    design_type_label: string;

    number_of_blocks: number;
    number_of_plots: number;
    number_of_subplots: number;
    number_of_replicates: number;
    number_of_harvests_per_year: number;

    factors: Factor[];
    crops: Crop[];
    rotations: Rotation[];
    factor_combinations: FactorCombination[];
    measurements: ExperimentMeasurement[];
}

export class Factor {
    id: number;
    factor_term: string;
    factor_label: string;
    description: string;
    plot_application: string;
    effect: string;
    levels: FactorLevel[];
}

export class FactorLevel {
    id: number;
    factor_variant_term: string;
    factor_variant_label: string;
    amount: string;
    amount_unit_term: string;
    amount_unit_label: string;
    start_year: number;
    end_year: number;
    crop_id: number;
    all_crops: boolean;
    note: string;
    application_frequency: string;
    application_method_term: string;
    application_method_label: string;
    chemical_form_term: string;
    chemical_form_label: string;
}

export class FactorCombination {
    name: string;
    description: string;
    start_year: number;
    end_year: number;
    members: FactorCombinationMember[];
}

export class FactorCombinationMember {
    factor_id: number;
    crop_id: number;
    all_crops: boolean;
    factor_level_id: number;
    comment: string;
    amount: string;
    amount_unit_term: string;
    amount_unit_label: string;
}

export class ExperimentMeasurement {
    material: string; // {'AllCrops', 'SpecifiedCrop', 'Soil'}
    crop_id: number;
    comment: string;
    collection_frequency: string;
    variable_term: string;
    variable_label: string;
    unit_term: string;
    unit_label: string;
    scale: string;
}

export class Contact {
    contact_type: string;
    value: string;
}

export class Person {
    title: string;
    name: string;
    affiliation: string;
    department: string;
    orcid: string;
    contacts: Contact[];
    is_contact: boolean;
    role_term: string;
    role_label: string;
}

export class Organization {
    name: string;
    name_uri: string;
    abbreviation: string;
    contacts: Contact[];
    role_term: string;
    role_label: string;
}

export class User {
    username: string;
    first_name: string;
    last_name: string;
    is_publisher: boolean;
    is_account_manager: boolean;
}

export class Vocabulary {
    id: number;
    name: string;
    version: string;
}

export class VocabularyTerm {
    identifier: string;
    name: string;
    description: string;
    dead: boolean;
}

export class Crop {
    static ALL_CROPS = Symbol('ALL_CROPS');

    id: number;
    crop_label: string;
    crop_term: string;
    start_year: number;
    end_year: number;
}

export class Rotation {
    name: string;
    phasing: string;
    start_year: number;
    end_year: number;
    phases: RotationPhase[];
    is_treatment: boolean;
}

export class RotationPhase {
    crop_id: number;
    notes: string;
    same_phase: boolean;
}

export class SiteSoilProperty {
    variable_term: string;
    variable_label: string;
    min_depth: number;
    max_depth: number;
    depth_unit: string;      // {'Metres', 'Feet'}
    is_estimated: boolean;
    is_baseline: boolean;
    reference_year: number;
    typical_value: number;
    max_value: number;
    min_value: number;
    unit_term: string;
    unit_label: string;
}

export class SiteClimateProperty {
    variable_term: string;
    variable_label: string;
    year_from: number;
    year_to: number;
    mean_value: number;
    max_value: number;
    min_value: number;
    unit_term: string;
    unit_label: string;
}

export class ExperimentLiterature {
    doi: string;
    title: string;
    language: string;
}

export class Material {
    constructor(public material: string, public crop: number) {}
}

export class SelectableItem {
    constructor(public id: (number | Symbol | Material), public name: string) {}
}

export class ExperimentPublicationRequest {
    request_time: string;
    request_status: string;
    experiment_desc: ExperimentDesc;
}
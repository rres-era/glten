import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { ExperimentDescBase, ExperimentDesc } from './types';

@Injectable({
    providedIn: 'root'
})
export class PublishedExperimentsService {
    private apiBase = '/api/v0/public';

    constructor(private http: HttpClient) { }

    getExperiments(query: string = null): Observable<PublishedExperimentsResponse> {
        let url = `${this.apiBase}/experiment`
        if (query !== null && query.length > 0) {
            url = `${url}?q=${query}`;
        }
        return this.http.get<PublishedExperimentsResponse>(url)
            .pipe(
                tap(
                    null,
                    (err) => {
                        console.log(err);                        
                    }
                )
            );
    }

    getExperiment(id: number): Observable<ExperimentDesc> {
        const url = `${this.apiBase}/experiment/${id}`;

        return this.http.get<ExperimentDesc>(url)
            .pipe(
                tap(
                    null,
                    (err) => {
                        console.log(err)
                    }
                )
            );
    }
}

export interface PublishedExperimentsResponse {
    total_experiments: number;
    total_matches: number;
    offset: number;
    limit: number;
    experiments: ExperimentDescBase[];
}

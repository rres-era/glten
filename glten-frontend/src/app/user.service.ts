import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, map, catchError, shareReplay } from 'rxjs/operators';

import { User } from './types';

class CurrentUser {
    user: User
};

@Injectable({
  providedIn: 'root'
})
export class UserService {
    private apiBase = '/api/v0';
    private _currentUser: Observable<User>;

    constructor(private http: HttpClient) { }

    getCurrentUser(): Observable<User> {
        if (!this._currentUser) {
            const url = `${this.apiBase}/current-user`;
            this._currentUser = this.http.get<CurrentUser>(url)
                .pipe(
                    tap(
                        null,
                        (err) => console.log(err)
                    ),
                    map((currentUser) => currentUser.user),
                    catchError((err, _) => {
                        console.log(err);
                        return of(<User>null);
                    }),
                    shareReplay(1)
                );
        }

        return this._currentUser;
    }
}


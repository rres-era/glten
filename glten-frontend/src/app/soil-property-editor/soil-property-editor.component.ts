import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'glt-soil-property-editor',
    templateUrl: './soil-property-editor.component.html',
    styleUrls: ['./soil-property-editor.component.css']
})
export class SoilPropertyEditorComponent {
    depthUnitValues = ['Centimetres','Inches','Metres', 'Feet'];

    @Input() model : FormGroup;
    @Output() onRemove: EventEmitter<any> = new EventEmitter();

    constructor() { }

    get depthUnitControl() : FormControl {
        return (this.model.get('depth') as FormGroup).get('unit') as FormControl;
    }

    get depthUnitLabel() {
        return this.depthUnitControl.value || 'Not specified';
    }

    setDepthUnit(unit: string) {
        this.depthUnitControl.setValue(unit);
        this.depthUnitControl.markAsDirty();
    }
}

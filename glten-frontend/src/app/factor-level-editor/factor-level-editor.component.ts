import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';

import { ExperimentFormModelService } from '../experiment-form-model.service';
import { Crop, SelectableItem } from '../types';

@Component({
    selector: 'glt-factor-level-editor',
    templateUrl: './factor-level-editor.component.html',
    styleUrls: ['./factor-level-editor.component.css']
})
export class FactorLevelEditorComponent implements OnInit {
    @Input() levelModel: FormGroup;
    @Input() cropModels: FormArray;
    @Output() onRemove: EventEmitter<any> = new EventEmitter();

    constructor(private modelService: ExperimentFormModelService) { }

    ngOnInit() {

        this.cropModels.valueChanges.subscribe(
            (newCrops) => {
                const cropControl = this.levelModel.get('cropId');

                const selectableCrops = this.selectableCrops;
                if (!selectableCrops.some((sc) => sc.id === cropControl.value)) {
                    cropControl.setValue(null);
                    cropControl.markAsDirty();
                }
            }
        );
    }

    get selectableCrops(): SelectableItem[] {
        return [
            new SelectableItem(null, 'Not specified'),
            new SelectableItem(Crop.ALL_CROPS, 'All crops'),
            ...this.modelService.selectableCrops(this.cropModels)
        ];
    }

    get cropLabel() {
        const cropId = this.levelModel.get('cropId').value;
        // No special-case for null because it's a selectable option.
        const crop = this.selectableCrops.find((sc) => sc.id === cropId);
        if (crop) {
            return crop.name;
        } else {
            return '???';
        }
    }

    setCrop(cropId: number) {
        const cropModel = this.levelModel.get('cropId');
        cropModel.setValue(cropId);
        cropModel.markAsDirty();
    }
}

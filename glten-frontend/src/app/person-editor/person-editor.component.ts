import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';

import { ExperimentFormModelService } from '../experiment-form-model.service';
import { Contact } from '../types';

@Component({
  selector: 'glt-person-editor',
  templateUrl: './person-editor.component.html',
  styleUrls: ['./person-editor.component.css']
})
export class PersonEditorComponent {

    @Input() personModel: FormGroup;

    @Output() onRemove: EventEmitter<any> = new EventEmitter();

    constructor(private modelService: ExperimentFormModelService) { }

    get contacts() {
        return this.personModel.get('contacts') as FormArray;
    }

    addContact() {
        this.contacts.push(
            this.modelService.contactToFormModel(new Contact())
        );
        this.contacts.markAsDirty();
    }

    removeContact(idx) {
        this.contacts.removeAt(idx);
        this.contacts.markAsDirty();
    }
}

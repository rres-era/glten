import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class GeocodeService {
    // There's an argument for proxying this on the server, but for now we can use
    // it directly because it has CORS headers.
    apiURL = 'https://nominatim.openstreetmap.org'

    constructor(private http: HttpClient) { }

    lookupPlaceName(name: string): Observable<Place[]> {
        const url = `${this.apiURL}/search?q=${encodeURIComponent(name)}&format=geocodejson`;
        return this.http.get<any>(url)
            .pipe(
                map((response) => {
                    const places = [];
                    for (const feature of response.features || []) {
                        if (feature.geometry && feature.geometry.type === 'Point') {
                            places.push({
                                type: feature.type,
                                name: feature.name,
                                label: feature.label,
                                latitude: feature.geometry.coordinates[1],
                                longitude: feature.geometry.coordinates[0]
                            });
                        }
                    }
                    return places;
                })
            );
    }
}

export interface Place {
    type: string;
    name: string;
    label: string;
    latitude: number;
    longitude: number;
}
//import { Component, AfterViewInit} from '@angular/core';
import { Component, AfterViewInit, OnInit } from '@angular/core';

import { FormControl } from '@angular/forms';
import { tap } from 'rxjs/operators';

import { ExperimentDescBase } from '../types';
import { PublishedExperimentsService, PublishedExperimentsResponse } from '../published-experiments.service';
import { CurationService } from '../curation.service';
import { MapConfigService } from '../map-config.service';
import { UserService } from '../user.service';

@Component({
  selector: 'glt-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements AfterViewInit {

    experiments: ExperimentDescBase[];
    err;
    wasQuery: boolean;
    total_experiments;
    total_matches;

    baseLayers: any;

    exptSearchControl = new FormControl('');
    exptLayers = [];

    userCanUnpublish = false;

    //constructor() { }

    constructor(
        private experimentsService: PublishedExperimentsService,
        private curationService: CurationService,
        private mapConfigService: MapConfigService,
        private userService: UserService
    ) {
        this.baseLayers = this.mapConfigService.makeBaseLayersMap();
    }

    ngAfterViewInit() {
        // Because we can't dynamically insert Twitter's script tag, 
        // manually force a reload once we've created the placeholder
        // element.

        // See https://stackoverflow.com/questions/50586799/how-to-embed-twitter-timeline-in-angular-6-application
        // for more info

        const w = window as any;
        if (w.twttr) {
            // Twitter JS code hasn't loaded yet.  This is mostly going to be an issue
            // on the server-side, but might also happen on slow connections client
            // side.  In the client side case, the Twitter JS load will presumably
            // eventually succeed, at which point it will scan the DOM, find the 
            // a.twitter-timeline element, and activate without further intervention.
            w.twttr.widgets.load();
        }
    }

    ngOnInit() {
        this.loadExperiments();
        this.userService.getCurrentUser().subscribe((user) => {
            if (user && user.is_publisher) {
                this.userCanUnpublish = true;
            }
        });
    }

    loadExperiments(query: string = null) {
        this.experimentsService.getExperiments(query)
            .subscribe(
                (resp) => {
                    this.err = null;
                    this.experiments = resp.experiments;
                    this.exptLayers = [
                        this.mapConfigService.experimentLayer(this.experiments || [])
                    ]
                    this.total_experiments = resp.total_experiments;
                    this.total_matches = resp.total_matches;
                    this.wasQuery = (query && query.length > 0);
                },
                (err) => {
                    this.err = err;
                    this.experiments = [];
                    this.exptLayers = [];
                }
            );
    }

    get mapOptions(): any {
        return this.mapConfigService.mapConfig();
    }

    exptSearchClicked(ev) {
        this.loadExperiments(this.exptSearchControl.value);
    }

    onMapReady(map) {
        this.mapConfigService.addAttributionControl(map);
    }

    makeUnpublishAction(experimentID) {
        return () => {
            if (window.confirm('Are you sure you want to unpublish this experiment?')) {
                return this.curationService.postUnpublish(experimentID)
                    .pipe(
                        tap(
                            () => this.loadExperiments(this.exptSearchControl.value),
                            (err) => window.alert(err.message)
                        )
                    );
            }
        }
    }
}

import { Component, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { latLng, LatLng, marker, icon } from 'leaflet';
import memoize from 'memoize-one';

import { MapConfigService } from '../map-config.service';
import { GeocodeService } from '../geocode.service';

@Component({
    selector: 'glt-site-location-editor',
    templateUrl: './site-location-editor.component.html',
    styleUrls: ['./site-location-editor.component.css']
})
export class SiteLocationEditorComponent {

    @Input() centroidModel: FormGroup;

    markerIcon = icon({
        iconSize: [ 21, 31 ],
        iconAnchor: [ 16, 31 ],
        iconUrl: 'static/marker-icon.png',
        shadowUrl: 'static/marker-shadow.png'
    });

    baseLayers: any;

    placeSearchControl = new FormControl('');
    mapCenter: LatLng;
    mapZoom: number;

    constructor(
        private mapConfigService: MapConfigService,
        private geocoder: GeocodeService
    ) { 
        this.baseLayers = this.mapConfigService.makeBaseLayersMap();

        const config = mapConfigService.mapConfig();
        this.mapCenter = config.center;
        this.mapZoom = config.zoom;
    }

    get mapOptions(): any {
        return (
            this.mapConfigService.mapConfig()
        );
    }

    makeMarkerLayer = memoize((latValue, longValue) => (
        marker(
            latLng(latValue, longValue),
            {
                icon: this.markerIcon
            }
        )
    ))

    get mapLayers(): any {
        const latValue = floatOrNull(this.centroidModel.get('latitude').value),
              longValue = floatOrNull(this.centroidModel.get('longitude').value);

        if (latValue === null || longValue === null) {
            return [];
        } else {
            return (
                [
                    this.makeMarkerLayer(latValue, longValue)
                ]
            );
        }
    }

    mapClicked(event) {
        const clickPos = event.latlng;
        this.centroidModel.setValue({
            latitude: clickPos.lat.toFixed(6),   // Leaflet returns untruncated doubles, which are clearly unreasonably precise.
            longitude: clickPos.lng.toFixed(6)   // 6dp gives a worst-case precision of ~1m, which is close to the resolution of 
                                                 // public satellite imagery, so should be plenty.
        });
        this.centroidModel.markAsDirty();
    }

    placeSearchClicked(event) {
        const search = this.placeSearchControl.value;
        if (search.length > 3) {
            this.geocoder.lookupPlaceName(search)
                .subscribe((result) => {
                    if (result.length > 0) {
                        this.mapCenter = latLng(result[0].latitude, result[0].longitude);
                        this.mapZoom = 12;
                    }
                });
        }
    }

    onMapReady(map) {
        this.mapConfigService.addAttributionControl(map);
    }
}

function floatOrNull(str) {
    if (str !== null && str !== '') {
        return parseFloat(str);
    } else {
        return null;
    }
}
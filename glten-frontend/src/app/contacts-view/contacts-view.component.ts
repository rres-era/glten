import { Component, Input } from '@angular/core';

import { Contact } from '../types';

@Component({
    selector: 'glt-contacts-view',
    templateUrl: './contacts-view.component.html',
    styleUrls: ['./contacts-view.component.css']
})
export class ContactsViewComponent {
    @Input() contacts: Contact[];


    constructor() { }


}

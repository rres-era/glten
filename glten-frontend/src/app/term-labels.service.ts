import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class TermLabelsService {
    constructor() { }

    dataPolicyOptions = [
        {status: 'YesOnline', label: 'Yes (available online)'},
        {status: 'YesOffline', label: 'Yes (not online)'},
        {status: 'No', label: 'No'},
        {status: 'NotKnown', label: "Don't know"}
    ]

    dataPolicyLabel(policy) {
        if (!policy) {
            return 'Choose one...';
        } else {
            return (this.dataPolicyOptions.find((opt) => opt.status === policy) || {label: 'Unrecognized'}).label;
        }
    }

    dataStatementOptions = [
        {status: "Freely available online", label: "Freely available online"},
        {status: "Available online with registration", label: "Available online with registration"},
        {status: "Available to any researcher on request", label: "Available to any researcher on request"},
        {status: "Available to partner organisations", label: "Available to partner organisations"},
        {status: "Available to collaborators only", label: "Available to collaborators only"},
        {status: "Not available outside of organisation", label: "Not available outside of organisation"},
        {status: "Other", label: "Other"},
        {status: "Don't know", label: "Don't Know"}
    ]

    dataStatementLabel(statement) {
        if (!statement) {
            return 'Choose one...';
        } else {
            return (this.dataStatementOptions.find((opt) => opt.status === statement) || {label: 'Unrecognized'}).label;
        }
    }

    dataLicenseOptions = [
        {status: 'CC0', label: 'CC0'},
        {status: "CC BY", label: "CC BY"},
        {status: "CC BY-SA", label: "CC BY-SA"},
        {status: "CC BY-ND", label: "CC BY-ND"},
        {status: "CC BY-NC", label: "CC BY-NC"},
        {status: "CC BY-NC-SA", label: "CC BY-NC-SA"},
        {status: "CC BY-NC-ND", label: "CC BY-NC-ND"},
        {status: "Other", label: "Other"},
        {status: "Don't know", label: "Don't know"}
    ]

    dataLicenseLabel(license) {
        if (!license) {
            return 'Choose one...';
        } else {
            return (this.dataLicenseOptions.find((opt) => opt.status === license) || {label: 'Unrecognized'}).label;
        }
    }
}

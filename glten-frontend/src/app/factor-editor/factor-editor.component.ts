import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';

import { ExperimentFormModelService } from '../experiment-form-model.service';
import { FactorLevel } from '../types';

@Component({
  selector: 'glt-factor-editor',
  templateUrl: './factor-editor.component.html',
  styleUrls: ['./factor-editor.component.css']
})
export class FactorEditorComponent {

    @Input() factorModel: FormGroup;
    @Input() cropModels: FormGroup;
    @Output() onRemove: EventEmitter<any> = new EventEmitter();

    plotApplicationValues = [
        'Whole plot',
        'Sub plot',
        'Other'
    ];

    effectValues = [
        null,     // == not specified
        'Direct',
        'Residual',
        'Cumulative',
    ]

    constructor(private modelService: ExperimentFormModelService) { }


    get plotApplication() {
        return this.factorModel.get('plotApplication');
    }

    get plotApplicationLabel() {
        return this.plotApplication.value || 'Select one...'
    }

    setPlotApplication(value) {
        this.plotApplication.setValue(value);
        this.plotApplication.markAsDirty();
    }

    get effect() {
        return this.factorModel.get('effect');
    }

    get effectLabel() {
        return this.effect.value || 'Not specified';
    }

    setEffect(value) {
        this.effect.setValue(value);
        this.effect.markAsDirty();
    }

    get levels() {
        return this.factorModel.get('levels') as FormArray;
    }

    addLevel() {
        this.levels.push(
            this.modelService.factorLevelToFormModel(
                new FactorLevel()
            )
        );
        this.levels.markAsDirty();
    }

    removeLevel(idx: number) {
        this.levels.removeAt(idx);
        this.levels.markAsDirty();
    }
}

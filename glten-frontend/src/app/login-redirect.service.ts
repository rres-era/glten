import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LoginRedirectService {
    constructor() { }

    redirectViaLogin() {
        window.location.href = `/accounts/login/?next=${window.location.pathname}`;
    }
}

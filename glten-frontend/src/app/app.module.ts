import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CookieModule } from 'ngx-cookie';
import { MomentModule } from 'ngx-moment';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExperimentListComponent } from './experiment-list/experiment-list.component';
import { ExperimentEditorComponent } from './experiment-editor/experiment-editor.component';
import { IsInvalidDirective } from './is-invalid.directive';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { PersonEditorComponent } from './person-editor/person-editor.component';
import { ContactEditorComponent } from './contact-editor/contact-editor.component';
import { OrganizationEditorComponent } from './organization-editor/organization-editor.component';
import { DesignPhaseEditorComponent } from './design-phase-editor/design-phase-editor.component';
import { VocabTermInputComponent } from './vocab-term-input/vocab-term-input.component';
import { FactorEditorComponent } from './factor-editor/factor-editor.component';
import { CropEditorComponent } from './crop-editor/crop-editor.component';
import { RotationEditorComponent } from './rotation-editor/rotation-editor.component';
import { FactorCombinationEditorComponent } from './factor-combination-editor/factor-combination-editor.component';
import { MeasurementEditorComponent } from './measurement-editor/measurement-editor.component';
import { FactorLevelEditorComponent } from './factor-level-editor/factor-level-editor.component';
import { PublicationRequestListComponent } from './publication-request-list/publication-request-list.component';
import { PublicationRequestReviewComponent } from './publication-request-review/publication-request-review.component';
import { PublicExperimentListComponent } from './public-experiment-list/public-experiment-list.component';
import { ExperimentViewComponent } from './experiment-view/experiment-view.component';
import { ExperimentDescViewComponent } from './experiment-desc-view/experiment-desc-view.component';
import { ContactsViewComponent } from './contacts-view/contacts-view.component';
import { DesignPhaseViewComponent } from './design-phase-view/design-phase-view.component';
import { SiteEditorComponent } from './site-editor/site-editor.component';
import { SiteViewComponent } from './site-view/site-view.component';
import { ValidationCheckmarkComponent } from './validation-checkmark/validation-checkmark.component';
import { ExperimentOverviewEditorComponent } from './experiment-overview-editor/experiment-overview-editor.component';
import { SiteLocationEditorComponent } from './site-location-editor/site-location-editor.component';
import { SoilPropertyEditorComponent } from './soil-property-editor/soil-property-editor.component';
import { ActionButtonComponent } from './action-button/action-button.component';
import { ClimatePropertyEditorComponent } from './climate-property-editor/climate-property-editor.component';
import { FormHelpComponent } from './form-help/form-help.component';
import { LoadingComponent } from './loading/loading.component';
import { LiteratureEditorComponent } from './literature-editor/literature-editor.component';
import { ThumbnailMapComponent } from './thumbnail-map/thumbnail-map.component';
import { AboutComponent } from './about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    ExperimentListComponent,
    ExperimentEditorComponent,
    IsInvalidDirective,
    HeaderComponent,
    HomeComponent,
    PersonEditorComponent,
    ContactEditorComponent,
    OrganizationEditorComponent,
    DesignPhaseEditorComponent,
    VocabTermInputComponent,
    FactorEditorComponent,
    CropEditorComponent,
    RotationEditorComponent,
    FactorCombinationEditorComponent,
    MeasurementEditorComponent,
    FactorLevelEditorComponent,
    PublicationRequestListComponent,
    PublicationRequestReviewComponent,
    PublicExperimentListComponent,
    ExperimentViewComponent,
    ExperimentDescViewComponent,
    ContactsViewComponent,
    DesignPhaseViewComponent,
    SiteEditorComponent,
    SiteViewComponent,
    ValidationCheckmarkComponent,
    ExperimentOverviewEditorComponent,
    SiteLocationEditorComponent,
    SoilPropertyEditorComponent,
    ActionButtonComponent,
    ClimatePropertyEditorComponent,
    FormHelpComponent,
    LoadingComponent,
    LiteratureEditorComponent,
    ThumbnailMapComponent,
	AboutComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CookieModule.forRoot(),
    MomentModule,
    NgbModule,
    AngularFontAwesomeModule,
    LeafletModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: []
})
export class AppModule {}

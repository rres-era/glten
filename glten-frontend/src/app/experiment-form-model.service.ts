import { Injectable } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, FormArray, FormBuilder, Validators,
         ValidationErrors } from '@angular/forms';

import { ExperimentDesc, ExperimentPeriod, Factor, FactorLevel, Person, Organization, Contact,
         Crop, Rotation, RotationPhase, FactorCombination, FactorCombinationMember,
         ExperimentMeasurement, SelectableItem, Material, SiteSoilProperty,
         SiteClimateProperty, ExperimentLiterature} from './types';


/*
 * Mapping between the backend (and over-the-wire) data module, and Angular "reactive forms"
 * logic.  Note that this also includes the validation logic.
 *
 * As well as being used on the client when editing, we create one of these from server.ts
 * when performing server-side data model validation, which permits validation logic to be
 * shared between the client and the server (and thus, hopefully, stay perfectly in sync).
 * Currently, server.ts only supports synchronous valdation of the data model.  Try to avoid
 * adding asynchrounous valdators and -- should they become essential in the future -- consider
 * changing the server.ts validation code to wait until the validation status converges.
 */


@Injectable({
  providedIn: 'root'
})
export class ExperimentFormModelService {
    // Keep these -ve, so that they can't clash with "real" IDs from the server.
    private idSeed = -1000;

    constructor(private fb: FormBuilder) { }

    termGroup(term, label) {
        return this.fb.control({
            identifier: term || null,
            name: label || ''
        });
    }

    descToFormModel(desc: ExperimentDesc): FormGroup {
        return (
            this.fb.group({
                overview: this.descToOverviewModel(desc),

                contacts: this.fb.group({
                    people: this.fb.array(
                        desc.people.map(this.personToFormModel, this)
                    ),

                    organizations: this.fb.array(
                        (desc.organizations || []).map(this.orgToFormModel, this)
                    )
                }),

                periods: this.fb.array(
                    (desc.periods && desc.periods.length > 0 ? desc.periods : [new ExperimentPeriod()])
                        .map(this.periodToFormModel, this),
                    {validators: [periodsNameValidator]}
                ),

                site: this.descToSiteModel(desc),

                literature: this.fb.array(
                    (desc.literature && desc.literature.length > 0 ? desc.literature : [])
                        .map(this.literatureToFormModel, this)
                )
            })
        );
    }

    descToOverviewModel(desc: ExperimentDesc): FormGroup {
        return (
            this.fb.group({
                name: [desc.name || '', [Validators.required]],
                localIdentifier: [desc.local_identifier || ''],
                dates: this.fb.group({
                    startYear: [desc.start_year ? desc.start_year.toString() : '', [Validators.required]],
                    endYear: [desc.end_year ? desc.end_year.toString() : ''],
                    ongoing: [desc.ongoing || false],
                    establishmentEnd: [desc.establishment_period_end ? desc.establishment_period_end.toString() : '']
                }, {validators: [datesPlusOngoingValidator]}),
                //}, {validators: [datesValidator]}),     
                type: this.termGroup(desc.experiment_type_term, desc.experiment_type_label),

                objective: [desc.objective || '',[Validators.required]],
                description: [desc.description || ''],
                
                //dataStatement: this.termGroup(desc.data_statement_term, desc.data_statement_label),
                dataStatementStatus: [desc.data_statement_term || '',[Validators.required]],
                //dataLicense: this.termGroup(desc.data_license_term, desc.data_license_label),
                dataLicenseStatus: [desc.data_license_term || '',[Validators.required]],
                dataURL: [desc.data_url || ''],
                dataPolicyStatus: [desc.data_policy_status || '', [Validators.required]],
                dataPolicyURL: [{value: desc.data_policy_url || '', disabled: desc.data_policy_status !== 'YesOnline'}],
                dataAccessStatement: [desc.data_access_statement || '',],
                dataContact: [desc.data_contact || ''],

                farm_operations_data: [desc.farm_operations_data || false],
                sample_archive: [desc.sample_archive || false],
                samples_available: [desc.samples_available || false],
            }, {validators: [dataPolicyValidator]})
        );
    }

    descToSiteModel(desc: ExperimentDesc): FormGroup {
        return (
            this.fb.group({
                name: [desc.site_name || '', [Validators.required]],
                localCode: [desc.site_local_code || ''],
                geonamesID: [desc.site_geonames_id || ''],
                doi: [desc.site_doi || ''],
                type: this.termGroup(desc.site_type_term, desc.site_type_label),
                region: [desc.site_region || ''],
                locality: [desc.site_locality || ''],
                country: [desc.site_country || ''],
                centroid: this.fb.group({
                    latitude: [desc.site_centroid_latitude, [Validators.required]],
                    longitude: [desc.site_centroid_longitude, [Validators.required]],
                }),
                elevation: this.fb.group({
                    value: [desc.site_elevation !== null ? desc.site_elevation.toString() : ''],
                    unit: [desc.site_elevation_unit]
                }, {validators: [valueUnitValidator]}),
                slope: [desc.site_slope],
                slopeAspect: [desc.site_slope_aspect || ''],
                soilType: this.termGroup(desc.site_soil_type_term, desc.site_soil_type_label),
                soilDescription: [desc.site_soil_description || ''],
                visitsAllowed: [desc.site_visits_allowed],
                visitingArrangements: [desc.site_visiting_arrangements || ''],
                history: [desc.site_history || ''],
                management: [desc.site_management || ''],
                soilProps: this.fb.array(
                    (desc.site_soil_properties || []).map(this.soilPropToFormModel, this)
                ),
                climaticType: this.termGroup(desc.site_climatic_type_term, desc.site_climatic_type_label),
                climateDescription: [desc.site_climate_description || ''],
                climateProps: this.fb.array(
                    (desc.site_climate_properties || []).map(this.climatePropToFormModel, this)
                )
            })
        );
    }

    soilPropToFormModel(prop: SiteSoilProperty): FormGroup {
        return (
            this.fb.group({
                variable: this.termGroup(prop.variable_term, prop.variable_label),
                depth: this.fb.group({
                    maxDepth: [typeof(prop.max_depth) === 'number' ? prop.max_depth.toString() : ''],
                    minDepth: [typeof(prop.min_depth) === 'number' ? prop.min_depth.toString() : ''],
                    unit: [prop.depth_unit]
                }, {validators: [depthValueUnitValidator]}),
                isEstimated: [prop.is_estimated || false],
                isBaseline: [prop.is_baseline || false],
                referenceYear: [typeof(prop.reference_year) === 'number' ? prop.reference_year.toString() : ''],
                typicalValue: [typeof(prop.typical_value) === 'number' ? prop.typical_value.toString() : ''],
                maxValue: [typeof(prop.max_value) === 'number' ? prop.max_value.toString() : ''],
                minValue: [typeof(prop.min_value) === 'number' ? prop.min_value.toString() : ''],
                unit: this.termGroup(prop.unit_term, prop.unit_label)
            })
        );
    }

    climatePropToFormModel(prop: SiteClimateProperty): FormGroup {
        return (
            this.fb.group({
                variable: this.termGroup(prop.variable_term, prop.variable_label),
                dates: this.fb.group({
                    startYear: [prop.year_from ? prop.year_from.toString() : ''],
                    endYear: [prop.year_to ? prop.year_to.toString() : '']
                }, {validators: [datesValidator]}),
                meanValue: [typeof(prop.mean_value) === 'number' ? prop.mean_value.toString() : ''],
                minValue: [typeof(prop.min_value) === 'number' ? prop.min_value.toString() : ''],
                maxValue: [typeof(prop.max_value) === 'number' ? prop.max_value.toString() : ''],
                unit: this.termGroup(prop.unit_term, prop.unit_label)
            })
        );
    }

    periodToFormModel(period: ExperimentPeriod): FormGroup {
        return (
            this.fb.group({
                name: [period.name || ''],  // Required IFF more than one period, so validated higher up.
                
                description: [period.description || ''],
                designDescription: [period.design_description || ''],
                factorial: [period.factorial || false],
                type: this.termGroup(period.design_type_term, period.design_type_label),

                dates: this.fb.group({
                    startYear: [period.start_year ? period.start_year.toString() : ''],
                    endYear: [period.end_year ? period.end_year.toString() : ''],
                    ongoing: [period.ongoing || false]
                }, {validators: [datesPlusOngoingValidator]}),

                numberOfBlocks: [
                    typeof(period.number_of_blocks) === 'number' 
                        ? period.number_of_blocks.toString() 
                        : ''],
                numberOfPlots: [
                    typeof(period.number_of_plots) === 'number'
                        ? period.number_of_plots.toString()
                        : ''],
                numberOfSubplots: [
                    typeof(period.number_of_subplots) === 'number'
                        ? period.number_of_subplots.toString()
                        : ''],
                numberOfReplicates: [
                    typeof(period.number_of_replicates) === 'number'
                        ? period.number_of_replicates.toString()
                        : ''],
                harvestsPerYear: [
                    typeof(period.number_of_harvests_per_year) === 'number'
                        ? period.number_of_harvests_per_year.toString()
                        : ''],

                factors: this.fb.array(
                    (period.factors || []).map(this.factorToFormModel, this)
                ),
                crops: this.fb.array(
                    (period.crops || []).map(this.cropToFormModel, this)
                ),
                rotations: this.fb.array(
                    (period.rotations || []).map(this.rotationToFormModel, this)
                ),
                factorCombinations: this.fb.array(
                    (period.factor_combinations || []).map(this.combinationToFormModel, this)
                ),
                measurements: this.fb.array(
                    (period.measurements || []).map(this.measurementToFormModel, this)
                )
            })
        );
    }

    factorToFormModel(factor: Factor): FormGroup {
        return (
            this.fb.group({
                id: [factor.id || --this.idSeed],
                factor: this.termGroup(factor.factor_term, factor.factor_label),
                description: [factor.description || ''],
                plotApplication: [factor.plot_application || ''],
                effect: [factor.effect || ''],
                levels: this.fb.array(
                    (factor.levels || []).map(this.factorLevelToFormModel, this)
                )
            })
        );
    }

    factorLevelToFormModel(level: FactorLevel): FormGroup {
        return (
            this.fb.group({
                id: [level.id || --this.idSeed],
                factorVariant: this.termGroup(
                    level.factor_variant_term,
                    level.factor_variant_label
                ),
                amount: [level.amount || ''],
                amountUnit: this.termGroup(
                    level.amount_unit_term,
                    level.amount_unit_label
                ),
                dates: this.fb.group({
                    startYear: [level.start_year ? level.start_year.toString() : ''],
                    endYear: [level.end_year ? level.end_year.toString() : '']
                }, {validators: [datesValidator]}),
                cropId: [level.all_crops ? Crop.ALL_CROPS : level.crop_id],
                note: [level.note || ''],
                applicationFrequency: [level.application_frequency || ''],
                applicationMethod: this.termGroup(
                    level.application_method_term,
                    level.application_method_label
                ),
                chemicalForm: this.termGroup(
                    level.chemical_form_term,
                    level.chemical_form_label
                )
            })
        );
    }

    cropToFormModel(crop: Crop): FormGroup {
        return (
            this.fb.group({
                id: [crop.id || --this.idSeed],
                crop: this.termGroup(crop.crop_term, crop.crop_label),
                dates: this.fb.group({
                    startYear: [crop.start_year ? crop.start_year.toString() : ''],
                    endYear: [crop.end_year ? crop.end_year.toString() : '']
                }, {validators: [datesValidator]})
            })
        );
    }

    rotationToFormModel(rotation: Rotation): FormGroup {
        return (
            this.fb.group({
                name: [rotation.name || ''],
                phasing: [rotation.phasing],
                dates: this.fb.group({
                    startYear: [rotation.start_year ? rotation.start_year.toString() : ''],
                    endYear: [rotation.end_year ? rotation.end_year.toString() : '']
                }, {validators: [datesValidator]}),
                phases: this.fb.array(
                    (rotation.phases && rotation.phases.length > 0 ? rotation.phases : [new RotationPhase()])
                        .map(this.rotationPhaseToFormModel, this)
                ),
                isTreatment: [rotation.is_treatment || false]
            })
        );
    }

    rotationPhaseToFormModel(phase: RotationPhase): FormGroup {
        return (
            this.fb.group({
                cropId: [phase.crop_id || null],
                notes: [phase.notes || ''],
                samePhaseAsPrevious: [phase.same_phase || false]
            })
        );
    }

    combinationToFormModel(combination: FactorCombination): FormGroup {
        return (
            this.fb.group({
                name: [combination.name || ''],
                description: [combination.description || ''],
                dates: this.fb.group({
                    startYear: [combination.start_year ? combination.start_year.toString() : ''],
                    endYear: [combination.end_year ? combination.end_year.toString() : '']
                }, {validators: [datesValidator]}),
                members: this.fb.array(
                    (combination.members && combination.members.length > 0 ? combination.members : [new FactorCombinationMember()])
                        .map(this.combinationMemberToFormModel, this)
                )
            })
        );
    }

    combinationMemberToFormModel(member: FactorCombinationMember): FormGroup {
        return (
            this.fb.group({
                factorId: [member.factor_id || null],
                cropId: [member.all_crops ? Crop.ALL_CROPS : member.crop_id || null],
                factorLevelId: [member.factor_level_id || null],
                comment: [member.comment || ''],
                amount: [member.amount || ''],
                amountUnit: this.termGroup(member.amount_unit_term, member.amount_unit_label)
            })
        );
    }

    measurementToFormModel(measurement: ExperimentMeasurement): FormGroup {
        return (
            this.fb.group({
                material: [
                    new Material(
                        measurement.material,
                        measurement.material === 'SpecifiedCrop' ? measurement.crop_id : null
                    )
                ],
                comment: [measurement.comment || ''],
                collectionFrequency: [measurement.collection_frequency || ''],
                variable: this.termGroup(measurement.variable_term, measurement.variable_label),
                unit: this.termGroup(measurement.unit_term, measurement.unit_label),
                scale: [measurement.scale || '']
            })
        );
    }

    personToFormModel(person: Person): FormGroup {
        return (
            this.fb.group({
                title: [person.title || ''],
                name: [person.name || '', [Validators.required]],
                affiliation: [person.affiliation || '',[Validators.required]],
                department: [person.department || ''],
                orcid: [person.orcid || ''],

                contacts: this.contactsToFormModel(person.contacts),
                isContact: [person.is_contact || false],

                role: this.termGroup(person.role_term, person.role_label)
            })
        );
    }

    orgToFormModel(org: Organization): FormGroup {
        return (
            this.fb.group({
                name: [org.name || '', [Validators.required]],
                nameURI: [org.name_uri || ''],
                abbreviation: [org.abbreviation || ''],

                contacts: this.contactsToFormModel(org.contacts),

                role: this.termGroup(org.role_term, org.role_label)
            })
        );
    }

    contactsToFormModel(contacts: Contact[]): FormArray {
        if (!contacts || contacts.length === 0) {
            contacts = [new Contact()];
        }
        return this.fb.array(
            contacts.map(this.contactToFormModel, this)
        );
    }

    contactToFormModel(contact: Contact): FormGroup {
        const valueControl = this.fb.control(contact.value || '')
        const typeControl = this.fb.control(contact.contact_type)

        if (!contact.contact_type) {
            valueControl.disable();
            typeControl.valueChanges.subscribe((obs) => {
                valueControl.enable();
            });
        }

        return (
            this.fb.group({
                type: typeControl,
                value: valueControl
            })
        );

        // TODO: Should have some value-specific validation in here.
    }

    literatureToFormModel(pub: ExperimentLiterature): FormGroup {
        return (
            this.fb.group({
                doi: [pub.doi || '', [Validators.required]],
                title: [pub.title || '', [Validators.required]],
                language: [pub.language || '']
            })
        );
    }

    modelToExperimentDesc(model: FormGroup): ExperimentDesc {
        const desc = new ExperimentDesc();

        {
            const overviewModel = model.get('overview');

            desc.name = stringOrNull(overviewModel.get('name').value);
            desc.local_identifier = stringOrNull(overviewModel.get('localIdentifier').value);

            [desc.experiment_type_term, desc.experiment_type_label] = termPair(overviewModel.get('type').value);

            const dates = overviewModel.get('dates') as FormGroup;
            desc.start_year = intOrNull(dates.get('startYear').value);
            desc.end_year = intOrNull(dates.get('endYear').value);
            desc.ongoing = dates.get('ongoing').value;
            desc.establishment_period_end = intOrNull(dates.get('establishmentEnd').value);

            desc.description = stringOrNull(overviewModel.get('description').value);
            desc.objective = stringOrNull(overviewModel.get('objective').value);

            //[desc.data_statement_term, desc.data_statement_label] = termPair(overviewModel.get('dataStatement').value);
            desc.data_statement_term = stringOrNull(overviewModel.get('dataStatementStatus').value);
            desc.data_statement_label = stringOrNull(overviewModel.get('dataStatementStatus').value);
            //[desc.data_license_term, desc.data_license_label] = termPair(overviewModel.get('dataLicense').value);
            desc.data_license_term = stringOrNull(overviewModel.get('dataLicenseStatus').value);
            desc.data_license_label = stringOrNull(overviewModel.get('dataLicenseStatus').value);
            desc.data_url = stringOrNull(overviewModel.get('dataURL').value);
            desc.data_policy_status = stringOrNull(overviewModel.get('dataPolicyStatus').value);
            desc.data_policy_url = stringOrNull(overviewModel.get('dataPolicyURL').value);
            desc.data_access_statement = stringOrNull(overviewModel.get('dataAccessStatement').value);
            desc.data_contact = stringOrNull(overviewModel.get('dataContact').value);

            desc.farm_operations_data = overviewModel.get('farm_operations_data').value;
            desc.sample_archive = overviewModel.get('sample_archive').value;
            desc.samples_available = overviewModel.get('samples_available').value;
        }

        {
            const siteModel = model.get('site') as FormGroup;
            desc.site_name = stringOrNull(siteModel.get('name').value);
            desc.site_local_code = stringOrNull(siteModel.get('localCode').value);
            desc.site_geonames_id = stringOrNull(siteModel.get('geonamesID').value);
            desc.site_doi = stringOrNull(siteModel.get('doi').value);
            [desc.site_type_term, desc.site_type_label] = termPair(siteModel.get('type').value);
            desc.site_region = stringOrNull(siteModel.get('region').value);
            desc.site_locality = stringOrNull(siteModel.get('locality').value);
            desc.site_country = stringOrNull(siteModel.get('country').value);
            {
                const centroidModel = siteModel.get('centroid') as FormGroup;
                desc.site_centroid_latitude = floatOrNull(centroidModel.get('latitude').value);
                desc.site_centroid_longitude = floatOrNull(centroidModel.get('longitude').value);
            }
            {
                const elevationModel = siteModel.get('elevation') as FormGroup;
                desc.site_elevation = floatOrNull(elevationModel.get('value').value);
                desc.site_elevation_unit = stringOrNull(elevationModel.get('unit').value);
            }
            desc.site_slope = floatOrNull(siteModel.get('slope').value);
            desc.site_slope_aspect = stringOrNull(siteModel.get('slopeAspect').value);
            [desc.site_soil_type_term, desc.site_soil_type_label] = termPair(siteModel.get('soilType').value);
            desc.site_soil_description = stringOrNull(siteModel.get('soilDescription').value);
            desc.site_visits_allowed = !!siteModel.get('visitsAllowed').value;
            desc.site_visiting_arrangements = stringOrNull(siteModel.get('visitingArrangements').value);
            desc.site_history = stringOrNull(siteModel.get('history').value);
            desc.site_management = stringOrNull(siteModel.get('management').value);
            desc.site_soil_properties = this.modelToSoilPropDescs(siteModel.get('soilProps') as FormArray);
            [desc.site_climatic_type_term, desc.site_climatic_type_label] = termPair(siteModel.get('climaticType').value);
            desc.site_climate_description = stringOrNull(siteModel.get('climateDescription').value);
            desc.site_climate_properties = this.modelToClimatePropDescs(siteModel.get('climateProps') as FormArray);
        }

        {
            const contactsModel = model.get('contacts');
            desc.people = this.modelToPeopleDescs(contactsModel.get('people') as FormArray);
            desc.organizations = this.modelToOrganizationDescs(contactsModel.get('organizations') as FormArray);
        }
        
        desc.periods = (model.get('periods') as FormArray).controls.map(this.modelToPeriodDesc, this);

        desc.literature = (model.get('literature') as FormArray).controls.map(this.modelToLiteratureDesc, this)
            .filter((pub) => {
                return pub.doi ||
                    pub.title ||
                    pub.language;
            });

        return desc;
    }

    modelToSoilPropDescs(propModels: FormArray): SiteSoilProperty[] {
        return propModels.controls.map((propModel) => {
            const prop = new SiteSoilProperty();
            [prop.variable_term, prop.variable_label] = termPair(propModel.get('variable').value);
            {
                const depthModel = propModel.get('depth') as FormGroup;
                prop.min_depth = floatOrNull(depthModel.get('minDepth').value);
                prop.max_depth = floatOrNull(depthModel.get('maxDepth').value);
                prop.depth_unit = stringOrNull(depthModel.get('unit').value);
            }
            prop.is_estimated = propModel.get('isEstimated').value;
            prop.is_baseline = propModel.get('isBaseline').value;
            prop.reference_year = intOrNull(propModel.get('referenceYear').value);
            prop.typical_value = floatOrNull(propModel.get('typicalValue').value);
            prop.max_value = floatOrNull(propModel.get('maxValue').value);
            prop.min_value = floatOrNull(propModel.get('minValue').value);
            [prop.unit_term, prop.unit_label] = termPair(propModel.get('unit').value);

            return prop;
        }).filter((prop) => {
            return prop.variable_term ||
                prop.variable_label ||
                typeof(prop.min_depth) === 'number' ||
                typeof(prop.max_depth) === 'number' ||
                prop.depth_unit ||
                prop.is_estimated ||
                prop.is_baseline ||
                typeof(prop.reference_year) === 'number' ||
                typeof(prop.typical_value) === 'number' ||
                typeof(prop.max_value) === 'number' ||
                typeof(prop.min_value) === 'number' ||
                prop.unit_term ||
                prop.unit_label;
        });
    }

    modelToClimatePropDescs(propModels: FormArray): SiteClimateProperty[] {
        return propModels.controls.map((propModel) => {
            const prop = new SiteClimateProperty();
            [prop.variable_term, prop.variable_label] = termPair(propModel.get('variable').value);
            {
                const dates = propModel.get('dates') as FormGroup;
                prop.year_from = intOrNull(dates.get('startYear').value);
                prop.year_to = intOrNull(dates.get('endYear').value);
            }
            prop.mean_value = floatOrNull(propModel.get('meanValue').value);
            prop.min_value = floatOrNull(propModel.get('minValue').value);
            prop.max_value = floatOrNull(propModel.get('maxValue').value);
            [prop.unit_term, prop.unit_label] = termPair(propModel.get('unit').value);

            return prop;
        }).filter((prop) => {
            return prop.variable_term ||
                prop.variable_label ||
                typeof(prop.year_from) === 'number' ||
                typeof(prop.year_to) === 'number' ||
                typeof(prop.mean_value) === 'number' ||
                typeof(prop.min_value) === 'number' ||
                typeof(prop.max_value) === 'number' ||
                prop.unit_term ||
                prop.unit_label;
        });
    }

    modelToPeopleDescs(personModels: FormArray): Person[] {
        return personModels.controls.map((personModel) => {
            const person = new Person();
            person.name = stringOrNull(personModel.get('name').value);
            person.title = stringOrNull(personModel.get('title').value);
            person.affiliation = stringOrNull(personModel.get('affiliation').value);
            person.department = stringOrNull(personModel.get('department').value);
            person.orcid = stringOrNull(personModel.get('orcid').value);

            person.contacts = (personModel.get('contacts') as FormArray).controls
                .map(this.contactModelToData, this)
                .filter((contact) => contact.value || contact.contact_type);
            person.is_contact = personModel.get('isContact').value;
            [person.role_term, person.role_label] = termPair(personModel.get('role').value);

            return person;
        }).filter((person) => {
            return person.name ||
                person.title ||
                person.department ||
                person.orcid ||
                person.contacts && person.contacts.length > 0;
        });
    }

    modelToOrganizationDescs(orgModels: FormArray): Organization[] {
        return orgModels.controls.map((orgModel) => {
            const org = new Organization();
            org.name = stringOrNull(orgModel.get('name').value);
            org.abbreviation = stringOrNull(orgModel.get('abbreviation').value);
            org.name_uri = stringOrNull(orgModel.get('nameURI').value);

            org.contacts = (orgModel.get('contacts') as FormArray).controls
                .map(this.contactModelToData, this)
                .filter((contact) => contact.value || contact.contact_type);
            [org.role_term, org.role_label] = termPair(orgModel.get('role').value);

            return org;
        }).filter((org) => {
            return org.name ||
                org.abbreviation ||
                org.name_uri ||
                org.contacts && org.contacts.length > 0;
        });
    }

    modelToPeriodDesc(periodModel: FormGroup): ExperimentPeriod {
        const period = new ExperimentPeriod();
        period.name = stringOrNull(periodModel.get('name').value);
        period.design_description = stringOrNull(periodModel.get('designDescription').value);
        period.description = stringOrNull(periodModel.get('description').value);
        period.factorial = periodModel.get('factorial').value;

        [period.design_type_term, period.design_type_label] = termPair(periodModel.get('type').value);

        const dates = periodModel.get('dates') as FormGroup;
        period.start_year = intOrNull(dates.get('startYear').value);
        period.end_year = intOrNull(dates.get('endYear').value);
        period.ongoing = dates.get('ongoing').value;

        period.number_of_blocks = intOrNull(periodModel.get('numberOfBlocks').value);
        period.number_of_plots = intOrNull(periodModel.get('numberOfPlots').value);
        period.number_of_subplots = intOrNull(periodModel.get('numberOfSubplots').value);
        period.number_of_replicates = intOrNull(periodModel.get('numberOfReplicates').value);
        period.number_of_harvests_per_year = intOrNull(periodModel.get('harvestsPerYear').value);

        period.factors = this.modelToFactorDescs(periodModel.get('factors') as FormArray);
        period.crops = this.modelToCropDescs(periodModel.get('crops') as FormArray);
        period.rotations = this.modelToRotationDescs(periodModel.get('rotations') as FormArray);
        period.factor_combinations = this.modelToCombinationDescs(periodModel.get('factorCombinations') as FormArray);
        period.measurements = this.modelToMeasurementDescs(periodModel.get('measurements') as FormArray);

        return period;
    }

    modelToFactorDescs(model: FormArray): Factor[] {
        return model.controls.map((factorModel) => {
            const factor = new Factor();
            factor.id = factorModel.get('id').value;

            [factor.factor_term, factor.factor_label] = termPair(factorModel.get('factor').value);

            factor.description = stringOrNull(factorModel.get('description').value);
            factor.effect = factorModel.get('effect').value;
            factor.plot_application = stringOrNull(factorModel.get('plotApplication').value);

            factor.levels = (factorModel.get('levels') as FormArray).controls.map((levelModel) => {
                const level = new FactorLevel();
                level.id = levelModel.get('id').value;

                [level.factor_variant_term, level.factor_variant_label] = termPair(levelModel.get('factorVariant').value);

                level.amount = stringOrNull(levelModel.get('amount').value);
                [level.amount_unit_term, level.amount_unit_label] = termPair(levelModel.get('amountUnit').value);

                const dates = levelModel.get('dates') as FormGroup;
                level.start_year = intOrNull(dates.get('startYear').value);
                level.end_year = intOrNull(dates.get('endYear').value);

                [level.crop_id, level.all_crops] = this.getCrop(levelModel.get('cropId'));
                level.note = stringOrNull(levelModel.get('note').value);
                level.application_frequency = stringOrNull(levelModel.get('applicationFrequency').value);
                [level.application_method_term, level.application_method_label] = termPair(levelModel.get('applicationMethod').value);
                [level.chemical_form_term, level.chemical_form_label] = termPair(levelModel.get('chemicalForm').value);

                return level;
            }).filter((level) => {
                return level.factor_variant_term ||
                    level.factor_variant_label ||
                    level.amount ||
                    level.amount_unit_term ||
                    level.amount_unit_label ||
                    level.start_year ||
                    level.end_year ||
                    level.crop_id ||
                    level.note ||
                    level.application_frequency;
            });

            return factor
        }).filter((factor) => {
            return factor.factor_term ||
                factor.factor_label ||
                factor.description ||
                factor.effect ||
                factor.plot_application ||
                factor.levels && factor.levels.length > 0;
        });
    }

    modelToCropDescs(model: FormArray): Crop[] {
        return model.controls.map((cropModel) => {
            const crop = new Crop();
            crop.id = cropModel.get('id').value;

            const dates = cropModel.get('dates') as FormGroup;
            crop.start_year = intOrNull(dates.get('startYear').value);
            crop.end_year = intOrNull(dates.get('endYear').value);
            [crop.crop_term, crop.crop_label] = termPair(cropModel.get('crop').value);

            return crop;
        }).filter((crop) => {
            return crop.start_year ||
                crop.end_year ||
                crop.crop_term ||
                crop.crop_label;
        });
    }

    modelToRotationDescs(model: FormArray): Rotation[] {
        return model.controls.map((rotModel) => {
            const rot = new Rotation();
            rot.name = rotModel.get('name').value;

            const dates = rotModel.get('dates') as FormGroup;
            rot.start_year = intOrNull(dates.get('startYear').value);
            rot.end_year = intOrNull(dates.get('endYear').value);

            rot.phasing = rotModel.get('phasing').value;

            rot.phases = (rotModel.get('phases') as FormArray).controls.map((phaseModel) => {
                const phase = new RotationPhase();
                phase.crop_id = phaseModel.get('cropId').value;
                phase.notes = stringOrNull(phaseModel.get('notes').value);
                phase.same_phase = phaseModel.get('samePhaseAsPrevious').value;
                return phase;
            });  // Currently NOT filtering phases because a "blank" phase might be useful for padding the cycle.

            rot.is_treatment = rotModel.get('isTreatment').value;

            return rot;
        }).filter((rot) => {
            return rot.name ||
                rot.start_year ||
                rot.end_year ||
                rot.phasing ||
                rot.phases && rot.phases.length > 0;
        });
    }

    modelToCombinationDescs(model: FormArray): FactorCombination[] {
        return model.controls.map((combinationModel) => {
            const combination = new FactorCombination();

            combination.name =  combinationModel.get('name').value;
            combination.description = combinationModel.get('description').value;

            const dates = combinationModel.get('dates');
            combination.start_year = intOrNull(dates.get('startYear').value);
            combination.end_year = intOrNull(dates.get('endYear').value);

            combination.members = (combinationModel.get('members') as FormArray).controls.map((memberModel) => {
                const member = new FactorCombinationMember();
                member.factor_id = memberModel.get('factorId').value;
                [member.crop_id, member.all_crops] = this.getCrop(memberModel.get('cropId'));
                member.factor_level_id = memberModel.get('factorLevelId').value;
                member.comment = stringOrNull(memberModel.get('comment').value);
                member.amount = stringOrNull(memberModel.get('amount').value);
                [member.amount_unit_term, member.amount_unit_label] = termPair(memberModel.get('amountUnit').value);

                return member;
            }).filter((member) => {
                return member.factor_id ||
                    member.crop_id ||
                    member.all_crops ||
                    member.factor_level_id ||
                    member.comment ||
                    member.amount ||
                    member.amount_unit_term ||
                    member.amount_unit_label;
            });

            return combination;
        }).filter((combination) => {
            return combination.name ||
                combination.description ||
                combination.start_year ||
                combination.end_year ||
                combination.members && combination.members.length > 0
        });
    }

    modelToMeasurementDescs(model: FormArray): ExperimentMeasurement[] {
        return model.controls.map((measurementModel) => {
            const measurement = new ExperimentMeasurement();

            [measurement.variable_term, measurement.variable_label] = termPair(measurementModel.get('variable').value);
            [measurement.unit_term, measurement.unit_label] = termPair(measurementModel.get('unit').value);

            const material = measurementModel.get('material').value as Material;
            if (material) {
                measurement.material = material.material;
                measurement.crop_id = material.crop;
            }
            measurement.comment = stringOrNull(measurementModel.get('comment').value);
            measurement.collection_frequency = stringOrNull(measurementModel.get('collectionFrequency').value);
            measurement.scale = stringOrNull(measurementModel.get('scale').value);

            return measurement;
        }).filter((measurement) => {
            return measurement.variable_term ||
                measurement.variable_label ||
                measurement.unit_term ||
                measurement.unit_label ||
                measurement.crop_id ||
                measurement.material ||
                measurement.comment ||
                measurement.collection_frequency ||
                measurement.scale;
        });
    }

    getCrop(control: AbstractControl): [number, boolean] {
        if (control.value === Crop.ALL_CROPS) {
            return [null, true];
        } else {
            return [control.value, false];
        }
    }

    contactModelToData(contactModel: FormArray) {
        const contact = new Contact();
        contact.contact_type = contactModel.get('type').value;
        contact.value = contactModel.get('value').value;
        return contact;
    }

    modelToLiteratureDesc(model: FormGroup): ExperimentLiterature {
        const pub = new ExperimentLiterature();
        pub.doi = stringOrNull(model.get('doi').value);
        pub.title = stringOrNull(model.get('title').value);
        pub.language = stringOrNull(model.get('language').value);
        return pub;
    }

    selectableCrops(cropModels: FormArray) {
        const sc = [];
        for (const cropModel of cropModels.controls) {
            const cropValue = cropModel.get('crop').value;
            if (cropValue && (typeof(cropValue) === 'string' || cropValue.name)) {
                sc.push(new SelectableItem(
                    cropModel.get('id').value,
                    cropValue.name || cropValue
                ));
            }
        }
        return sc;
    }

    selectableFactors(factorModels: FormArray) {
        const sf = [];
        for (const factorModel of factorModels.controls) {
            const factorFactor = factorModel.get('factor').value,
                  factorId = factorModel.get('id').value;
            if (factorFactor) {
                if (typeof(factorFactor) === 'string') {
                    sf.push(new SelectableItem(factorId, factorFactor));
                } else if (factorFactor.name) {
                    sf.push(new SelectableItem(factorId, factorFactor.name));
                }
            }
        }
        return sf;
    }

    selectableFactorLevels(levelModels: FormArray) {
        const sfl = [];
        for (const levelModel of levelModels.controls) {
            const levelVariant = levelModel.get('factorVariant').value,
                  levelId = levelModel.get('id').value;

            if (levelVariant) {
                if (typeof(levelVariant) === 'string') {
                    sfl.push(new SelectableItem(levelId, levelVariant));
                } else {
                    sfl.push(new SelectableItem(levelId, levelVariant.name));
                }
            }
        }
        return sfl;
    }
}

function floatOrNull(str) {
    if (str !== null && str !== '') {
        return parseFloat(str);
    } else {
        return null;
    }
}

function stringOrNull(str) {
    if (str && str !== '') {
        return str;
    } else {
        return null;
    }
}

function intOrNull(str) {
    if (str !== null && str !== '') {
        return parseInt(str); 
    } else {
        return null;
    }
}

function termPair(model): [string, string] {
    return [
        model.identifier,
        stringOrNull(typeof(model) === 'string' ? model : model.name)
    ];
}

function datesValidator(group: FormGroup) : ValidationErrors {
    const startYearControl = group.controls['startYear'],
          endYearControl = group.controls['endYear'];

    const start = intOrNull(startYearControl.value),
          end = intOrNull(endYearControl.value);

    if (start !== null && end !== null && end < start) {
        endYearControl.setErrors({
            lessThanStart: true
        });
    } else {
        endYearControl.setErrors(null);
    }

    return;
}

function datesPlusOngoingValidator(group: FormGroup): ValidationErrors {
    const ongoingControl = group.controls['ongoing'],
        startYearControl = group.controls['startYear'],
        endYearControl = group.controls['endYear'];
    
    const ongoing = ongoingControl.value,
        start = intOrNull(startYearControl.value),
        end = intOrNull(endYearControl.value);

    if (start !== null && end !== null && end < start) {
        endYearControl.setErrors({
            lessThanStart: true
        });
    } else if (end === null && ongoing === false) {
        endYearControl.setErrors({
            required: true
        });
    } else {
        endYearControl.setErrors(null);
    }
    
    return;
}

function valueUnitValidator(group: FormGroup) : ValidationErrors {
    const valueControl = group.get('value'),
          unitControl = group.get('unit');
    if (valueControl.value !== '' && valueControl.value !== null && !unitControl.value) {
        unitControl.setErrors({
            required: true
        });
    } else {
        unitControl.setErrors(null);
    }
    return;
} 

function depthValueUnitValidator(group: FormGroup) : ValidationErrors {
    const valueControl1 = group.get('minDepth'),
          valueControl2 = group.get('maxDepth'),
          unitControl = group.get('unit');
    if (((valueControl1.value !== '' && valueControl1.value !== null) || (valueControl2.value !== '' && valueControl2.value !== null)) && !unitControl.value) {
        unitControl.setErrors({
            required: true
        });
    } else {
        unitControl.setErrors(null);
    }
    return;
}

function dataPolicyValidator(group: FormGroup): ValidationErrors {
    const policyStatusControl = group.get('dataPolicyStatus'),
          policyURLControl = group.get('dataPolicyURL');

          
    if (policyStatusControl.value === 'YesOnline') {
        if (policyURLControl.disabled) {
            policyURLControl.enable();
        }
        if (policyURLControl.value !== '' && policyURLControl.value !== null) {
            policyURLControl.setErrors(null);
        } else {
            policyURLControl.setErrors({required: true});
        }
    } else {
        if (policyURLControl.enabled) {
            policyURLControl.disable();
        }
        policyURLControl.setErrors(null);
    }

    return;
}

function periodsNameValidator(array: FormArray): ValidationErrors {
    for (const period of array.controls) {
        const nameControl = period.get('name');
        if ((nameControl.value && nameControl.value.length > 0) || array.controls.length <= 1) {
            nameControl.setErrors(null);
        } else {
            nameControl.setErrors({required: true});
        }
    }

    return;
}
import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
// import { Router } from '@angular/router';
import {Router, NavigationEnd} from '@angular/router'; // added for google analytics

import { Subject } from 'rxjs';

declare var window: any;
declare let gtag: Function; // added for google analytics

// Router interaction based on
// https://stackoverflow.com/questions/42514571/how-can-i-change-the-routing-in-angular-from-outside-the-scope-of-the-angular-ap

@Component({
    selector: 'glt-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
    private routerSubject: Subject<string> = new Subject<string>();

    constructor(private router: Router, private ngZone: NgZone) {
		this.router.events.subscribe(event => {
         if(event instanceof NavigationEnd){
             gtag('config', 'UA-154425543-1', 
                   {
                     'page_path': event.urlAfterRedirects
                   }
                  );
          }
       }
    )}
	
	ngOnInit() {
        window._gltenRouter = this.routerSubject;
        this.routerSubject.subscribe((url: string) => {
            this.ngZone.run(() =>
                this.router.navigate([`/${url}`])
            );
        });
    }

    ngOnDestroy() {
        this.routerSubject.unsubscribe();
    }

}

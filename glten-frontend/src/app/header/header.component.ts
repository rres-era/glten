import { Component, OnInit } from '@angular/core';

import { User } from '../types';
import { UserService } from '../user.service';

@Component({
  selector: 'glt-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    currentUser: User;

    constructor(private userService: UserService) {}

    ngOnInit() {
        this.loadCurrentUser();
    }

    loadCurrentUser() {
        this.userService.getCurrentUser()
            .subscribe((currentUser) => {
                this.currentUser = currentUser;
            });
    }
}

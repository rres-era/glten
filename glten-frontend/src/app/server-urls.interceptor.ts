import {Injectable, Inject, Optional} from '@angular/core';
import {HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders} from '@angular/common/http';
import {Request} from 'express';
import {REQUEST} from '@nguniversal/express-engine/tokens';

@Injectable()
export class ServerURLSInterceptor implements HttpInterceptor {
    constructor(@Optional() @Inject(REQUEST) protected request: Request) {}

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        let serverReq: HttpRequest<any> = req;
        if (this.request) {
            let newUrl = `${this.request.protocol}://${this.request.get('host')}`;
            if (!req.url.startsWith('/')) {
                newUrl += '/';
            }
            newUrl += req.url;

            // Attempt to pass the users' session cookie on to the back end, so
            // requests to authenticated resources can work.  Note that by default,
            // you can't set the 'cookie' header here -- there is a patch to the
            // underlying xhr2 library in main.server.ts to permit this.  Obviously,
            // cookie manipulation in this way will never work on the client.
            let headers = req.headers;
            if (this.request.headers.cookie) {
                headers = headers.append('cookie', this.request.headers.cookie);
            }

            serverReq = req.clone({
                url: newUrl,
                headers: headers
            });
        }
        return next.handle(serverReq);
    }
}
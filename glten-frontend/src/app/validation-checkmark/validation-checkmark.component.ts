import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';

/**
 * This observes the model, rather than doing the more obvious "just use the valid flag"
 * solution because we transiently disable the model (for instance, while saving) and
 * the valid flag is not available then!
 */

@Component({
    selector: 'glt-validation-checkmark',
    templateUrl: './validation-checkmark.component.html',
    styleUrls: ['./validation-checkmark.component.css']
})
export class ValidationCheckmarkComponent implements OnInit {
    @Input() model: AbstractControl;
    valid: boolean = false;

    constructor() { }

    ngOnInit() {
        if (this.model) {
            this.valid = this.model.status === 'VALID';
            this.model.statusChanges.subscribe((newStatus) => {
                // If the model becomes DISABLED or PENDING, we'll trust the
                // previous validity status.

                if (newStatus === 'VALID') {
                    this.valid = true;
                } else if (newStatus === 'INVALID') {
                    this.valid = false;
                }
            })
        }
    }
}

import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppModule } from './app.module';
import { AppComponent } from './app.component';
import { ServerURLSInterceptor } from './server-urls.interceptor';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';

@NgModule({
  imports: [
      AppModule,
      ServerModule,
      ModuleMapLoaderModule,
  ],
  providers: [{
      provide: HTTP_INTERCEPTORS,
      useClass: ServerURLSInterceptor,
      multi: true
  }],
  bootstrap: [AppComponent],
})
export class AppServerModule {}

import { Injectable } from '@angular/core';

import { render } from 'mustache';
import { Map, latLng, LatLng, tileLayer, marker, icon, featureGroup, control } from 'leaflet';

import { ExperimentDescBase } from './types';

@Injectable({
    providedIn: 'root'
})
export class MapConfigService {
    constructor() { }

    /**
     * Template for content of per-expt map popups.  Currently uses Mustache syntax.
     *
     * NB: an rxjs Subject object is exposed in the global JS namespace in order to trigger
     * router transitions.  See also app.component.
     *
     * NB2: we use a Bootstrap btn-link here (instead of a conventional <a> tag) in order
     * to avoid inadvertant navigation if the click event isn't cancelled.  There is a little
     * bit of global CSS (in styles.css) to ensure this is styled normally.
     */

    popupTemplate: string = `<button class="btn btn-link"
                                     onclick="_gltenRouter.next('/experiments/{{ experiment_id }}')">
                                {{name}}
                             </button>
                             <div>{{ site_name }}</div>
                             {{#organizations}}<div style="font-style: italic">{{name}}</div>{{/organizations}}`

    siteLatLng(site) {
        if (!site || site.site_centroid_latitude === null || site.site_centroid_longitude === null) {
            return null;
        }

        return latLng(site.site_centroid_latitude, site.site_centroid_longitude);
    }

    /**
     * Create a map of label->base layers.  This (annoyingly!) can't be a singleton since Leaflet misbehaves
     * (e.g. failing to load tiles) if layer objects are shared between leaflet instances.  However, components
     * embedding a map should cache the result of calling this, since updating the layers will lead to "forgetting"
     * the users' selection.
     */

    makeBaseLayersMap() {
        const MAP_LAYER = tileLayer(
            '//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            {attribution: '© OpenStreetMap contributors' }
        );
        const IMAGE_LAYER = tileLayer(
            '//server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
            {attribution: 'Source: Esri, DigitalGlobe, GeoEye, Earthstar Geographics, CNES/Airbus DS, USDA, USGS, AeroGRID, IGN, and the GIS User Community'}
        );

        return {
            'Maps': MAP_LAYER,
            'Satellite': IMAGE_LAYER
        }
    }

    experimentLayer(expts: ExperimentDescBase[]): any {
        const features = [];
        for (const expt of expts || []) {
            const sitePos = this.siteLatLng(expt);
            if (sitePos) {
                const feature = marker(
                    sitePos,
                    {
                        icon: icon({
                          iconSize: [ 21, 31 ],
                          iconAnchor: [ 16, 31 ],
                          iconUrl: 'static/marker-icon.png',
                          shadowUrl: 'static/marker-shadow.png'
                       })
                    }
                );

                feature.bindPopup(
                    render(this.popupTemplate, expt)
                );

                features.push(feature);
            }
        }

        return featureGroup(features);
    }

    mapConfig(zoom=2, focus: ExperimentDescBase = null): any {
        const config = (
            {
                zoom: zoom,
                center: this.siteLatLng(focus) || latLng(0, 0),
                scrollWheelZoom: false,  // After some testing, TAD prefers this disabled, because otherwise it is too
                                         // easy to trigger a zoom accidentally when using the scroll wheel (or trackpad
                                         // panning gestures) to scroll.
                attributionControl: false  // Add this ourselves for better configurability.
            }
        );

        return config;
    }

    addAttributionControl(map: Map) {
        map.addControl(
            control.attribution({
                position: 'bottomright',
                prefix: ''
            })
        );
    }
}
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'glt-contact-editor',
    templateUrl: './contact-editor.component.html',
    styleUrls: ['./contact-editor.component.css']
})
export class ContactEditorComponent {
    @Input() contactModel: FormGroup;
    @Output() onRemove: EventEmitter<any> = new EventEmitter();

    contactTypes = [
        {
            key: 'Email',
            label: 'E-mail',
            icon: 'send'
        },
        {
            key: 'Telephone',
            label: 'Telephone',
            icon: 'phone'
        },
        {
            key: 'Fax', 
            label: 'Fax',
            icon: 'fax'
        },
        {
            key: 'Address',
            label: 'Post',
            icon: 'envelope'

        },
        {
            key: 'URL',
            label: 'Web URL',
            icon: 'link'
        }
    ];

    constructor() { }

    get contactType() {
        return this.contactModel.get('type') as FormControl;
    }

    get contactTypeLabel() {
        return (
            this.contactTypes.filter((ct) => ct.key === this.contactType.value)[0] || {label: 'Select contact method...'}
        ).label;
    }

    get contactTypeIcon() {
        return (
            this.contactTypes.filter((ct) => ct.key === this.contactType.value)[0] || {icon: null}
        ).icon;
    }

    setContactType(typeKey) {
        this.contactType.setValue(typeKey);
        this.contactType.markAsDirty();
    }
}

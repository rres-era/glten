from datetime import datetime, timezone
import logging
import urllib.request as http
import json

from django.conf import settings
from django.shortcuts import get_object_or_404
from django.urls import path, include
from django.db.transaction import atomic
from django.db.models import Q
from django.http import Http404
from django.contrib.postgres.search import SearchVector, SearchQuery
from django.contrib.postgres.aggregates import StringAgg
from django.template.loader import render_to_string
from django.core.mail import send_mail, EmailMessage
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.models import User, Permission
from django.conf import settings

from rest_framework import status, serializers
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import AllowAny, BasePermission
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes

from glten.models import Experiment, ExperimentDesc, CurrentUser, Vocabulary, VocabularyTerm, \
    ExperimentPublicationRequest, ExperimentPublication
from glten.serializers import ExperimentDescSerializer, ExperimentSerializer, CurrentUserSerializer, \
    VocabularySerializer, VocabularyTermSerializer, VocabularyTermSerializerVerbose, \
    ExperimentPublicationRequestSerializer, ExperimentPublicationRequestFullSerializer, ConciseDescSerializer


DESCRIPTION_HISTORY_LIMIT = 5


# Control the fields which are used to build the full-text search index of published experiments.
# Note that the index WON'T be automatically rebuilt if this is edited -- only newly-published
# experiments will benefit from extra fields.  If need be a migration could be added to
# refresh the index on existing experiments.

EXPERIMENT_SV = SearchVector(
    'name',
    'experiment_type_label',
    'objective',
    'description',

    'site_name',
    'site_locality',
    'site_region',
    'site_country',
    'site_history',
    'site_management',

    'site_soil_type_label',
    'site_soil_description',

    'site_climatic_type_label',
    'site_climate_description',

    StringAgg('organizations__name', delimiter='; '),
    StringAgg('people__name', delimiter='; '),

    StringAgg('periods__name',  delimiter='; '),
    StringAgg('periods__design_description',  delimiter='; '),
    StringAgg('periods__description', delimiter='; '),

    StringAgg('periods__crops__crop_label', delimiter='; '),
    StringAgg('periods__factors__factor_label', delimiter='; '),
    StringAgg('periods__measurements__variable_label', delimiter='; '),

    StringAgg('literature__title', delimiter='; ')
)


class PublishExperimentPermission(BasePermission):
    def has_permission(self, request, view):
        return request.user is not None and request.user.has_perm('glten_publish_experiment')


def validate_desc(desc):
    """
    Validate desc via the server-side validation endpoint, if configured.  See SERVER_VALIDATION_ENDPOINT
    in local_settings_template.py
    """

    validator_service = settings.SERVER_VALIDATION_ENDPOINT
    if validator_service is None:
        return True

    serializer = ExperimentDescSerializer(desc)
    data = json.dumps(serializer.data).encode('utf-8')
    validate_req = http.Request(validator_service, headers={'content-type': 'application/json'}, method='POST')
    resp = http.urlopen(validate_req, data);
    result = json.loads(resp.read().decode('utf-8'))

    return result['status'] == 'VALID'


def get_publishers():
    """
    Return a QuerySet of all User objects with the "publish" permission.
    """

    publisher_perm = Permission.objects.get(codename='publish_experiment', content_type__app_label='glten')  # Throws if not found
    return User.objects.filter(Q(user_permissions=publisher_perm) |
                               Q(groups__permissions=publisher_perm) |
                               Q(is_superuser=True))


def gc_experiment(expt):
    """
    'Garbage collector' to ensure that an experiment doesn't accumulate an unreasonable
    number of old descriptions.  This is an initial implementation which might need
    tuning in the future, in particular paying more attention to age and novelty of
    descriptions, rather than just limiting their number.
    """

    # If there is an open or resolved publication requests, we definitely want to keep
    # the corresponding description.  Publication requests that have been voided (probably
    # because the user superceded them with a newer version before they were reviewed)
    # are reasonable targets for deletion
    pubreqs = expt.publication_requests.exclude(request_status='Void')
    unpublished_descs = expt.descriptions.exclude(publication_requests__pk__in=pubreqs).\
        order_by('-saved_time')

    for i in range(DESCRIPTION_HISTORY_LIMIT, len(unpublished_descs)):
        unpublished_descs[i].delete()


@api_view(['GET', 'POST'])
@atomic
def experiment(request):
    if request.method == 'GET':
        sort = request.query_params.get('sort', 'name')
        if sort not in {'name', 'date'}:
            return Response('sort should be `name` or `date`', status=status.HTTP_400_BAD_REQUEST)

        sort_order = request.query_params.get('order', 'asc')
        if sort_order not in {'asc', 'desc'}:
            return Response('order should be `asc` or `desc`')

        user = request.user
        expts = list(user.experiments.filter(deleted=False))

        if sort == 'name':
            expts.sort(key=lambda expt: expt.latest_desc.name if expt.latest_desc else '')
        elif sort == 'date':
            dummy_date = datetime.fromtimestamp(0, tz=timezone.utc)
            expts.sort(key=lambda expt: expt.latest_desc.saved_time if expt.latest_desc else dummy_date)

        if sort_order == 'desc':
            expts.reverse()

        serializer = ExperimentSerializer(expts, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = ExperimentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(owner=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
@atomic
def experiment_desc(request, expt_id, desc_id = None):
    expt = get_object_or_404(Experiment.objects.filter(deleted=False), pk=expt_id)

    if expt.owner != request.user:
        raise PermissionDenied()

    if request.method == 'GET':
        if desc_id is not None:
            desc = expt.descriptions.get(pk=desc_id)
            if desc is None:
                return Http404('No matching description revision')
        else:
            desc = expt.descriptions.all().order_by('-saved_time').first()
            if desc is None:
                desc = ExperimentDesc()
                
        serializer = ExperimentDescSerializer(desc)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = ExperimentDescSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(experiment=expt)
            gc_experiment(expt)  # Ensure that the number of descs doesn't get out of hand
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@atomic
def experiment_revisions(request, expt_id):
    expt = get_object_or_404(Experiment.objects.filter(deleted=False), pk=expt_id)

    serializer = ConciseDescSerializer(expt.descriptions.all().order_by('-saved_time'), many=True)
    return Response(serializer.data)


@api_view(['POST'])
def experiment_request_publication(request, expt_id):
    with atomic():
        expt = get_object_or_404(Experiment.objects.filter(deleted=False), pk=expt_id)
        latest_desc = expt.descriptions.all().order_by('-saved_time').first()

        if not validate_desc(latest_desc):
            return Response('Experiment description is not valid', status=status.HTTP_400_BAD_REQUEST)

        pending_pubreqs = expt.publication_requests.filter(request_status='Pending')
        for pubreq in pending_pubreqs:
            pubreq.request_status = 'Void'
            pubreq.save()

        pubreq = expt.publication_requests.create(experiment_desc=latest_desc)

    email_from = getattr(settings, 'DEFAULT_FROM_EMAIL', None)
    emails_to = [user.email for user in get_publishers() if user.email is not None and user.email != '']
    if len(emails_to) is not None and email_from is not None:
        try:
            msg = EmailMessage(
                f'Ready to review: { pubreq.experiment_desc.name }',
                render_to_string('approval-flow/ready_to_review.html',
                                 {'experiment_name': pubreq.experiment_desc.name,
                                  'experiment_id': expt.pk,
                                  'site_url': f'{request.scheme}://{get_current_site(request).domain}'}),
                email_from,
                [],
                emails_to   # BCC these addresses, because there may be multiple reviewers who don't
                            # want their addresses leaked to one another.
            )
            msg.send()
        except Exception as e:
            logging.exception('Failed to send e-mail confirmation')

    serializer = ExperimentPublicationRequestSerializer(pubreq)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(['POST'])
@permission_classes((PublishExperimentPermission,))
@atomic
def experiment_unpublish(request, expt_id):
    expt = get_object_or_404(Experiment.objects.filter(deleted=False), pk=expt_id)
    expt.publications.all().delete()
    removal_request = expt.publication_requests.create(request_status='Removed')
    serializer = ExperimentPublicationRequestSerializer(removal_request)
    return Response(serializer.data)


@api_view(['POST'])
@atomic
def experiment_delete(request, expt_id):
    expt = get_object_or_404(Experiment, pk=expt_id)
    if expt.owner != request.user:
        raise PermissionDenied()

    if expt.publications.all().exists():
        return Response('Cannot delete experiment, because it has been published', status.HTTP_400_BAD_REQUEST)

    # Void any pending publication requests, to prevent possible confusion about
    # what should be published.
    expt.publication_requests.filter(request_status='Pending').update(request_status='Void')
    expt.deleted = True
    expt.save()

    serializer = ExperimentSerializer(expt)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes((PublishExperimentPermission,))
def publication_requests(request):
    pubreqs = ExperimentPublicationRequest.objects.filter(request_status='Pending',
                                                          experiment__deleted=False)
    serializer = ExperimentPublicationRequestSerializer(pubreqs, many=True)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes((PublishExperimentPermission,))
def publication_request(request, expt_id):
    expt = get_object_or_404(Experiment.objects.filter(deleted=False), pk=expt_id)
    pubreq = expt.publication_requests.\
        exclude(request_status='Void').\
        order_by('-request_time').\
        first()

    if pubreq is None:
        raise Http404('No publication request for this experiment')

    if pubreq.request_status != 'Pending':
        return Response('No pending request for this experiment', status.HTTP_400_BAD_REQUEST)

    serializer = ExperimentPublicationRequestFullSerializer(pubreq)
    return Response(serializer.data)


@api_view(['POST'])
@permission_classes((PublishExperimentPermission,))
def publication_request_approve(request, expt_id):
    with atomic():
        expt = get_object_or_404(Experiment.objects.filter(deleted=False), pk=expt_id)
        pubreq = expt.publication_requests.\
            exclude(request_status='Void').\
            order_by('-request_time').\
            first()

        if pubreq is None or pubreq.request_status != 'Pending':
            return Response('No pending request for this experiment', status=status.HTTP_400_BAD_REQUEST)

        if not validate_desc(pubreq.experiment_desc):
            return Response('Experiment description is not valid', status=status.HTTP_400_BAD_REQUEST)

        pubreq.request_status = 'Approved'
        pubreq.save()

        search_vector = ExperimentDesc.objects.filter(pk=pubreq.experiment_desc.pk).\
            annotate(search=EXPERIMENT_SV).\
            values_list('search').\
            first()[0]

        expt.publications.all().delete()
        expt.publications.create(experiment_desc=pubreq.experiment_desc,
                                 publisher=request.user,
                                 search_vector=search_vector)

    # E-mail sending runs outside the transaction for to minimize the time we keep the DB
    # transaction open.

    email_from = getattr(settings, 'DEFAULT_FROM_EMAIL', None)
    email_to = expt.owner.email
    if email_to is not None and email_from is not None:
        try:
            send_mail(
                f'Approved: { pubreq.experiment_desc.name }',
                render_to_string('approval-flow/approved.html', {'experiment_name': pubreq.experiment_desc.name,
                                                                 'experiment_id': expt.pk,
                                                                 'site_url': f'{request.scheme}://{get_current_site(request).domain}'}),
                email_from,
                [email_to]
            )
        except Exception as e:
            logging.exception('Failed to send e-mail confirmation')

    serializer = ExperimentPublicationRequestSerializer(pubreq)
    return Response(serializer.data)


class RejectionBodySerializer(serializers.Serializer):
    comment = serializers.CharField(required=True, allow_null=False, allow_blank=False)


@api_view(['POST'])
@permission_classes((PublishExperimentPermission,))
def publication_request_reject(request, expt_id):
    with atomic():
        expt = get_object_or_404(Experiment.objects.filter(deleted=False), pk=expt_id)
        pubreq = expt.publication_requests.\
            exclude(request_status='Void').\
            order_by('-request_time').\
            first()

        if pubreq is None or pubreq.request_status != 'Pending':
            return Response('No pending request for this experiment', status.HTTP_400_BAD_REQUEST)

        serializer = RejectionBodySerializer(data=request.data)
        if serializer.is_valid():
            pubreq.request_status = 'Rejected'
            pubreq.comment = serializer.validated_data['comment']
            pubreq.save()
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    email_from = getattr(settings, 'DEFAULT_FROM_EMAIL', None)
    email_to = expt.owner.email
    if email_to is not None and email_from is not None:
        try:
            send_mail(
                f'Rejected: { pubreq.experiment_desc.name }',
                render_to_string('approval-flow/rejected.html', {'experiment_name': pubreq.experiment_desc.name,
                                                                 'experiment_id': expt.pk,
                                                                 'site_url': f'{request.scheme}://{get_current_site(request).domain}'}),
                email_from,
                [email_to]
            )
        except Exception as e:
            logging.exception('Failed to send e-mail confirmation')

    resp_serializer = ExperimentPublicationRequestSerializer(pubreq)
    return Response(resp_serializer.data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def current_user(request):
    user = request.user
    resp_data = CurrentUser(user)  # Wrap so we can give an explicit null for the
                                   # not-logged-in case

    serializer = CurrentUserSerializer(resp_data)
    return Response(serializer.data)


@api_view(['GET'])
def vocab_list(request):
    vocabs = Vocabulary.objects.all()
    serializer = VocabularySerializer(vocabs, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def vocab_terms(request, vocab_id):
    vocab = get_object_or_404(Vocabulary, pk=vocab_id)
    serializer = VocabularyTermSerializer(vocab.terms.all(), many=True)
    return Response(serializer.data)


@api_view(['GET'])
def vocab_descriptions(request, vocab_id):
    vocab = get_object_or_404(Vocabulary, pk=vocab_id)
    serializer = VocabularyTermSerializerVerbose(vocab.terms.all(), many=True)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def public_experiments(request):
    search = request.query_params.get('q')

    offset_str = request.query_params.get('offset', 0)
    try:
        offset = int(offset_str)
    except ValueError:
        return Response('offset should be an integer', status=status.HTTP_400_BAD_REQUEST)

    limit_str = request.query_params.get('count', 1000000)
    try:
        limit = int(limit_str)
    except ValueError:
        return Response('limit should be an integer', status=status.HTTP_400_BAD_REQUEST)

    sort = request.query_params.get('sort', 'name')
    if sort not in {'name', 'date'}:
        return Response('sort should be `name` or `date`', status=status.HTTP_400_BAD_REQUEST)

    sort_order = request.query_params.get('order', 'asc')
    if sort_order not in {'asc', 'desc'}:
        return Response('order should be `asc` or `desc`')

    pubs = ExperimentPublication.objects.filter(experiment__deleted=False)
    if search is not None:
        pubs = pubs.filter(search_vector=SearchQuery(search))

    expts = [vals.experiment_desc 
             for vals in pubs.select_related('experiment_desc').all()]
    if sort == 'name':
        expts.sort(key=lambda expt: expt.name)
    elif sort == 'date':
        expts.sort(key=lambda expt: expt.saved_time)

    if sort_order == 'desc':
        expts.reverse()

    expts = expts[offset:offset+limit]

    response = {
        'total_experiments': ExperimentPublication.objects.count(),
        'total_matches': pubs.count(),
        'offset': offset,
        'limit': limit,
        'experiments': ConciseDescSerializer(expts, many=True).data,
    }

    return Response(response)


@api_view(['GET'])
@permission_classes((AllowAny,))
def public_experiment_desc(request, expt_id):
    expt = ExperimentDesc.objects.filter(experiment__pk=expt_id, 
                                         experiment__deleted=False,
                                         publications__isnull=False).first()
    if expt is None:
        raise Http404('No such experiment, or not published')
    else:
        serializer = ExperimentDescSerializer(expt)
        return Response(serializer.data)


urlpatterns = [
    path('current-user', current_user),
    path('experiment', experiment),
    path('experiment/<int:expt_id>', experiment_revisions),
    path('experiment/<int:expt_id>/desc', experiment_desc),
    path('experiment/<int:expt_id>/desc/<int:desc_id>', experiment_desc),
    path('experiment/<int:expt_id>/publish', experiment_request_publication),
    path('experiment/<int:expt_id>/unpublish', experiment_unpublish),
    path('experiment/<int:expt_id>/delete', experiment_delete),
    path('pubreq', publication_requests),
    path('pubreq/<int:expt_id>', publication_request),
    path('pubreq/<int:expt_id>/approve', publication_request_approve),
    path('pubreq/<int:expt_id>/reject', publication_request_reject),
    path('vocab', vocab_list),
    path('vocab/<int:vocab_id>/terms', vocab_terms),
    path('vocab/<int:vocab_id>/descriptions', vocab_descriptions),
    path('public/experiment', public_experiments),
    path('public/experiment/<int:expt_id>', public_experiment_desc)
]
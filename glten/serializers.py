from django.contrib.auth.models import User
from rest_framework import serializers
from composite_field.rest_framework_support import ModelSerializer

from glten.models import Experiment, ExperimentDesc, CurrentUser, Crop, Person, Contact, Organization, \
  Vocabulary, VocabularyTerm, ExperimentPeriod, Rotation, RotationPhase, Factor, FactorLevel, \
  FactorCombination, FactorCombinationMember, ExperimentMeasurement, ExperimentPublicationRequest, \
  SitePerimeterPoint, SiteSoilProperty, SiteClimateProperty, ExperimentLiterature


class VocabularyTermRelatedField(serializers.RelatedField):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def to_representation(self, value):
        return value.identifier

    def to_internal_value(self, data):
        if data is not None:
            return self.get_queryset().get(identifier=data)


class PrimaryKeyOneWayField(serializers.Field):
    """
    WARNING: an assymetrical field implementation.

    On read, converts objects to their primary keys.
    On write, just returns the representation data.

    Intended for use in nested serializers, where the primary key will get resolved
    further up in the serializer hierarchy at write time.
    """
    def to_internal_value(self, data):
        return data

    def to_representation(self, value):
        return value.pk


class CropSerializer(ModelSerializer):
    id = serializers.IntegerField(required=False)  # Need to make this explict in order to retrieve IDs when writing
    crop_term = VocabularyTermRelatedField(required=False, 
                                           allow_null=True,
                                           queryset=VocabularyTerm.objects.filter(vocabulary__name='Crops'))

    class Meta:
        model = Crop
        exclude = ('period',)  # Include ID to allow links in rotation


class FactorLevelSerializer(ModelSerializer):
    id = serializers.IntegerField(required=False)  # Need to make this explict in order to retrieve IDs when writing
    factor_variant_term = VocabularyTermRelatedField(required=False, 
                                                     allow_null=True,
                                                     queryset=VocabularyTerm.objects.filter(vocabulary__name='Factor Levels'))
    amount_unit_term = VocabularyTermRelatedField(required=False, 
                                                  allow_null=True,
                                                  queryset=VocabularyTerm.objects.filter(vocabulary__name='Units'))
    application_method_term = VocabularyTermRelatedField(required=False,
                                                         allow_null=True,
                                                         queryset=VocabularyTerm.objects.filter(vocabulary__name='Application Methods'))
    chemical_form_term = VocabularyTermRelatedField(required=False,
                                                    allow_null=True,
                                                    queryset=VocabularyTerm.objects.filter(vocabulary__name='Substance'))
    crop_id = PrimaryKeyOneWayField(source='crop', required=False, allow_null=True)

    class Meta:
        model = FactorLevel
        fields = ('id', 'factor_variant_term', 'factor_variant_label', 'amount', 'amount_unit_term', 'amount_unit_label', \
                  'start_year', 'end_year', 'crop_id', 'all_crops', 'note', 'application_frequency',
                  'application_method_term', 'application_method_label', 'chemical_form_term', 'chemical_form_label')

class FactorSerializer(ModelSerializer):
    id = serializers.IntegerField(required=False)  # Need to make this explict in order to retrieve IDs when writing
    factor_term = VocabularyTermRelatedField(required=False,
                                             allow_null=True,
                                             queryset=VocabularyTerm.objects.filter(vocabulary__name='Factors'))
    levels = FactorLevelSerializer(required=False, many=True)

    class Meta:
        model = Factor
        exclude = ('period',)


class FactorCombinationMemberSerializer(ModelSerializer):
    factor_id = PrimaryKeyOneWayField(source='factor', required=False, allow_null=True)
    crop_id = PrimaryKeyOneWayField(source='crop', required=False, allow_null=True)
    factor_level_id = PrimaryKeyOneWayField(source='factor_level', required=False, allow_null=True)
    amount_unit_term = VocabularyTermRelatedField(required=False,
                                                  allow_null=True,
                                                  queryset=VocabularyTerm.objects.filter(vocabulary__name='Units'))


    class Meta:
        model = FactorCombinationMember
        fields = ('factor_id', 'crop_id', 'all_crops', 'factor_level_id', 'comment', 'amount',
                  'amount_unit_term', 'amount_unit_label')


class FactorCombinationSerializer(ModelSerializer):
    members = FactorCombinationMemberSerializer(required=False, many=True)

    class Meta:
        model = FactorCombination
        exclude = ('id', 'period')


class ExperimentMeasurementSerializer(ModelSerializer):
    crop_id = PrimaryKeyOneWayField(source='crop', required=False, allow_null=True)
    variable_term = VocabularyTermRelatedField(required=False,
                                               allow_null=True,
                                               queryset=VocabularyTerm.objects.filter(vocabulary__name='Variables'))
    unit_term = VocabularyTermRelatedField(required=False,
                                           allow_null=True,
                                           queryset=VocabularyTerm.objects.filter(vocabulary__name='Units'))

    class Meta:
        model = ExperimentMeasurement
        fields = ('material', 'crop_id', 'comment', 'collection_frequency', 'variable_label', 'variable_term',
                  'unit_label', 'unit_term', 'scale')


class ContactSerializer(ModelSerializer):
    class Meta:
        model = Contact
        exclude = ('id',)


class PersonSerializer(ModelSerializer):
    contacts = ContactSerializer(required=False, many=True)
    role_term = VocabularyTermRelatedField(required=False,
                                           allow_null=True,
                                           queryset=VocabularyTerm.objects.filter(vocabulary__name='Person Roles'))

    class Meta:
        model = Person
        exclude = ('experiment',)


class OrganizationSerializer(ModelSerializer):
    contacts = ContactSerializer(required=False, many=True)
    role_term = VocabularyTermRelatedField(required=False,
                                           allow_null=True,
                                           queryset=VocabularyTerm.objects.filter(vocabulary__name='Organization Roles'))

    class Meta:
        model = Organization
        exclude = ('experiment',)


class RotationPhaseSerializer(ModelSerializer):
    crop_id = PrimaryKeyOneWayField(source='crop', required=False, allow_null=True)

    class Meta:
        model = RotationPhase 
        fields = ('crop_id', 'notes', 'same_phase')


class RotationSerializer(ModelSerializer):
    phases = RotationPhaseSerializer(required=False, many=True)

    class Meta:
        model = Rotation
        exclude = ('period',)


class ExperimentPeriodSerializer(ModelSerializer):
    design_type_term = VocabularyTermRelatedField(required=False,
                                                  allow_null=True,
                                                  queryset=VocabularyTerm.objects.filter(vocabulary__name='Design Types'))
    crops = CropSerializer(required=False, many=True)
    rotations = RotationSerializer(required=False, many=True)
    factors = FactorSerializer(required=False, many=True)
    factor_combinations = FactorCombinationSerializer(required=False, many=True)
    measurements = ExperimentMeasurementSerializer(required=False, many=True)

    class Meta:
        model = ExperimentPeriod
        exclude = ('experiment_desc',)


class SitePerimeterPointSerializer(ModelSerializer):
    class Meta:
        model = SitePerimeterPoint
        exclude = ('site',)


class SiteSoilPropertySerializer(ModelSerializer):
    variable_term = VocabularyTermRelatedField(required=False,
                                               allow_null=True,
                                               queryset=VocabularyTerm.objects.filter(vocabulary__name='Soil Properties'))
    unit_term = VocabularyTermRelatedField(required=False,
                                           allow_null=True,
                                           queryset=VocabularyTerm.objects.filter(vocabulary__name='Units'))

    class Meta:
        model = SiteSoilProperty
        exclude = ('site',)


class SiteClimatePropertySerializer(ModelSerializer):
    variable_term = VocabularyTermRelatedField(required=False,
                                               allow_null=True,
                                               queryset=VocabularyTerm.objects.filter(vocabulary__name='Climate Properties'))
    unit_term = VocabularyTermRelatedField(required=False,
                                           allow_null=True,
                                           queryset=VocabularyTerm.objects.filter(vocabulary__name='Units'))

    class Meta:
        model = SiteClimateProperty
        exclude = ('site',)


class LiteratureSerializer(ModelSerializer):
    class Meta:
        model = ExperimentLiterature
        exclude = ('id', 'experiment',)


class ExperimentDescSerializer(ModelSerializer):
    experiment_type_term = VocabularyTermRelatedField(required=False,
                                                      allow_null=True,
                                                      queryset=VocabularyTerm.objects.filter(vocabulary__name='Experiment Types'))
    experiment_id = PrimaryKeyOneWayField(source='experiment', read_only=True)
    people = PersonSerializer(required=False, many=True)
    organizations = OrganizationSerializer(required=False, many=True)
    periods = ExperimentPeriodSerializer(required=False, many=True)
    data_statement_term = VocabularyTermRelatedField(required=False,
                                                     allow_null=True,
                                                     queryset=VocabularyTerm.objects.filter(vocabulary__name='Data Statements'))
    data_license_term = VocabularyTermRelatedField(required=False,
                                                   allow_null=True,
                                                   queryset=VocabularyTerm.objects.filter(vocabulary__name='Data Licenses'))
    site_type_term = VocabularyTermRelatedField(required=False,
                                                allow_null=True,
                                                queryset=VocabularyTerm.objects.filter(vocabulary__name='Site Types'))
    site_perimeter = SitePerimeterPointSerializer(required=False, many=True)
    site_soil_type_term = VocabularyTermRelatedField(required=False,
                                                     allow_null=True,
                                                     queryset=VocabularyTerm.objects.filter(vocabulary__name='Soil Types'))
    site_soil_properties = SiteSoilPropertySerializer(required=False, many=True)
    site_climatic_type_term = VocabularyTermRelatedField(required=False,
                                                         allow_null=True,
                                                         queryset=VocabularyTerm.objects.filter(vocabulary__name='Climatic Types'))
    site_climate_properties = SiteClimatePropertySerializer(required=False, many=True)
    literature = LiteratureSerializer(required=False, many=True)

    def create(self, validated_data):
        orgs = validated_data.pop('organizations', [])
        people = validated_data.pop('people', [])
        periods = validated_data.pop('periods', [])
        perimeter = validated_data.pop('site_perimeter', [])
        soil_properties = validated_data.pop('site_soil_properties', [])
        climate_properties = validated_data.pop('site_climate_properties', [])
        literature = validated_data.pop('literature', [])

        desc = ExperimentDesc.objects.create(**validated_data)

        for org in orgs:
            contacts = org.pop('contacts', [])
            org_record = desc.organizations.create(**org)
            for contact in contacts:
                org_record.contacts.create(**contact)

        for person in people:
            contacts = person.pop('contacts', [])
            person_record = desc.people.create(**person)
            for contact in contacts:
                person_record.contacts.create(**contact)

        for period in periods:
            crops = period.pop('crops', [])
            rotations = period.pop('rotations', [])
            factors = period.pop('factors', [])
            factor_combinations = period.pop('factor_combinations', [])
            measurements = period.pop('measurements', [])

            period_record = desc.periods.create(**period)
            
            crops_by_id = {}
            for crop in crops:
                crop_id = crop.pop('id', -1)
                crop_record = period_record.crops.create(**crop)
                crops_by_id[crop_id] = crop_record

            for rotation in rotations:
                phases = rotation.pop('phases', [])
                rot_record = period_record.rotations.create(**rotation)
                for (number, phase) in enumerate(phases):
                    crop_id = phase.pop('crop', None)
                    rot_record.phases.create(**phase, crop=crops_by_id[crop_id] if crop_id is not None else None, number=number)

            factors_by_id = {}
            levels_by_id = {}
            for factor in factors:
                factor_id = factor.pop('id', -1)
                levels = factor.pop('levels', [])
                factor_record = period_record.factors.create(**factor)
                factors_by_id[factor_id] = factor_record
                for level in levels:
                    level_id = level.pop('id', -1)
                    crop_id = level.pop('crop', None)
                    level_record = factor_record.levels.create(**level,
                                                               crop=crops_by_id[crop_id] if crop_id is not None else None)
                    levels_by_id[level_id] = level_record

            for factor_combination in factor_combinations:
                members = factor_combination.pop('members', [])
                fc_record = period_record.factor_combinations.create(**factor_combination)
                for member in members:
                    factor_id = member.pop('factor', None)
                    crop_id = member.pop('crop', None)
                    factor_level_id = member.pop('factor_level', None)
                    fc_record.members.create(**member,
                                             factor=factors_by_id[factor_id] if factor_id is not None else None,
                                             factor_level=levels_by_id[factor_level_id] if factor_level_id is not None else None,
                                             crop=crops_by_id[crop_id] if crop_id is not None else None)

            for measurement in measurements:
                crop_id = measurement.pop('crop', None)
                period_record.measurements.create(**measurement,
                                                  crop=crops_by_id[crop_id] if crop_id is not None else None)

        for (index, point) in enumerate(perimeter):
            desc.site_perimeter.create(**point, index=index)

        for prop in soil_properties:
            desc.site_soil_properties.create(**prop)

        for prop in climate_properties:
            desc.site_climate_properties.create(**prop)

        for pub in literature:
            desc.literature.create(**pub)

        return desc

    class Meta:
        model = ExperimentDesc
        exclude = ('experiment',)
        include = ('experiment_id')


class ConciseDescSerializer(ModelSerializer):
    """
    A minimalist representation of an ExperimentDesc, for (read-only) embedding
    within other API responses.
    """

    experiment_id = PrimaryKeyOneWayField(source='experiment')
    organizations = OrganizationSerializer(required=False, many=True)

    class Meta:
        model = ExperimentDesc
        fields = ('id', 'experiment_id', 'name', 'saved_time',
                  'site_name', 'organizations',
                  'site_centroid_latitude', 'site_centroid_longitude')


class ExperimentSerializer(ModelSerializer):
    def create(self, validated_data):
        return Experiment.objects.create(owner=validated_data['owner'])

    latest_desc = serializers.SerializerMethodField()
    published = serializers.SerializerMethodField()
    latest_request = serializers.SerializerMethodField()

    def get_latest_desc(self, obj):
        latest_desc = obj.latest_desc
        if latest_desc is not None:
            desc_serializer = ConciseDescSerializer(latest_desc)
            return desc_serializer.data
        else:
            return None

    def get_published(self, obj):
        pub = obj.publications.first()  # Should only be one.
        if pub is not None:
            desc_serializer = ConciseDescSerializer(pub.experiment_desc)
            return desc_serializer.data
        else:
            return None

    def get_published_latest(self, obj):
        pub = obj.publications.first()
        latest_desc = obj.latest_desc

        return pub is not None and latest_desc is not None and pub.experiment_desc == latest_desc

    def get_latest_request(self, obj):
        pubreq = obj.publication_requests.exclude(request_status='Void').order_by('-request_time').first()
        if pubreq is not None:
            pubreq_serializer = ExperimentPublicationRequestSerializer(pubreq)
            return pubreq_serializer.data
        else:
            return None

    class Meta:
        model = Experiment
        fields = ('id', 'latest_desc', 'published', 'latest_request')


class UserSerializer(ModelSerializer):
    is_publisher = serializers.SerializerMethodField()
    is_account_manager = serializers.SerializerMethodField()

    def get_is_publisher(self, user):
        return user.has_perm('glten.publish_experiment')

    def get_is_account_manager(self, user):
        return user.is_staff or user.is_superuser

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'is_publisher', 'is_account_manager')


class CurrentUserSerializer(serializers.Serializer):
    user = UserSerializer(required=False)


class VocabularySerializer(ModelSerializer):
    class Meta:
        model = Vocabulary
        fields = ('id', 'name', 'version', 'search_full_text')


class VocabularyTermSerializer(ModelSerializer):
    class Meta:
        model = VocabularyTerm
        fields = ('identifier', 'name', 'uri', 'dead')


class VocabularyTermSerializerVerbose(ModelSerializer):
    class Meta:
        model = VocabularyTerm
        fields = ('identifier', 'name', 'description', 'uri', 'dead')


class ExperimentPublicationRequestSerializer(ModelSerializer):
    experiment_id = PrimaryKeyOneWayField(source='experiment')
    experiment_desc = ConciseDescSerializer()

    class Meta:
        model = ExperimentPublicationRequest
        fields = ('experiment_id', 'experiment_desc', 'request_time', 'request_status', 'comment', )

class ExperimentPublicationRequestFullSerializer(ModelSerializer):
    experiment_id = PrimaryKeyOneWayField(source='experiment')
    experiment_desc = ExperimentDescSerializer()

    class Meta:
        model = ExperimentPublicationRequest
        fields = ('experiment_id', 'experiment_desc', 'request_time', 'request_status', 'comment', )

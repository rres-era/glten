"""
WSGI config for glten project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os
import sys 
from django.core.wsgi import get_wsgi_application

sys.path.append('home/ec2-user/glten/web')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'local_settings')

application = get_wsgi_application()

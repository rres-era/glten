from django import forms
from django.views.generic.edit import FormView
from django.contrib.auth import update_session_auth_hash, password_validation
from django.contrib.auth import views as auth_views
from django.contrib.auth import forms as auth_forms
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters


# Attempts to re-use as much as possible of the basic Django PasswordChangeForm,
# but extended to allow for other types of profile update as well.

class UserChangeForm(auth_forms.PasswordChangeForm):
    old_password = forms.CharField(
        label="Current password",
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'current-password', 'autofocus': True}),
        help_text="(Required to make any changes)"
    )
    new_password1 = forms.CharField(
        label="New password",
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
        strip=False,
        help_text=password_validation.password_validators_help_text_html() + '<p>Leave blank for no change</p>',
        required=False
    )
    new_password2 = forms.CharField(
        label="New password confirmation",
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
        required=False
    )

    first_name = forms.CharField(
        label='First name',
        strip=True
    )

    last_name = forms.CharField(
        label='Last name',
        strip=True
    )

    email = forms.CharField(
        label='E-mail',
        strip=True
    )

    def __init__(self, user, initial={}, *args, **kwargs):
        self.user = user
        initial['first_name'] = user.first_name
        initial['last_name'] = user.last_name
        initial['email'] = user.email
        super().__init__(user=user, initial=initial, *args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')

        if password1 == '' and password2 == '':
            return ''
        else:
            return super().clean_new_password2()

    def save(self, commit=True):
        self.user.first_name = self.cleaned_data['first_name']
        self.user.last_name = self.cleaned_data['last_name']
        self.user.email = self.cleaned_data['email']

        password = self.cleaned_data.get('new_password1')
        if len(password) > 0:
            self.user.set_password(password)

        if commit:
            self.user.save()
        return self.user

    field_order = ['old_password', 'first_name', 'last_name', 'email', 'new_password1', 'new_password2']


class UserChangeView(auth_views.PasswordContextMixin, FormView):
    form_class = UserChangeForm
    success_url = '/'
    template_name = 'registration/user_change_form.html'
    title = 'User change'

    @method_decorator(sensitive_post_parameters())
    @method_decorator(csrf_protect)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, form.user)
        return super().form_valid(form)

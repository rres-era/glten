from django.contrib import admin

from glten.models import Experiment

admin.site.register(Experiment)
from glten.models import Vocabulary, VocabularyTerm

from django.core.management.base import BaseCommand
from django.db import transaction
import csv

# Script for loading ontologies/vocabularies into the GLTEN database.  This is configured
# by the BOOTSTRAP_ONTOLOGIES array, below.
#
# The script attempts to re-use existing ontology terms in the database.  This means it
# should always be safe to re-run
#
# The following properties are recognized on an ontology definition:
#
# - file (string) name of file to load
# - name (string) name of ontology
# - version (string, default "1") current ontology version (not currently used by client)
# - search_full_text (boolean, default False) hint that clients should use full-text search
#.                                            when querying this ontology.  Works best if
#                                             there is a description on most terms.
# - type (string) loader function to use.  Currently only 'csv' is supported
#
# For CSV ontologies (currently the only supported type), the following additional options
# are supported:
# 
# - name_field (string, required) name of column to use as term name
# - identifier_field (string, required) name of column to use as term identifier
# - uri_field (string) name of column to use as term URI
# - description_field (string) name of column to use as term description
# - filter_field (string) name of column to use for filtering the terms
# - filter_include_values (list) list of one or more strings to match in the
#                                "filter_field" column.  None can be used here
#                                to include terms where the filter_field column
#                                is empty.


BOOTSTRAP_ONTOLOGIES = [
    {
        'type': 'csv',
        'name': 'Units',
        'file': 'ontologies/measurement_units.csv',
        'identifier_field': 'Measurement unit',
        'uri_field': 'uri',
        'name_field': 'Measurement unit name'
    },
    {
        'type': 'csv',
        'name': 'Variables',
        'file': 'ontologies/variables.csv',
        'identifier_field': 'variableName',
        'name_field': 'variableName',
        'uri_field': 'variableURI',
        'filter_field': 'propertyType',
        'filter_include_values': ['S', None]  # Soil and "unannotated" variables are included
    },
    {
        'type': 'csv',
        'name': 'Factor Levels',
        'file': 'ontologies/factors.csv',
        'identifier_field': 'factorName',
        'uri_field': 'uri',
        'name_field': 'factorName'
    },
    {
        'type': 'csv',
        'name': 'Factors',
        'file': 'ontologies/factor_types.csv',
        'identifier_field': 'factorName',
        'name_field': 'factorName',
        'uri_field': 'uri'
    },
    {
        'type': 'csv',
        'name': 'Crops',
        'file': 'ontologies/crops.csv',
        'identifier_field': 'variableName',
        'name_field': 'variableName',
        'uri_field': 'variableURI',
		'description_field':'altName',
		'search_full_text': True
    },
    {
        'type': 'csv',
        'name': 'Soil Types',
        'file': 'ontologies/soils.csv',
        'identifier_field': 'variableName',
        'name_field': 'variableName',
        'uri_field': 'variableURI',
        'description_field': 'description',
        'search_full_text': True
    },
    {
        'type': 'csv',
        'name': 'Substance',
        'file': 'ontologies/substance.csv',
        'identifier_field': 'name',
        'name_field': 'name',
        'uri_field': 'uri'
    },
    {
        'type': 'csv',
        'name': 'Application Methods',
        'file': 'ontologies/applicationMethods.csv',
        'identifier_field': 'name',
        'name_field': 'name',
        'description_field': 'description',
        'uri_field': 'uri'
    },
    {
        'type': 'csv',
        'name': 'Data Statements',
        'file': 'ontologies/data_statements.csv',
        'identifier_field': 'statementText',
        'name_field': 'statementText'
    },
    {
        'type': 'csv',
        'name': 'Data Licenses',
        'file': 'ontologies/data_licenses.csv',
        'identifier_field': 'license_name',
        'name_field': 'license_name'
    },
    {
        'type': 'csv',
        'name': 'Site Types',
        'file': 'ontologies/site_types.csv',
        'identifier_field': 'name',
        'name_field': 'name',
        'uri_field': 'uri'
    },
    {
        'type': 'csv',
        'name': 'Experiment Types',
        'file': 'ontologies/experiment_types.csv',
        'identifier_field': 'name',
        'name_field': 'name',
        'uri_field': 'uri'
    },
    {
        'type': 'csv',
        'name': 'Design Types',
        'file': 'ontologies/design_types.csv',
        'identifier_field': 'name',
        'name_field': 'name',
        'uri_field': 'uri'
    },
    {
        'type': 'csv',
        'name': 'Person Roles',
        'file': 'ontologies/person_roles.csv',
        'identifier_field': 'name',
        'name_field': 'name',
        'uri_field': 'uri'
    },
    {
        'type': 'csv',
        'name': 'Organization Roles',
        'file': 'ontologies/organization_roles.csv',
        'identifier_field': 'name',
        'name_field': 'name',
        'uri_field': 'uri'
    },
    {
        'type': 'csv',
        'name': 'Soil Properties',
        'file': 'ontologies/variables.csv',
        'identifier_field': 'variableName',
        'name_field': 'variableName',
        'uri_field': 'variableURI',
        'filter_field': 'propertyType',
        'filter_include_values': ['S']
    },
    {
        'type': 'csv',
        'name': 'Climate Properties',
        'file': 'ontologies/variables.csv',
        'identifier_field': 'variableName',
        'name_field': 'variableName',
        'uri_field': 'variableURI',
        'filter_field': 'propertyType',
        'filter_include_values': ['W']
    },
    {
        'type': 'csv',
        'name': 'Climatic Types',
        'file': 'ontologies/climatic_types.csv',
        'identifier_field': 'name',
        'name_field': 'name',
        'uri_field': 'uri'
    }
]

class Command(BaseCommand):
    help = 'Import required ontologies and terms'

    def handle(self, *args, **options):
        with transaction.atomic():
            for record in BOOTSTRAP_ONTOLOGIES:
                opts = dict(record)
                file = opts.pop('file')
                name = opts.pop('name')
                input_type = opts.pop('type')
                version = opts.pop('version', '1')
                fulltext = opts.pop('search_full_text', False)

                loader = LOADERS[input_type]
                vocab, _created = Vocabulary.objects.get_or_create(name=name)
                vocab.version = version
                vocab.search_full_text = fulltext
                vocab.save()

                loader(file, vocab, **opts)


def load_csv(file, vocab, 
             identifier_field=None, 
             name_field=None,
             uri_field=None,
             description_field=None,
             filter_field=None,
             filter_include_values=None):
    # If the vocabulary already has terms, set them dead, then re-vivify any terms
    # that get re-used.  This means that any terms that existed at the last load
    # but have now been deleted will be left in the DB but marked as dead.
    vocab.terms.all().update(dead=True)

    print(file)
    filter_term = lambda val: True

    if filter_include_values is not None:
        filter_set = set(filter_include_values)
        filter_term = lambda val: val in filter_set

    with open(file) as csv_file:
        reader = csv.DictReader(csv_file)
        for row in reader:
            def get_field(fid):
                if fid is not None:
                    fval = row.get(fid)
                    if len(fval) > 0:
                        return fval

            if filter_term(get_field(filter_field)):
                term, _created = vocab.terms.get_or_create(identifier=get_field(identifier_field))
                term.name = get_field(name_field)
                term.uri = get_field(uri_field)
                term.description = get_field(description_field)
                term.dead = False  # Re-vivify any re-used existing terms
                term.save()


LOADERS = {
    'csv': load_csv
}

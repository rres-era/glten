import subprocess

from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = 'Build frontend code'

    def handle(self, *args, **opts):
        subprocess.call([
            'npm', 'install'
        ], cwd='glten-frontend')
        subprocess.call([
            'npm', 'run', '--', 'ng', 'build',
            '--prod',
            '--deploy-url', '/static/',
            '--output-path', '../frontend-static'
        ], cwd='glten-frontend')

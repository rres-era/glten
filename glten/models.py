from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVectorField
from django.utils.functional import cached_property

from composite_field import CompositeField

# NB: models are deliberately written rather "liberally" -- i.e. very few
# required/non-blank fields.  This is intentional, to permit work-in-progress
# drafts of experiments to be saved to the database.  There is an additional
# layer of validation (required fields etc.) which runs on the client.  For
# production deployments, the server can also re-run the validator (on the server,
# but still using the same JS code) before approving a description for
# publication.
#
# For vocabulary terms, there would ideally be constraints here defining
# which vocabulary/s are relevant.  This could, in principle, be done with
# limit_choices_to.  However, Django Rest Framework doesn't appear to 
# honour these, so instead we put these constraints in the relevant serializers.


class Vocabulary(models.Model):
    name = models.TextField(db_index=True, null=False, blank=False)
    version = models.TextField(null=True, blank=True)
    search_full_text = models.BooleanField(default=True)


class VocabularyTerm(models.Model):
    vocabulary = models.ForeignKey('Vocabulary', on_delete=models.CASCADE, related_name='terms')
    identifier = models.TextField(db_index=True, null=False)
    uri = models.TextField(null=True, blank=True)
    name = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    dead = models.BooleanField(default=False)

    class Meta:
        unique_together = ['vocabulary', 'identifier']


class LabelOrTerm(CompositeField):
    label = models.TextField(null=True, blank=True)
    term = models.ForeignKey('VocabularyTerm', null=True, on_delete=models.SET_NULL, related_name = '+')


class Experiment(models.Model):
    owner = models.ForeignKey(User, null=False, on_delete=models.PROTECT, related_name='experiments')
    deleted = models.BooleanField(default=False)

    @cached_property
    def latest_desc(self):
        return self.descriptions.order_by('-saved_time').first()

    def __str__(self):
        name = 'Unnamed'
        desc = self.latest_desc
        if desc is not None:
            name = desc.name

        return '{}: {}'.format(self.pk, name)


class ExperimentDesc(models.Model):
    ELEVATION_UNIT_CHOICES = ['Feet', 'Metres']
    DATA_POLICY_CHOICES = ['YesOnline', 'YesOffline', 'No', 'NotKnown']

    experiment = models.ForeignKey('Experiment', on_delete=models.CASCADE, related_name='descriptions')
    saved_time = models.DateTimeField(auto_now=True, null=False)

    name = models.TextField(null=True, blank=True)
    local_identifier = models.TextField(null=True, blank=True)
    experiment_type = LabelOrTerm()  ## "Experiment Type"

    url = models.TextField(null=True, blank=True)
    start_year = models.IntegerField(null=True)
    end_year = models.IntegerField(null=True)
    ongoing = models.BooleanField(default=False)
    establishment_period_end = models.IntegerField(null=True)
    objective = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
	#owner_curated = models.TextField(null=True, blank=True) new option not implemented

    # site info.  It's somewhat tempting to break this into its own model, but
    # in practice these are 1:1 with experiments, so an extra join in the DB
    # would be superfluous

    site_name = models.TextField(null=True, blank=True)
    site_local_code = models.TextField(null=True, blank=True)
    site_geonames_id = models.TextField(null=True, blank=True)  ## What's this?
    site_doi = models.TextField(null=True, blank=True)
    site_type = LabelOrTerm() ## "Site Type"
    site_locality = models.TextField(null=True, blank=True)
    site_region = models.TextField(null=True, blank=True)
    site_country = models.TextField(null=True, blank=True)
    site_centroid_latitude = models.FloatField(null=True)
    site_centroid_longitude = models.FloatField(null=True)
    site_elevation = models.FloatField(null=True)
    site_elevation_unit = models.TextField(null=True, choices=zip(ELEVATION_UNIT_CHOICES, ELEVATION_UNIT_CHOICES))
    site_slope = models.FloatField(null=True)
    site_slope_aspect = models.TextField(null=True, blank=True)
    # prototype has "ecosystemId" -- what's this
    # prototype has "climaticRegionId"
    site_visits_allowed = models.BooleanField(default=False)
    site_visiting_arrangements = models.TextField(null=True, blank=True)
    site_history = models.TextField(null=True, blank=True)
    site_management = models.TextField(null=True, blank=True)

    site_soil_type = LabelOrTerm()  # Soil Types
    site_soil_description = models.TextField(null=True, blank=True)

    site_climatic_type = LabelOrTerm()  # Climate types
    site_climate_description = models.TextField(null=True, blank=True)

    # data availability/info
    data_statement = LabelOrTerm()
    data_url = models.TextField(null=True, blank=True)
    data_policy_status = models.TextField(null=True, choices=zip(DATA_POLICY_CHOICES, DATA_POLICY_CHOICES))
    data_policy_url = models.TextField(null=True, blank=True)
    data_license = LabelOrTerm()
    data_contact = models.TextField(null=True, blank=True)

    farm_operations_data = models.BooleanField(default=False)
    sample_archive = models.BooleanField(default=False)
    samples_available = models.BooleanField(default=False)
    data_access_statement = models.TextField(null=True, blank=True)


class ExperimentPeriod(models.Model):
    experiment_desc = models.ForeignKey('ExperimentDesc', on_delete=models.CASCADE, related_name='periods')
    start_year = models.IntegerField(null=True)
    end_year = models.IntegerField(null=True)
    ongoing = models.BooleanField(default=False)
    name = models.TextField(null=True, blank=True)
    design_type = LabelOrTerm() ## "Design Type"
    number_of_blocks = models.IntegerField(null=True)
    number_of_plots = models.IntegerField(null=True)
    number_of_subplots = models.IntegerField(null=True)
    number_of_replicates = models.IntegerField(null=True)
    number_of_factor_combinations = models.IntegerField(null=True)
    number_of_harvests_per_year = models.IntegerField(null=True)
    design_description = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    factorial = models.BooleanField(default=False)

    class Meta:
        ordering = ['pk']


class Crop(models.Model):
    period = models.ForeignKey('ExperimentPeriod', on_delete=models.CASCADE, related_name='crops')
    crop = LabelOrTerm()
    start_year = models.IntegerField(null=True)
    end_year = models.IntegerField(null=True)

    class Meta:
        ordering = ['pk']


class Rotation(models.Model):
    PHASING_CHOICES = [
        'Complete',
        'Incomplete'
    ]

    period = models.ForeignKey('ExperimentPeriod', on_delete=models.CASCADE, related_name='rotations')
    name = models.TextField(null=True, blank=True)
    phasing = models.TextField(null=True, choices=zip(PHASING_CHOICES, PHASING_CHOICES))
    start_year = models.IntegerField(null=True)
    end_year = models.IntegerField(null=True)
    is_treatment = models.BooleanField(default=False)

    class Meta:
        ordering = ['pk']


class RotationPhase(models.Model):
    rotation = models.ForeignKey('Rotation', on_delete=models.CASCADE, related_name='phases')
    number = models.IntegerField(null=False)  # Not user-editable, so okay to be rigid here
    crop = models.ForeignKey('Crop', null=True, on_delete=models.SET_NULL)
    notes = models.TextField(null=True, blank=True)
    same_phase = models.BooleanField(default=False)

    class Meta:
        unique_together = ['rotation', 'number']
        ordering = ['number']


class Factor(models.Model):
    PLOT_APPLICATION_CHOICES = ['Sub plot', 'Whole plot', 'Other']
    EFFECT_CHOICES = ['Direct', 'Residual', 'Cumulative']

    period = models.ForeignKey('ExperimentPeriod', on_delete=models.CASCADE, related_name='factors')
    factor = LabelOrTerm()
    description = models.TextField(null=True, blank=True)
    plot_application = models.TextField(null=True, choices=zip(PLOT_APPLICATION_CHOICES, PLOT_APPLICATION_CHOICES))
    effect = models.TextField(null=True, blank=True, choices=zip(EFFECT_CHOICES, EFFECT_CHOICES))

    class Meta:
        ordering = ['pk']


# Was "ExperimentFactorCategory" in prototype
class FactorLevel(models.Model):
    factor = models.ForeignKey('Factor', on_delete=models.CASCADE, related_name='levels')
    factor_variant = LabelOrTerm()
    amount = models.TextField(null=True, blank=True)
    amount_unit = LabelOrTerm()
    start_year = models.IntegerField(null=True)
    end_year = models.IntegerField(null=True)
    crop = models.ForeignKey('Crop', null=True, on_delete=models.SET_NULL)
    all_crops = models.BooleanField(default=False)
    note = models.TextField(null=True, blank=True)
    application_frequency = models.TextField(null=True, blank=True)
    application_method = LabelOrTerm()
    chemical_form = LabelOrTerm()

    class Meta:
        ordering = ['pk']


class FactorCombination(models.Model):
    period = models.ForeignKey('ExperimentPeriod', on_delete=models.CASCADE, related_name='factor_combinations')
    name = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    start_year = models.IntegerField(null=True)
    end_year = models.IntegerField(null=True)

    class Meta:
        ordering = ['pk']


class FactorCombinationMember(models.Model):
    factor_combination = models.ForeignKey('FactorCombination', on_delete=models.CASCADE, related_name='members')
    factor = models.ForeignKey('Factor', null=True, on_delete=models.SET_NULL)
    factor_level = models.ForeignKey('FactorLevel', null=True, on_delete=models.SET_NULL)
    crop = models.ForeignKey('Crop', null=True, on_delete=models.SET_NULL)
    all_crops = models.BooleanField(default=False)
    comment = models.TextField(null=True, blank=True)
    amount = models.TextField(null=True, blank=True)
    amount_unit = LabelOrTerm()

    class Meta:
        ordering = ['pk']


# TODO: prototype had these connected to experiments, not design-periods.  This creates some
# cross-referencing issues because crops are attached to periods.  Reconsider once everything 
# else is going.
class ExperimentMeasurement(models.Model):
    MATERIAL_CHOICES = ['AllCrops', 'SpecifiedCrop', 'Soil']

    period = models.ForeignKey('ExperimentPeriod', on_delete=models.CASCADE, related_name='measurements')
    material = models.TextField(null=True, choices=zip(MATERIAL_CHOICES, MATERIAL_CHOICES))
    crop = models.ForeignKey('Crop', null=True, on_delete=models.SET_NULL, related_name='measurements')
    comment = models.TextField(null=True, blank=True)
    collection_frequency = models.TextField(null=True, blank=True)
    variable = LabelOrTerm();
    unit = LabelOrTerm()
    scale = models.TextField(null=True, blank=True)

    class Meta:
        ordering = ['pk']


class Contact(models.Model):
    CONTACT_TYPE_CHOICES = [
        'Telephone',
        'Email',
        'Address',
        'Fax',
        'URL'
    ]

    contact_type = models.TextField(null=False, blank=False, choices=zip(CONTACT_TYPE_CHOICES, CONTACT_TYPE_CHOICES))
    value = models.TextField(null=False, blank=True)


class Person(models.Model):
    experiment = models.ForeignKey('ExperimentDesc', null=False, on_delete=models.CASCADE, related_name='people')
    title = models.TextField(null=True, blank=True)  # Could make this a controlled vocabulary -- but is it worth it?
    name = models.TextField(null=False, blank=True)  # Currently making this a "flat" string which allows culturally-preferred order
                                                     # of name components
                                                     # (https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/)
    affiliation = models.TextField(null=True, blank=True)
    department = models.TextField(null=True, blank=True)
    orcid = models.TextField(null=True, blank=True)
    contacts = models.ManyToManyField(Contact)
    is_contact = models.BooleanField(default=False)
    role = LabelOrTerm()  # Person Roles

    class Meta:
        ordering = ['pk']


class Organization(models.Model):
    experiment = models.ForeignKey('ExperimentDesc', null=False, on_delete=models.CASCADE, related_name='organizations')
    name = models.TextField(null=True, blank=True)
    name_uri = models.TextField(null=True, blank=True)
    abbreviation = models.TextField(null=True, blank=True)
    # nis_id   ## what is this
    contacts = models.ManyToManyField(Contact)
    role = LabelOrTerm()  # Organization Roles

    class Meta:
        ordering = ['pk']


class SitePerimeterPoint(models.Model):
    site = models.ForeignKey(ExperimentDesc, null=False, on_delete=models.CASCADE, related_name='site_perimeter')
    index = models.IntegerField(null=False)  # Not user-editable
    latitude = models.FloatField(null=False)
    longitude = models.FloatField(null=False)
    elevation = models.FloatField(null=True)

    class Meta:
        unique_together = ['site', 'index']
        ordering = ['index']


class SiteSoilProperty(models.Model):
    DEPTH_UNIT_CHOICES = ['Centimetres','Inches','Feet', 'Metres']

    site = models.ForeignKey(ExperimentDesc, null=False, on_delete=models.CASCADE, related_name='site_soil_properties')
    variable = LabelOrTerm()  ## Soil properties
    min_depth = models.FloatField(null=True)
    max_depth = models.FloatField(null=True)
    depth_unit = models.TextField(null=True, choices=zip(DEPTH_UNIT_CHOICES, DEPTH_UNIT_CHOICES))
    is_estimated = models.BooleanField(default=False)
    is_baseline = models.BooleanField(default=False)
    reference_year = models.IntegerField(null=True)
    typical_value = models.FloatField(null=True)
    max_value = models.FloatField(null=True)
    min_value = models.FloatField(null=True)
    unit = LabelOrTerm()  ## Units

    class Meta:
        ordering = ['pk']


class SiteClimateProperty(models.Model):
    site = models.ForeignKey(ExperimentDesc, null=False, on_delete=models.CASCADE, related_name='site_climate_properties')
    variable = LabelOrTerm()  ## Climate properties
    year_from = models.IntegerField(null=True)
    year_to = models.IntegerField(null=True)
    mean_value = models.FloatField(null=True)
    max_value = models.FloatField(null=True)
    min_value = models.FloatField(null=True)
    unit = LabelOrTerm()  ## Units

    class Meta:
        ordering = ['pk']


class ExperimentLiterature(models.Model):
    experiment = models.ForeignKey(ExperimentDesc, null=False, on_delete=models.CASCADE, related_name='literature')
    doi = models.TextField(null=True, blank=True)
    title = models.TextField(null=True, blank=True)
    language = models.TextField(null=True, blank=True)

    class Meta:
        ordering = ['pk']


class ExperimentPublication(models.Model):
    experiment = models.ForeignKey(Experiment, null=False, on_delete=models.PROTECT, related_name='publications')
    experiment_desc = models.ForeignKey(ExperimentDesc, null=False, on_delete=models.PROTECT, related_name='publications')
    publisher = models.ForeignKey(User, null=False, on_delete=models.PROTECT)
    published_time = models.DateTimeField(null=False, auto_now=True)
    search_vector = SearchVectorField(null=True)

    class Meta:
        indexes = [
            GinIndex(fields=['search_vector'])
        ]


class ExperimentPublicationRequest(models.Model):
    STATUS_CHOICES = [
        'Pending',
        'Accepted',
        'Rejected',
        'Void',    # Status for requests which have been superceded by another.  Only retained for logging purposes
        'Removed'  # Used for dummy "requests" generated when an experiment is unpublished.
    ]

    experiment = models.ForeignKey(Experiment, null=False, on_delete=models.PROTECT, related_name='publication_requests')
    experiment_desc = models.ForeignKey(ExperimentDesc, null=True, on_delete=models.SET_NULL, related_name='publication_requests')
    request_time = models.DateTimeField(null=False, auto_now=True)
    request_status = models.TextField(null=False, blank=False, default='Pending', choices=zip(STATUS_CHOICES, STATUS_CHOICES))
    comment = models.TextField(null=True, blank=True)

    class Meta:
        permissions = (
            ('publish_experiment', "Can see and approve other users' publication requests"),
        )


# Wrapper for user to help with serialization.
class CurrentUser:
    def __init__(self, user):
        self.user = user
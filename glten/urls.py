from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic.base import TemplateView
from django.contrib.auth import views as auth_views

from glten.user_change import UserChangeView


class FrontendPage(TemplateView):
    template_name = 'index.html'


urlpatterns = [
    # Account management.

    path('accounts/login/',
         auth_views.LoginView.as_view(extra_context={'contact_email': getattr(settings, 'DEFAULT_FROM_EMAIL', 'unknown')}),
         name='login'),
    path('accounts/logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),

    path('accounts/user-change/', UserChangeView.as_view(), name='user_change'),

    path('accounts/password_reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('accounts/password_reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('accounts/reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('accounts/reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    path('api/v0/', include('glten.api')),

    path('admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + [
    re_path(r'^.*$', FrontendPage.as_view())
]

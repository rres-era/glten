# Generated by Django 2.2.5 on 2019-09-18 07:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('glten', '0008_literature'),
    ]

    operations = [
        migrations.AddField(
            model_name='experiment',
            name='deleted',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='experimentpublicationrequest',
            name='request_status',
            field=models.TextField(choices=[('Pending', 'Pending'), ('Accepted', 'Accepted'), ('Rejected', 'Rejected'), ('Void', 'Void'), ('Removed', 'Removed')], default='Pending'),
        ),
    ]

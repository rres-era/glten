# GLTEN Portal Architecture Overview

The portal is written with a (mostly) clear separation between backend (in Python) and frontend (JS).
(exception: user management -- see below).  The two components communicate using an HTTP/JSON
API (in a generally REST-ful style).  As well as running in the users' browser

## Key technologies

**Database**: Postgres.  The database uses fairly standard SQL, with the schema and all
queries generated via the Django ORM, so it should be possible to move to another
(Django-supported) SQL database with minimal changes.  The one exception to this is
the full-text experiment search, which is implemented using Postgres `tsvector`
records.  This would need to be replaced with either a similar full-text indexing
feature in the alternative database, or with a dedicated full-text indexing service
(e.g. Lucene, Sphinx, ...)

**Backend**: Django + Django Rest Framework (mostly for the "serializer" abstraction).

**User authentication/management**: For simplicity, as much of this as possible has
been handled with django.control.auth and django.contrib.admin -- which means it is
somewhat separate from the rest of the frontend.  There is a small amount of
bespoke Django form code in order to support user profile alterations while sticking
within this general pattern.

**Other frontend**: Angular.io (version 8.x).  Curation interface (and standalone)
validation uses the "Reactive Forms" model from @angular/forms.  Certain UI components
(notably dropdowns, typeaheads, etc.) from [ng-bootstrap](https://ng-bootstrap.github.io/).

For production deployments, we support server-side rendering using the 
[Univeral Angular Applications](https://angular.io/guide/universal) model.  Note that
there are a few GLTEN-specific changes to the default `server.ts` -- see comments
within that file.

**Styling**: Bootstrap 4.x

**Mapping and geo-coding**: Maps are rendered using Leaflet.js, wrapped using the
ngx-leaflet module.  See map-config.service for the default layer configurations.

Geo-coding (for the place name lookup in the site editor) is currently implemented
using the Openstreetmap's [Nominatim](https://wiki.openstreetmap.org/wiki/Nominatim) service.
It is considered unlikely that GLTEN will generate sufficient traffic to approach rate
limits on this service, but if traffic levels increase it could be swapped out for another
geocoding service.  The simplest option might be a locally-hosted copy of Nominatim.

## Database design

In most respects, the database is a fairly conventional relational model of the domain,
implemented using the Django ORM.  One major deviation is that -- for the top-level
object of interest (a long-term experiment) -- we permit many versions (internally
called descriptions) to exist within the database.  The primary motivations for this
are:

1. To support "undo" capabilities in the curation interface -- in particular, reverting
to earlier version 

2. To allow continued editing of an experiment after a particular description has been
submitted for publication -- while continuing to use the same data model for both
"work in progress" and "published" experiments.

This model permits further "historical" features in the future (e.g. viewing of
previously-published description in addition to the latest), and also means that
information is retained in the database which helps reconstruct who edited what, when.

A number of other tables (design phases, crop rotations, soil properties, etc., etc.)
form a tree structure rooted on experiment_desc.  When writing a description, all these
sub-parts get written at the same time -- there is no attempt to share sub-parts of
descriptions between versions, since this would be at best a minor reduction in database
size, but a big increase in complexity.  To enforce this, the sub-part tables are all linked
with "ON DELETE CASCADE" foreign keys.

Because we want to be able to store incomplete drafts in the database, we don't enforce
many field-level contraints at the database level (in particular, nearly every field
may be blank) -- although there *are* constraints to ensure that relationships between
tables in the database (foreign keys) remains coherent.  The final arbiter of validity
(required fields, etc.) is the Angular Forms validation code, implemented in 
`experiment-form-model.service`.  This is run on the client as the form is edited, and
the user interface prevents generation of publication requests when the form state isn't
valid.  To prevent invalid descriptions being approved using non-standard (or out of
date) clients, there is a second layer of validation (using the same code) which runs
on the server side before a publication request is accepted (see `/_internal/validate`
in the API section).

To ensure that the database doesn't grow indefinitely, there is a simple "garbage collector"
which runs when a new description is posted.  The current implementation limits the number of
**unpublished** descriptions for each experiment, and deletes old ones once this is exceeded.
Descriptions which have been published -- even if they are later superceded -- will always be
retained.

## API overview

All public API endpoints are found in the `/api/v0` namespace.  After release, incompatible
changes should be denoted with a new version tag.

"Experiment" endpoints allow users to access their own (potentially unpublished) experiment
descriptions, and all require authentication.

    GET /api/v0/experiment -- return a list of experiments

    POST /api/v0/experiment -- create a new experiment, owned by the user associated
                               with the current session

    GET /api/v0/experiment/:id/revisions -- list revisions associated with experiment <id>

    GET /api/v0/experiment/:id/desc
    GET /api/v0/experiment/:id/desc/:rev -- get a specified revision of the experiment
                                            description, or the latest revision if not
                                            specified

    POST /api/v0/experiment/:id/desc -- submit a new revision of an experiment's description.
                                        may trigger garbage collection of old revisions.

    POST /api/v0/experiment/:id/publish -- submit a request to publish the experiment

    POST /api/v0/experiment/:id/unpublish -- unpublish the experiment (only available to)
                                             users with "publish" permissions

    POST /api/v0/experiment/:id/delete -- delete the experiment.  in the current implementation,
                                          this will fail if there experiment is currently published.

"Pubreq" endpoints allow priveleged users to manipulate a queue of pending publication
requests.

    GET /api/v0/pubreq -- list currently-open publication requests.

    GET /api/v0/pubreq/:id -- get the experiment description associated with the
                              specified publication request.

    POST /api/v0/pubreq/:id/approve -- approve the publication request

    POST /api/v0/pubreq/:id/reject -- reject a publication request.  Body should
                                      have the form {'comment': 'Reason I dislike this'}

"Vocab" endpoints allow (read) access to vocabularies/ontologies, and are unrestricted.

    GET /api/v0/vocab -- list available vocabularies

    GET /api/v0/vocab/:id/terms -- list terms from the specified vocabulary

    GET /api/v0/vocab/:id/descriptions -- list terms plus descriptions

"Public" endpoints allow (read) access to the full set of published experiments, and are
unrestricted.

    GET /public/experiment -- list published experiments.  Can optionally be paginated
                              with count=<number> and offset=<number parameters, and
                              filtered using a q=<search_string> parameter, which, in
                              the current implementation, is passed to a full-text search
                              of the experiment descriptions.
                              As of November 2019, sorting can be controlled with
                              sort=<name|date> and order=<asc|desc>.

    GET /public/experiment/:id -- retrieve a description

There is one special internal API end-point:

    POST /_internal/validate

    Accepts an experiment description as the BODY of the request.  Runs the (Javascript)
    validation code then returns: {'status': 'VALID'} if the submitted data is valid,
    or an alternative status otherwise.

    Note that this is implemented in Javascript, so that validation logic can be shared with
    the client.  It is currently co-located with the server-side rendering support (see
    server.ts)


